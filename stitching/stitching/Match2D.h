//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// Match2D.h

#pragma once

#include "Matrix.h"

class CMatch2D
// container for corresponding 2D image points
{
 public:
    
    CMatch2D() { }
    ~CMatch2D() { }
    
    CMatch2D(int nMatches)
        {
            Create(nMatches);
        }

    void Create(int nMatches) 
        { data[0].Create(2, nMatches);
          data[1].Create(2, nMatches); }
    
    CMatch2D(const CMatch2D &c) { this->operator=(c); }   
    void operator=(const CMatch2D &c)
        {
            data[0] = c.data[0];
            data[1] = c.data[1];
        }

    CMatrix& operator[](int i) { return data[i]; }

    CMatrix data[2];
};
