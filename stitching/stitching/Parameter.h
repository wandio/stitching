//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

#pragma once

#include <math.h>
#include <iostream>
using namespace std;

//
// SIFT Parameters
//

enum ESIFTSizeSelector
{
    eMinDim,
    eRelativeScale
};

class CSIFTParameters
{
public:

CSIFTParameters() :
    nMatchesPerFeature(5),
        nOverlap(9),
        outlierDist(0.8f),
        sizeSelector(eMinDim),
        minImageDim(400),
        minImageDimThumb(50),
        relativeScale(0.25f),
        smallImageDim(300),
        SkipLevels(2),
        maxMemory((int)(0.25 * pow(2.0f, 30.0f)))
        { }
    
    void reset(void) {CSIFTParameters tmp; *this = tmp;} // Reset parameters to original values

    int nMatchesPerFeature;         // number of nearest neighbour matches to be considered for each feature
    int nOverlap;                   // the maximum number of images that may overlap a given ray
    float outlierDist;              // fraction of outlier distance within which correct matches lie
    ESIFTSizeSelector sizeSelector; // flag to determine SIFT image size selection method 
    int minImageDim;                // minimum dimension of input image
    int minImageDimThumb;           // minimum dimension of thumbnail image (copied from RenderParameters)
    float relativeScale;            // size of SIFT image relative to input image
    int smallImageDim;              // images with dim < smallImageDim may have extra SIFT features (SkipLevels < 2)
    int SkipLevels;					// levels to skip in SIFT extraction
    int maxMemory;                  // maximum memory for storage of SIFT images (computed from SystemParameters)
};

//
// RANSAC Parameters
//

class CRANSACParameters
{
public:
CRANSACParameters() :
    minDistComputeSimilarity(1.0f),
        epsilon(10.0f),
        alpha(10.0f),
        autoAlpha(false),
        alphaMax(30.0f),
        alphaMin(10.0f),
        beta(0.2f),
        nMatchesPerImage(5),
        maxIterations(500),
        maxScaleS(32.0f)
        { }

    void reset(void) {CRANSACParameters tmp; *this = tmp;} // reset parameters to original values
			
    float minDistComputeSimilarity; // minimum distance between pairs of points used to compute a similarity transform
    float epsilon;                  // outlier distance for RANSAC
    float alpha;                    // nMatches > alpha + beta * nOverlap for a correct image match
    bool autoAlpha;                 // set alpha as a fraction of the average number of interest points
    float alphaMax;                 // max value of alpha
    float alphaMin;					// min value of alpha
    float beta;                     
    int nMatchesPerImage;           // number of images to attempt to match to each other image
    int maxIterations;              // maximum number of RANSAC iterations
    float maxScaleS;                // maximum scale change for similarity transform
};

//
// Bundle Adjustment Parameters
//

enum EErrorFunction
{
    eQuadratic,
    eTruncatedQuadratic,
    eHuber
};

enum EOutlierDistanceUnits
{
    ePixels,
    eStandardDeviations
};

class CBundleParameters
{
public:
CBundleParameters() : 
    maxIterations(20),
        maxLMIterations(20),
        sigmae(1.0f),
        dsigmae(5.0f),
        maxError(6.0f),
        maxDError(0.1f),
        f0(500.0f),
        autoFocalLengthInit(true),
        priorSigmafFactor(0.1f),
        priorSigmaTheta((float)M_PI/16.0f),
        initialErrorFunction(eQuadratic),
        initialOutlierDistance(3.0f),
        initialOutlierDistanceUnits(eStandardDeviations),
        finalErrorFunction(eHuber),
        finalOutlierDistance(1.0f),
        finalOutlierDistanceUnits(ePixels)
        { }
    
    void reset(void) {CBundleParameters tmp; *this = tmp;} // reset parameters to original values

    int   maxIterations;  // maximum number of bundle adjustment iterations
    int   maxLMIterations; // maximum number of Levenberg-Margquardt iterations attempted
    float sigmae;         // noise standard deviation 
    float dsigmae;        // multiplicative factor for sigmae
    float maxError;       // maximum rms error for convergence
    float maxDError;      // maximum change in error between iterations for convergence
    float f0;             // initial value for focal length
    bool autoFocalLengthInit; // use heuristic to set focal length
    float priorSigmafFactor; // standard deviation for prior on focal length is this factor * mean f
    float priorSigmaTheta; // standard deviation for prior on theta
    EErrorFunction initialErrorFunction; // error function for initial bundle adjustment
    float initialOutlierDistance; // outlier distance for initial bundle adjustment
    EOutlierDistanceUnits initialOutlierDistanceUnits; // units for initial outlier distance
    EErrorFunction finalErrorFunction; // error function for final bundle adjustment
    float finalOutlierDistance; // outlier distance for final bundle adjustment
    EOutlierDistanceUnits finalOutlierDistanceUnits; // units for final outlier distance
};

//
// Rendering Parameters
//

enum ESizeSelector
{
    eWidth,
    eHeight,
    eRelativeSize
};

enum EBlendingMethod
{
    eNoBlend,
    eLinear,
    eMultiBand
};

enum EImageRotation
{
    eUpright,
    eClockwise,
    eAntiClockwise,
    eUpsideDown
};

enum ECropStyle
{
    eInterior,
    eExterior,
    eNone
};  

class CRenderParameters
{
public:
CRenderParameters() :
    render(true),
        thetaMin((float)-M_PI),
        thetaMax((float)M_PI),
        phiMin((float)-M_PI/2),
        phiMax((float)M_PI/2),
        width(2048),
        height(1400),
        relativeSize(0.1f),
        sizeSelector(eWidth),
        downsampleSmoothing(3.0f/8.0f),
        nBlendingBands(2),
        sigmaBlend(5.0f),
        imageRotation(eUpright),
        autoStraighten(true),
        autoCrop(eExterior),
        blendingMethod(eMultiBand),
        renderInBlocks(true),
        maxBlockSize(8192),
        minBlockSize(512),
        maxMemory((int)(0.75f * pow(2.0f, 30.0f))),
        renderEachIteration(false),
        useBoundedConvolve(true),
        JPEGQuality(85),
        blockDilate(true),
        thetaExtra(0.0f),
        phiExtra(0.0f),
        psiExtra(0.0f),
        gainCompensation(false),
        sigmaGain(0.1f),
        meanGain(1.0f),
        minImageDimThumb(50)
        { }

    void reset(void) {CRenderParameters tmp; *this = tmp;} // reset parameters to original values

    bool render; // render panorama or not
    
    // Angular range of panorama
    float thetaMin;
    float thetaMax;
    float phiMin;
    float phiMax;

    ESizeSelector sizeSelector; // which of width, height or relative scale determines panorama size
    int width; // width of panorama in pixels
    int height; // height of panorama in pixels
    float relativeSize; // relative scale of panorama compared to image 1
  
    float downsampleSmoothing; // for downsampling factor r, smooth with sigma = r * downsampleSmoothing
    int nBlendingBands; // number of bands to use in multi-band blending scheme
    float sigmaBlend; // gaussian blurring standard deviation for multiband blending

    EImageRotation imageRotation;
    EBlendingMethod blendingMethod;
    bool autoStraighten;
    ECropStyle autoCrop;
    bool renderInBlocks;      // render final panorama in blocks of size blockSize to save memory
    int maxBlockSize;         // maximum and minimum values for render block size
    int minBlockSize;
    int maxMemory;			  // maximum memory available for panorama rendering (computed from SystemParameters)
    bool renderEachIteration; // render previews after each bundle adjustment iteration
    bool useBoundedConvolve; 
    int JPEGQuality;		  // JPEG Quality setting
    bool blockDilate;		  // use block dilation in multi-band blending
    float thetaExtra;		  // extra rotations in theta, phi and psi (manual) 
    float phiExtra; 
    float psiExtra; 
    bool gainCompensation;    // compute gain for each image
    float sigmaGain;		  // standard deviation of gain correction
    float meanGain;           // mean for gain correction
    int minImageDimThumb;     // minimum dimension of thumbnail image
};

class CSystemParameters
{
public: 
CSystemParameters() : 
    //maxMemory(pow(2, 30)), // 1 Gig
    maxMemory(pow(2, 29)), // 0.5 Gig
        //maxMemory((int)(pow(2.0f, 28.0f))), // 0.25 Gig
        SIFTImageMemoryFrac(0.25f),
        outputHomography(false)
	{}
    
    void reset(void) {CSystemParameters tmp; *this = tmp;} // reset parameters to original values
    
    int maxMemory;             // total amount of memory available to the program
    float SIFTImageMemoryFrac; // fraction of memory to be spent on storing SIFT images
    bool outputHomography;     // panorama output is a 3 x 3 homography matrix (note: input not supported!)
};
