//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// BundleAdjust.h

#pragma once

#include "Parameter.h"
#include "MultiImageMatch.h"
#include <vector>
#include "Camera.h"

class CBundleAdjust
{
public:
    CBundleAdjust() { }
    ~CBundleAdjust() { }

    CBundleAdjust(CBundleParameters BundleParameters);

    void BundleAdjust(CMultiImageMatch &MultiImageMatch, vector<int> imageID, vector <CCamera> &Cameras, 
                      EErrorFunction errorFunction, float outlierDistance, EOutlierDistanceUnits outlierDistanceUnits); // Compute JtJ directly
    
    void RobustErrorFunction(CMatrix &r, CMatrix &fr, CMatrix &dfdr, float outlierDistance, EErrorFunction errorFunction); // Compute Huber robust error function
	
    void BundleAdjust0(CMultiImageMatch &MultiImageMatch, vector<int> imageID, vector <CCamera> &Cameras);    


    CBundleParameters Param;
};
