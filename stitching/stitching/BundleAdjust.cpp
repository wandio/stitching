//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// BundleAdjust.cpp

#include "assert.h"

#include "BundleAdjust.h"
#include "MatrixHelpers.h"

void NormaliseJ(CMatrix &J, float meanf);

CBundleAdjust::CBundleAdjust(CBundleParameters BundleParameters)
{
    Param = BundleParameters;
}

void CBundleAdjust::BundleAdjust0(CMultiImageMatch &MultiImageMatch, vector<int> imageID, vector <CCamera> &Cameras)
{    
    int   maxIterations = Param.maxIterations;
    int   maxLMIterations = Param.maxLMIterations;
    float sigmae        = Param.sigmae;
    float dsigmae       = Param.dsigmae;
    float maxError      = Param.maxError;
    float maxDError     = Param.maxDError;

    int** nMatches     = MultiImageMatch.nMatches;
    CMatch2D** matches = MultiImageMatch.matches;

    int nImages = (int)imageID.size();
    int nParameters = 4 * nImages - 3;

    // Set prior covariance
    
    // set sigma_f = 1/10 * mean f
    float meanf = 0;
    float priorSigmaf = 0;
    for (int i = 0; i < nImages; i++)
    {
        meanf += Cameras[imageID[i]].f;
    }
    meanf /= (float)nImages;
    priorSigmaf = 0.05f;
    //priorSigmaf = meanf / 10.0f;

    float priorSigmaTheta = (float)M_PI / 16.0f;
    
    CMatrix invCp(nParameters, nParameters);
    float priorSigmafInvSq = 1 / (priorSigmaf * priorSigmaf);
    float priorSigmaThetaInvSq = 1 / (priorSigmaTheta * priorSigmaTheta);
    invCp(0, 0) = priorSigmafInvSq;
    for (int i = 0; i < nImages - 1; i++)
    {
        invCp(4 * i + 1, 4 * i + 1) = priorSigmaThetaInvSq;
        invCp(4 * i + 2, 4 * i + 2) = priorSigmaThetaInvSq;
        invCp(4 * i + 3, 4 * i + 3) = priorSigmaThetaInvSq;
        invCp(4 * i + 4, 4 * i + 4) = priorSigmafInvSq;
    }

    // Rotation derivatives
    CMatrix dRdt1(3, 3); dRdt1(1, 2) = -1; dRdt1(2, 1) = 1;
    CMatrix dRdt2(3, 3); dRdt2(0, 2) = 1;  dRdt2(2, 0) = -1;
    CMatrix dRdt3(3, 3); dRdt3(0, 1) = -1; dRdt3(1, 0) = 1;

    int totalNMatches = 0;
    for (int i = 0; i < nImages; i++)
    {
        int im1 = imageID[i];
        for (int j = 0; j < nImages; j++)
        {
            int im2 = imageID[j];
            totalNMatches += nMatches[im1][im2];
        }// end for j
    }// end for i

    // Optimisation loop
    
    bool converged = false;
    int iteration = 0;
    float error;

    while (!converged && (iteration < maxIterations))
    {
        iteration++;
        //cerr << "iteration = " << iteration << endl;

        // Build Jacobean

        cerr << "Compute J" << endl;

        CMatrix J(2 * totalNMatches, 4 * nImages);
        CMatrix r(2 * totalNMatches, 1);
        
        int row = 0;
 
        for (int i1 = 0; i1 < nImages; i1++)
        {
            int im1 = imageID[i1];

            CMatrix Rim1 = Cameras[im1].R;
            float fim1   = Cameras[im1].f;
            
            for (int i2 = 0; i2 < nImages; i2++)
            {
                
                int im2 = imageID[i2];
                
                CMatrix Rim2 = Cameras[im2].R;
                float fim2   = Cameras[im2].f;
                
                int nMatches12 = nMatches[im1][im2];

                if (nMatches12 > 0)
                {                                        
                    CMatrix &u1 = matches[im1][im2][0];
                    CMatrix &u2 = matches[im1][im2][1];
                    
                    CMatrix u1h = homogeneous(u1);

                    CMatrix Kim1    = K(fim1);
                    CMatrix Kim2    = K(fim2);
                    CMatrix Kim1inv = K(1/fim1);
                    CMatrix Kim2inv = K(1/fim2);
                    
                    CMatrix ph = Kim2 * Rim2 * ~Rim1 * Kim1inv * u1h;
                    CMatrix p = unhomogeneous(ph);
                    
                    CMatrix r21 = u2 - p;
                    r21.vectorize();
                    
                    r.assign(r21, row);
                    
                    CMatrix dphdtim11 = Kim2 * Rim2 * ~(Rim1 * dRdt1) * Kim1inv * u1h;
                    CMatrix dphdtim12 = Kim2 * Rim2 * ~(Rim1 * dRdt2) * Kim1inv * u1h;
                    CMatrix dphdtim13 = Kim2 * Rim2 * ~(Rim1 * dRdt3) * Kim1inv * u1h;

                    CMatrix dinvKim1dfim1(3, 3);
                    float fim1neginvsq = - 1 / (fim1 * fim1);
                    dinvKim1dfim1(0, 0) = fim1neginvsq; dinvKim1dfim1(1, 1) = fim1neginvsq;
                    
                    CMatrix dphdfim1 = Kim2 * Rim2 * ~Rim1 * dinvKim1dfim1 * u1h;
                    
                    CMatrix dphdtim21 = Kim2 * Rim2 * dRdt1 * ~Rim1 * Kim1inv * u1h;
                    CMatrix dphdtim22 = Kim2 * Rim2 * dRdt2 * ~Rim1 * Kim1inv * u1h;
                    CMatrix dphdtim23 = Kim2 * Rim2 * dRdt3 * ~Rim1 * Kim1inv * u1h;

                    CMatrix dKim2dfim2(3, 3);
                    dKim2dfim2(0, 0) = 1; dKim2dfim2(1, 1) = 1;

                    CMatrix dphdfim2 = dKim2dfim2 * Rim2 * ~Rim1 * Kim1inv * u1h;
                    
                    CMatrix dp1dph(3, nMatches12);
                    dp1dph.assign( scalarpow(ph.row(2), -1), 0);
                    dp1dph.assign( - scalardiv( ph.row(0), scalarpow(ph.row(2), 2)), 2 );

                    CMatrix dp2dph(3, nMatches12);
                    dp2dph.assign( scalarpow(ph.row(2), -1), 1);
                    dp2dph.assign( - scalardiv( ph.row(1), scalarpow(ph.row(2), 2)), 2 );
                    
                    CMatrix dpdtim11(2, nMatches12);
                    CMatrix dpdtim12(2, nMatches12);
                    CMatrix dpdtim13(2, nMatches12);
                    CMatrix dpdfim1(2, nMatches12);

                    dpdtim11.assign( rowsum(scalarmult(dp1dph, dphdtim11)), 0);
                    dpdtim11.assign( rowsum(scalarmult(dp2dph, dphdtim11)), 1);
                    dpdtim12.assign( rowsum(scalarmult(dp1dph, dphdtim12)), 0);
                    dpdtim12.assign( rowsum(scalarmult(dp2dph, dphdtim12)), 1);
                    dpdtim13.assign( rowsum(scalarmult(dp1dph, dphdtim13)), 0);
                    dpdtim13.assign( rowsum(scalarmult(dp2dph, dphdtim13)), 1);
                    
                    dpdfim1.assign( rowsum(scalarmult(dp1dph, dphdfim1)), 0);
                    dpdfim1.assign( rowsum(scalarmult(dp2dph, dphdfim1)), 1);
                    
                    CMatrix dpdtim21(2, nMatches12);
                    CMatrix dpdtim22(2, nMatches12);
                    CMatrix dpdtim23(2, nMatches12);
                    CMatrix dpdfim2(2, nMatches12);

                    dpdtim21.assign( rowsum(scalarmult(dp1dph, dphdtim21)), 0);
                    dpdtim21.assign( rowsum(scalarmult(dp2dph, dphdtim21)), 1);
                    dpdtim22.assign( rowsum(scalarmult(dp1dph, dphdtim22)), 0);
                    dpdtim22.assign( rowsum(scalarmult(dp2dph, dphdtim22)), 1);
                    dpdtim23.assign( rowsum(scalarmult(dp1dph, dphdtim23)), 0);
                    dpdtim23.assign( rowsum(scalarmult(dp2dph, dphdtim23)), 1);
                    
                    dpdfim2.assign( rowsum(scalarmult(dp1dph, dphdfim2)), 0);
                    dpdfim2.assign( rowsum(scalarmult(dp2dph, dphdfim2)), 1);
                    
                    dpdtim11.vectorize();
                    dpdtim12.vectorize();
                    dpdtim13.vectorize();
                    dpdfim1.vectorize();

                    J.assign(dpdtim11, row, 4 * i1 + 0);
                    J.assign(dpdtim12, row, 4 * i1 + 1);
                    J.assign(dpdtim13, row, 4 * i1 + 2);
                    J.assign(dpdfim1, row, 4 * i1 + 3);

                    dpdtim21.vectorize();
                    dpdtim22.vectorize();
                    dpdtim23.vectorize();
                    dpdfim2.vectorize();
                    
                    J.assign(dpdtim21, row, 4 * i2 + 0);
                    J.assign(dpdtim22, row, 4 * i2 + 1);
                    J.assign(dpdtim23, row, 4 * i2 + 2);
                    J.assign(dpdfim2, row, 4 * i2 + 3);
                    
                    row += 2 * nMatches12;

                }// end if nMatches12 > 0

            }// end for i2

        }// end for i1

        cerr << "Set up LS" << endl;
      
        // scale J
        NormaliseJ(J, meanf);

        J = J.submatrix(0, 2 * totalNMatches - 1, 3, 4 * nImages - 1);

        // Levenberg-Marquardt iteration
        
        float error0 = r.norm() / (float)sqrt((float)totalNMatches);
        error = error0;
        cerr << "  error = " << error << endl;
        
        CMatrix Jt = ~J;
        CMatrix Jtr = Jt * r;
        //CMatrix JtJ = Jt * J;
        cerr << "Compute JtJ" << endl;
        CMatrix JtJ = ComputeMtM(J);

        bool errorDecreased = false;
        bool convergedLM = false;
        int LMiteration = 0;

        vector<CCamera> CamerasTmp = Cameras;

        cerr << "Begin LM" << endl;
        while (!errorDecreased && !convergedLM)
        {
            LMiteration++;
            convergedLM = LMiteration >= maxLMIterations;
            //cerr << "LMiteration = " << LMiteration << endl;

            float sigmae2 = sigmae * sigmae;
            
            CMatrix JtJplusInvCp = JtJ + (invCp * sigmae2);
            CMatrix Jtrtmp = Jtr;

            //cerr << "JtJ = " << JtJ << endl;
            //cerr << "Jtr = " << Jtr << endl;
            
            cerr << "Solve Linear System" << endl;
            CMatrix x2 = SolveLinearSystem(JtJplusInvCp, Jtrtmp);

            //cerr << "x = " << x2 << endl;
            
            // set x(1:3) == 0 
            CMatrix x(4 * nImages, 1); 
            x.assign(x2, 3);
            
            // Update parameters
            
            for (int i = 0; i < nImages; i++)
            {
                CamerasTmp[imageID[i]].R = Cameras[imageID[i]].R * VectorRotation( x.submatrix(4 * i, 4 * i + 2, 0, 0) );
                //CamerasTmp[imageID[i]].f = Cameras[imageID[i]].f + x[4 * i + 3];
                CamerasTmp[imageID[i]].f = Cameras[imageID[i]].f + meanf * x[4 * i + 3];
                //cerr << "f[ " << i << "] = " << CamerasTmp[imageID[i]].f << endl;
            }

            // Compute error

            row = 0;

            for (int i1 = 0; i1 < nImages; i1++)
            {
                int im1 = imageID[i1];
                CMatrix Rtmpim1 = CamerasTmp[im1].R;
                float   ftmpim1 = CamerasTmp[im1].f;
                
                for (int i2 = 0; i2 < nImages; i2++)
                {
                    int im2 = imageID[i2]; 
                    CMatrix Rtmpim2 = CamerasTmp[im2].R;
                    float   ftmpim2 = CamerasTmp[im2].f;
                    
                    int nMatches12 = nMatches[im1][im2];

                    if (nMatches12 > 0)
                    {
                        CMatrix &u1 = matches[im1][im2][0];
                        CMatrix &u2 = matches[im1][im2][1];
            
                        CMatrix u1h = homogeneous(u1);

                        CMatrix Kim1inv = K(1/ftmpim1);
                        CMatrix Kim2    = K(ftmpim2);
                    
                        CMatrix ph = Kim2 * Rtmpim2 * ~Rtmpim1 * Kim1inv * u1h;
                        CMatrix p = unhomogeneous(ph);
                        
                        CMatrix r21 = u2 - p;
                        r21.vectorize();
                        
                        r.assign(r21, row);                        
                        
                        row += 2 * nMatches12;
                    }// end if nMatches12 > 0

                }// end for i2

            }// end for i1

            // Compute new error

            error = r.norm() / (float)sqrt((float)totalNMatches);
            //cerr << " error = " << error << endl;
        
            errorDecreased = error < error0;
            
            if (errorDecreased)
            {
                sigmae /= dsigmae;
                Cameras = CamerasTmp;
            }
            else
            {
                sigmae *= dsigmae;
            }
            
        }// end while (!errorDecreased)
        
        converged = ((error < maxError) && (error0 - error < maxDError)) || convergedLM;        
       
    }// end while (!converged && (iteration < maxIterations))

    //cerr << "  error = " << error << endl;
        
}// end CPanorama::BundleAdjust0


void CBundleAdjust::BundleAdjust(CMultiImageMatch &MultiImageMatch, vector<int> imageID, vector<CCamera> &Cameras, 
                                 EErrorFunction errorFunction, float outlierDistance0, EOutlierDistanceUnits outlierDistanceUnits)
{
    // Compute JtJ directly
	
    int   maxIterations = Param.maxIterations;
    int   maxLMIterations = Param.maxLMIterations;
    float sigmae        = Param.sigmae;
    float dsigmae       = Param.dsigmae;
    float maxError      = Param.maxError;
    float maxDError     = Param.maxDError;

    int** nMatches   = MultiImageMatch.nMatches;
    CMatch2D** matches = MultiImageMatch.matches;

    int nImages = (int)imageID.size();
    int nParameters = 4 * nImages - 3;

    // Set prior covariance

    float meanf = 0;
    for (int i = 0; i < nImages; i++)
    {
        meanf += Cameras[imageID[i]].f;
    }
    meanf /= (float)nImages;
    //priorSigmaf = 0.05;
    float priorSigmaf     = Param.priorSigmafFactor * meanf;
    float priorSigmaTheta = Param.priorSigmaTheta;
    
    CMatrix invCp(nParameters, nParameters);
    float priorSigmafInvSq = 1 / (priorSigmaf * priorSigmaf);
    float priorSigmaThetaInvSq = 1 / (priorSigmaTheta * priorSigmaTheta);
    invCp(0, 0) = priorSigmafInvSq;
    for (int i = 0; i < nImages - 1; i++)
    {
        invCp(4 * i + 1, 4 * i + 1) = priorSigmaThetaInvSq;
        invCp(4 * i + 2, 4 * i + 2) = priorSigmaThetaInvSq;
        invCp(4 * i + 3, 4 * i + 3) = priorSigmaThetaInvSq;
        invCp(4 * i + 4, 4 * i + 4) = priorSigmafInvSq;
    }

    // Rotation derivatives
    CMatrix dRdt1(3, 3); dRdt1(1, 2) = -1; dRdt1(2, 1) = 1;
    CMatrix dRdt2(3, 3); dRdt2(0, 2) = 1;  dRdt2(2, 0) = -1;
    CMatrix dRdt3(3, 3); dRdt3(0, 1) = -1; dRdt3(1, 0) = 1;

    int totalNMatches = 0;
    for (int i = 0; i < nImages; i++)
    {
        int im1 = imageID[i];
        for (int j = 0; j < nImages; j++)
        {
            int im2 = imageID[j];
            totalNMatches += nMatches[im1][im2];
        }// end for j
    }// end for i

    //cerr << "totalNMatches = " << totalNMatches << endl;

    // 
    // Compute Initial Error
    //

    float error;
    float outlierDistance;
    float sumrsq = 0;

    for (int i1 = 0; i1 < nImages; i1++)
    {
        int im1 = imageID[i1];
        CMatrix Rtmpim1 = Cameras[im1].R;
        float   ftmpim1 = Cameras[im1].f;
                
        for (int i2 = 0; i2 < nImages; i2++)
        {
            int im2 = imageID[i2]; 
            CMatrix Rtmpim2 = Cameras[im2].R;
            float   ftmpim2 = Cameras[im2].f;

            int nMatches12 = nMatches[im1][im2];

            if (nMatches12 > 0)
            {
                CMatrix &u1 = matches[im1][im2][0];
                CMatrix &u2 = matches[im1][im2][1];
            
                CMatrix u1h = homogeneous(u1);

                CMatrix Kim1inv = K(1/ftmpim1);
                CMatrix Kim2    = K(ftmpim2);
                    
                CMatrix ph = Kim2 * Rtmpim2 * ~Rtmpim1 * Kim1inv * u1h;
                CMatrix p = unhomogeneous(ph);
                        
                CMatrix r21 = u2 - p;

                sumrsq += sum(scalarpow(r21, 2));

            }// end if nMatches12 > 0

        }// end for i2

    }// end for i1

    error = (float)sqrt(sumrsq/(float)totalNMatches);
    if (outlierDistanceUnits == eStandardDeviations)
        outlierDistance = outlierDistance0 * error;
    else
        outlierDistance = outlierDistance0;

    //
    // Optimisation loop
    //

    bool converged = false;
    int iteration = 0;
    float werror; 

    while (!converged && (iteration < maxIterations))
    {
        iteration++;

        //cerr << "iteration = " << iteration << endl;       
        //cerr << "Compute JtJ" << endl;
        
        // Build Jacobean products JtJ, Jtr
        CMatrix JtJ(4 * nImages, 4 * nImages);
        CMatrix Jtr(4 * nImages, 1);
        CMatrix r(2 * totalNMatches, 1);
        
        int row = 0;
 
        for (int i1 = 0; i1 < nImages; i1++)
        {
            int im1 = imageID[i1];
            CMatrix Rim1 = Cameras[im1].R;
            float   fim1 = Cameras[im1].f;
            
            for (int i2 = 0; i2 < nImages; i2++)
            {

                int im2 = imageID[i2];
                CMatrix Rim2 = Cameras[im2].R;
                float   fim2 = Cameras[im2].f;
                
                int nMatches12 = nMatches[im1][im2];

                if (nMatches12 > 0)
                {                                        
                    CMatrix &u1 = matches[im1][im2][0];
                    CMatrix &u2 = matches[im1][im2][1];
                    
                    CMatrix u1h = homogeneous(u1);

                    CMatrix Kim1    = K(fim1);
                    CMatrix Kim2    = K(fim2);
                    CMatrix Kim1inv = K(1/fim1);
                    CMatrix Kim2inv = K(1/fim2);
                    
                    CMatrix ph = Kim2 * Rim2 * ~Rim1 * Kim1inv * u1h;
                    CMatrix p = unhomogeneous(ph);
                    
                    CMatrix r21 = u2 - p;

                    CMatrix fr;
                    CMatrix dfdr;
                    if (errorFunction != eQuadratic)
                    {	
                        RobustErrorFunction(r21, fr, dfdr, outlierDistance, errorFunction);
                        // robustify residual
                        r21 = fr;					
                    }

                    r21.vectorize();
                    r.assign(r21, row);
                               
                    CMatrix dphdtim11 = Kim2 * Rim2 * ~(Rim1 * dRdt1) * Kim1inv * u1h;
                    CMatrix dphdtim12 = Kim2 * Rim2 * ~(Rim1 * dRdt2) * Kim1inv * u1h;
                    CMatrix dphdtim13 = Kim2 * Rim2 * ~(Rim1 * dRdt3) * Kim1inv * u1h;

                    CMatrix dinvKim1dfim1(3, 3);
                    float fim1neginvsq = - 1 / (fim1 * fim1);
                    dinvKim1dfim1(0, 0) = fim1neginvsq; dinvKim1dfim1(1, 1) = fim1neginvsq;
                    
                    CMatrix dphdfim1 = Kim2 * Rim2 * ~Rim1 * dinvKim1dfim1 * u1h;
                    
                    CMatrix dphdtim21 = Kim2 * Rim2 * dRdt1 * ~Rim1 * Kim1inv * u1h;
                    CMatrix dphdtim22 = Kim2 * Rim2 * dRdt2 * ~Rim1 * Kim1inv * u1h;
                    CMatrix dphdtim23 = Kim2 * Rim2 * dRdt3 * ~Rim1 * Kim1inv * u1h;

                    CMatrix dKim2dfim2(3, 3);
                    dKim2dfim2(0, 0) = 1; dKim2dfim2(1, 1) = 1;

                    CMatrix dphdfim2 = dKim2dfim2 * Rim2 * ~Rim1 * Kim1inv * u1h;
                    
                    CMatrix dp1dph(3, nMatches12);
                    dp1dph.assign( scalarpow(ph.row(2), -1), 0);
                    dp1dph.assign( - scalardiv( ph.row(0), scalarpow(ph.row(2), 2)), 2 );

                    CMatrix dp2dph(3, nMatches12);
                    dp2dph.assign( scalarpow(ph.row(2), -1), 1);
                    dp2dph.assign( - scalardiv( ph.row(1), scalarpow(ph.row(2), 2)), 2 );
                    
                    CMatrix dpdtim11(2, nMatches12);
                    CMatrix dpdtim12(2, nMatches12);
                    CMatrix dpdtim13(2, nMatches12);
                    CMatrix dpdfim1(2, nMatches12);

                    dpdtim11.assign( rowsum(scalarmult(dp1dph, dphdtim11)), 0);
                    dpdtim11.assign( rowsum(scalarmult(dp2dph, dphdtim11)), 1);
                    dpdtim12.assign( rowsum(scalarmult(dp1dph, dphdtim12)), 0);
                    dpdtim12.assign( rowsum(scalarmult(dp2dph, dphdtim12)), 1);
                    dpdtim13.assign( rowsum(scalarmult(dp1dph, dphdtim13)), 0);
                    dpdtim13.assign( rowsum(scalarmult(dp2dph, dphdtim13)), 1);
                    
                    dpdfim1.assign( rowsum(scalarmult(dp1dph, dphdfim1)), 0);
                    dpdfim1.assign( rowsum(scalarmult(dp2dph, dphdfim1)), 1);
                    
                    CMatrix dpdtim21(2, nMatches12);
                    CMatrix dpdtim22(2, nMatches12);
                    CMatrix dpdtim23(2, nMatches12);
                    CMatrix dpdfim2(2, nMatches12);

                    dpdtim21.assign( rowsum(scalarmult(dp1dph, dphdtim21)), 0);
                    dpdtim21.assign( rowsum(scalarmult(dp2dph, dphdtim21)), 1);
                    dpdtim22.assign( rowsum(scalarmult(dp1dph, dphdtim22)), 0);
                    dpdtim22.assign( rowsum(scalarmult(dp2dph, dphdtim22)), 1);
                    dpdtim23.assign( rowsum(scalarmult(dp1dph, dphdtim23)), 0);
                    dpdtim23.assign( rowsum(scalarmult(dp2dph, dphdtim23)), 1);
                    
                    dpdfim2.assign( rowsum(scalarmult(dp1dph, dphdfim2)), 0);
                    dpdfim2.assign( rowsum(scalarmult(dp2dph, dphdfim2)), 1);

                    if (errorFunction != eQuadratic)
                    {
                        // robustify derivatives
                        dpdtim11 = scalarmult(dpdtim11, dfdr);
                        dpdtim12 = scalarmult(dpdtim12, dfdr);
                        dpdtim13 = scalarmult(dpdtim13, dfdr);
                        dpdfim1  = scalarmult(dpdfim1,  dfdr);

                        dpdtim21 = scalarmult(dpdtim21, dfdr);
                        dpdtim22 = scalarmult(dpdtim22, dfdr);
                        dpdtim23 = scalarmult(dpdtim23, dfdr);
                        dpdfim2  = scalarmult(dpdfim2,  dfdr);
                        // end robustify derivatives
                    }

                    dpdtim11.vectorize();
                    dpdtim12.vectorize();
                    dpdtim13.vectorize();
                    dpdfim1.vectorize();

                    dpdtim21.vectorize();
                    dpdtim22.vectorize();
                    dpdtim23.vectorize();
                    dpdfim2.vectorize();
                    
                    //
                    // Compute JtJ
                    //
           
                    // JtJ(i1, ...)
         
                    JtJ(4 * i1 + 0, 4 * i1 + 0) += dot(dpdtim11, dpdtim11);
                    JtJ(4 * i1 + 0, 4 * i1 + 1) += dot(dpdtim11, dpdtim12);
                    JtJ(4 * i1 + 0, 4 * i1 + 2) += dot(dpdtim11, dpdtim13);
                    JtJ(4 * i1 + 0, 4 * i1 + 3) += dot(dpdtim11, dpdfim1);
                    
                    JtJ(4 * i1 + 0, 4 * i2 + 0) += dot(dpdtim11, dpdtim21);
                    JtJ(4 * i1 + 0, 4 * i2 + 1) += dot(dpdtim11, dpdtim22);
                    JtJ(4 * i1 + 0, 4 * i2 + 2) += dot(dpdtim11, dpdtim23);
                    JtJ(4 * i1 + 0, 4 * i2 + 3) += dot(dpdtim11, dpdfim2);

                    JtJ(4 * i1 + 1, 4 * i1 + 0) += dot(dpdtim12, dpdtim11);
                    JtJ(4 * i1 + 1, 4 * i1 + 1) += dot(dpdtim12, dpdtim12);
                    JtJ(4 * i1 + 1, 4 * i1 + 2) += dot(dpdtim12, dpdtim13);
                    JtJ(4 * i1 + 1, 4 * i1 + 3) += dot(dpdtim12, dpdfim1);
                    
                    JtJ(4 * i1 + 1, 4 * i2 + 0) += dot(dpdtim12, dpdtim21);
                    JtJ(4 * i1 + 1, 4 * i2 + 1) += dot(dpdtim12, dpdtim22);
                    JtJ(4 * i1 + 1, 4 * i2 + 2) += dot(dpdtim12, dpdtim23);
                    JtJ(4 * i1 + 1, 4 * i2 + 3) += dot(dpdtim12, dpdfim2);

                    JtJ(4 * i1 + 2, 4 * i1 + 0) += dot(dpdtim13, dpdtim11);
                    JtJ(4 * i1 + 2, 4 * i1 + 1) += dot(dpdtim13, dpdtim12);
                    JtJ(4 * i1 + 2, 4 * i1 + 2) += dot(dpdtim13, dpdtim13);
                    JtJ(4 * i1 + 2, 4 * i1 + 3) += dot(dpdtim13, dpdfim1);
                    
                    JtJ(4 * i1 + 2, 4 * i2 + 0) += dot(dpdtim13, dpdtim21);
                    JtJ(4 * i1 + 2, 4 * i2 + 1) += dot(dpdtim13, dpdtim22);
                    JtJ(4 * i1 + 2, 4 * i2 + 2) += dot(dpdtim13, dpdtim23);
                    JtJ(4 * i1 + 2, 4 * i2 + 3) += dot(dpdtim13, dpdfim2);

                    JtJ(4 * i1 + 3, 4 * i1 + 0) += dot(dpdfim1, dpdtim11);
                    JtJ(4 * i1 + 3, 4 * i1 + 1) += dot(dpdfim1, dpdtim12);
                    JtJ(4 * i1 + 3, 4 * i1 + 2) += dot(dpdfim1, dpdtim13);
                    JtJ(4 * i1 + 3, 4 * i1 + 3) += dot(dpdfim1, dpdfim1);
                    
                    JtJ(4 * i1 + 3, 4 * i2 + 0) += dot(dpdfim1, dpdtim21);
                    JtJ(4 * i1 + 3, 4 * i2 + 1) += dot(dpdfim1, dpdtim22);
                    JtJ(4 * i1 + 3, 4 * i2 + 2) += dot(dpdfim1, dpdtim23);
                    JtJ(4 * i1 + 3, 4 * i2 + 3) += dot(dpdfim1, dpdfim2);

                    // JtJ(i2, ...) 

                    JtJ(4 * i2 + 0, 4 * i2 + 0) += dot(dpdtim21, dpdtim21);
                    JtJ(4 * i2 + 0, 4 * i2 + 1) += dot(dpdtim21, dpdtim22);
                    JtJ(4 * i2 + 0, 4 * i2 + 2) += dot(dpdtim21, dpdtim23);
                    JtJ(4 * i2 + 0, 4 * i2 + 3) += dot(dpdtim21, dpdfim2);
                    
                    JtJ(4 * i2 + 0, 4 * i1 + 0) += dot(dpdtim21, dpdtim11);
                    JtJ(4 * i2 + 0, 4 * i1 + 1) += dot(dpdtim21, dpdtim12);
                    JtJ(4 * i2 + 0, 4 * i1 + 2) += dot(dpdtim21, dpdtim13);
                    JtJ(4 * i2 + 0, 4 * i1 + 3) += dot(dpdtim21, dpdfim1);

                    JtJ(4 * i2 + 1, 4 * i2 + 0) += dot(dpdtim22, dpdtim21);
                    JtJ(4 * i2 + 1, 4 * i2 + 1) += dot(dpdtim22, dpdtim22);
                    JtJ(4 * i2 + 1, 4 * i2 + 2) += dot(dpdtim22, dpdtim23);
                    JtJ(4 * i2 + 1, 4 * i2 + 3) += dot(dpdtim22, dpdfim2);
                    
                    JtJ(4 * i2 + 1, 4 * i1 + 0) += dot(dpdtim22, dpdtim11);
                    JtJ(4 * i2 + 1, 4 * i1 + 1) += dot(dpdtim22, dpdtim12);
                    JtJ(4 * i2 + 1, 4 * i1 + 2) += dot(dpdtim22, dpdtim13);
                    JtJ(4 * i2 + 1, 4 * i1 + 3) += dot(dpdtim22, dpdfim1);

                    JtJ(4 * i2 + 2, 4 * i2 + 0) += dot(dpdtim23, dpdtim21);
                    JtJ(4 * i2 + 2, 4 * i2 + 1) += dot(dpdtim23, dpdtim22);
                    JtJ(4 * i2 + 2, 4 * i2 + 2) += dot(dpdtim23, dpdtim23);
                    JtJ(4 * i2 + 2, 4 * i2 + 3) += dot(dpdtim23, dpdfim2);
                    
                    JtJ(4 * i2 + 2, 4 * i1 + 0) += dot(dpdtim23, dpdtim11);
                    JtJ(4 * i2 + 2, 4 * i1 + 1) += dot(dpdtim23, dpdtim12);
                    JtJ(4 * i2 + 2, 4 * i1 + 2) += dot(dpdtim23, dpdtim13);
                    JtJ(4 * i2 + 2, 4 * i1 + 3) += dot(dpdtim23, dpdfim1);

                    JtJ(4 * i2 + 3, 4 * i2 + 0) += dot(dpdfim2, dpdtim21);
                    JtJ(4 * i2 + 3, 4 * i2 + 1) += dot(dpdfim2, dpdtim22);
                    JtJ(4 * i2 + 3, 4 * i2 + 2) += dot(dpdfim2, dpdtim23);
                    JtJ(4 * i2 + 3, 4 * i2 + 3) += dot(dpdfim2, dpdfim2);
                    
                    JtJ(4 * i2 + 3, 4 * i1 + 0) += dot(dpdfim2, dpdtim11);
                    JtJ(4 * i2 + 3, 4 * i1 + 1) += dot(dpdfim2, dpdtim12);
                    JtJ(4 * i2 + 3, 4 * i1 + 2) += dot(dpdfim2, dpdtim13);
                    JtJ(4 * i2 + 3, 4 * i1 + 3) += dot(dpdfim2, dpdfim1);

                    //
                    // Compute Jtr
                    //

                    Jtr[4 * i1 + 0] += dot(dpdtim11, r21);
                    Jtr[4 * i1 + 1] += dot(dpdtim12, r21);
                    Jtr[4 * i1 + 2] += dot(dpdtim13, r21);
                    Jtr[4 * i1 + 3] += dot(dpdfim1, r21);

                    Jtr[4 * i2 + 0] += dot(dpdtim21, r21);
                    Jtr[4 * i2 + 1] += dot(dpdtim22, r21);
                    Jtr[4 * i2 + 2] += dot(dpdtim23, r21);
                    Jtr[4 * i2 + 3] += dot(dpdfim2, r21);
                
                    row += 2 * nMatches12;
                    
                }// end if nMatches12 > 0
                
            }// end for i2
            
        }// end for i1
        
         // Levenberg-Marquardt iteration
        float werror0 = r.norm() / (float)sqrt((float)totalNMatches);
        werror = werror0;
        cerr << "  error = " << werror << endl;
        
        CMatrix JtJ2 = JtJ.submatrix(3, 4 * nImages - 1, 3, 4 * nImages - 1);
        CMatrix Jtr2 = Jtr.submatrix(3, 4 * nImages - 1, 0, 0);
                        
        bool errorDecreased = false;
        bool convergedLM    = false;
        int LMiteration = 0;

        vector<CCamera> CamerasTmp = Cameras;

        while (!errorDecreased && !convergedLM)
        {
            LMiteration++;             
            //cerr << "LMiteration = " << LMiteration << endl;
            
            float sigmae2 = sigmae * sigmae;
            
            CMatrix JtJplusInvCp = JtJ2 + (invCp * sigmae2);
            CMatrix Jtrtmp = Jtr2;

            //cerr << "JtJ = " << JtJ2 << endl;
            //cerr << "Jtr = " << Jtr2 << endl;
            
            //cerr << "Solve Linear System" << endl;
            CMatrix x2 = SolveLinearSystem(JtJplusInvCp, Jtrtmp);

            //cerr << "x = " << x2 << endl;
            
            // set x(1:3) == 0 
            CMatrix x(4 * nImages, 1); 
            x.assign(x2, 3);
            
            // Update parameters
            
            for (int i = 0; i < nImages; i++)
            {
                CamerasTmp[imageID[i]].R = Cameras[imageID[i]].R * VectorRotation( x.submatrix(4 * i, 4 * i + 2, 0, 0) );
                CamerasTmp[imageID[i]].f = Cameras[imageID[i]].f + x[4 * i + 3];
                //ftmp[imageID[i]] = f[imageID[i]] + meanf * x[4 * i + 3];
                //cerr << "f[ " << i << "] = " << ftmp[imageID[i]] << endl;
            }

            // Compute error

            float sumrsq = 0;
            float sumwrsq = 0;

            for (int i1 = 0; i1 < nImages; i1++)
            {
                int im1 = imageID[i1];
                CMatrix Rtmpim1 = CamerasTmp[im1].R;
                float   ftmpim1 = CamerasTmp[im1].f;
                
                for (int i2 = 0; i2 < nImages; i2++)
                {
                    int im2 = imageID[i2]; 
                    CMatrix Rtmpim2 = CamerasTmp[im2].R;
                    float   ftmpim2 = CamerasTmp[im2].f;

                    int nMatches12 = nMatches[im1][im2];

                    if (nMatches12 > 0)
                    {
                        CMatrix &u1 = matches[im1][im2][0];
                        CMatrix &u2 = matches[im1][im2][1];
            
                        CMatrix u1h = homogeneous(u1);

                        CMatrix Kim1inv = K(1/ftmpim1);
                        CMatrix Kim2    = K(ftmpim2);
                    
                        CMatrix ph = Kim2 * Rtmpim2 * ~Rtmpim1 * Kim1inv * u1h;
                        CMatrix p = unhomogeneous(ph);
                        
                        CMatrix r21 = u2 - p;

                        float rsq = sum(scalarpow(r21, 2));

                        float wrsq;					
						
                        if (errorFunction != eQuadratic)
                        {														
                            CMatrix fr;
                            CMatrix dfdr;	
                            RobustErrorFunction(r21, fr, dfdr, outlierDistance, errorFunction);
                            wrsq = sum(scalarpow(fr, 2));
                        }
                        else
                        {
                            wrsq = rsq;
                        }

                        sumrsq += rsq;
                        sumwrsq += wrsq;

                    }// end if nMatches12 > 0

                }// end for i2

            }// end for i1

            // Compute new error

            error  = (float)sqrt(sumrsq/(float)totalNMatches);
            werror = (float)sqrt(sumwrsq/(float)totalNMatches);
        
            errorDecreased = werror < werror0;
            
            if (errorDecreased)
            {
                sigmae /= dsigmae;
                Cameras = CamerasTmp;
            }
            else
            {
                sigmae *= dsigmae;
            }
            
            convergedLM = LMiteration > maxLMIterations;            
            if (convergedLM)
                cerr << "  BundleAdjust: maximum LM iterations reached " << endl;
            
        }// end while (!errorDecreased && !convergedLM)

        if (outlierDistanceUnits == eStandardDeviations)
            outlierDistance = outlierDistance0 * error;

        converged = ((werror < maxError) && (werror0 - werror < maxDError)) || convergedLM;        
       
    }// end while (!converged && (iteration < maxIterations))

    cerr << "  error = " << werror << endl;
        
}// end CPanorama::BundleAdjust

void NormaliseJ(CMatrix &J, float meanf)
{
    int nRows = J.nRows;
    int nCols = J.nCols;
    for (int i = 0; i < nRows; i++)
    {
        for (int j = 3; j < nCols; j += 4)
        {
            J(i, j) *= meanf;
        }
    }
}

void CBundleAdjust::RobustErrorFunction(CMatrix &r, CMatrix &fr, CMatrix &dfdr, float outlierDistance, EErrorFunction errorFunction)
{
    // Compute robust error function and its derivatives

    int nRows = r.nRows;
    int nCols = r.nCols;
    assert(nRows == 2);

    fr.Create(nRows, nCols);
    dfdr.Create(nRows, nCols);

    float outlierDistance2 = outlierDistance * outlierDistance;

    for (int col = 0; col < nCols; col++)
    {
        float r0 = r(0, col);
        float r1 = r(1, col);
		
        float rc = (float)sqrt(r0 * r0 + r1 * r1);
        float frc;
        float dfdrc;

        if (rc < outlierDistance)
        {
            frc   = rc;
            dfdrc = 1;
        }
        else // rc >= outlierDistance
        {
            if (errorFunction == eHuber)
            {
                frc   = (float)sqrt(2 * outlierDistance * fabs(rc) - outlierDistance2);
                dfdrc = sgn(rc) * outlierDistance / (float)sqrt(2 * outlierDistance * fabs(rc) - outlierDistance2);
            }
            else // errorFunction == eTruncatedQuadratic
            {
                frc = 0;
                dfdrc = 0;
            }
        }

        fr(0, col) = r0 * frc / rc;
        fr(1, col) = r1 * frc / rc;
        dfdr(0, col) = dfdrc;
        dfdr(1, col) = dfdrc;
    }// end for col
	
}
