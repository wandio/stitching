//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// RANSAC.cpp

#include "assert.h"

#include "RANSAC.h"
#include "Matrix.h"
#include "MatrixHelpers.h"
#include "BoolVec.h"
#include <functional>
#include <algorithm>

typedef struct {int nMatches; int imageID;} SnMatchesID;
bool operator>(SnMatchesID nm1, SnMatchesID nm2) { return (nm1.nMatches > nm2.nMatches); }

void CRANSAC::Create(CMatch2D** matches, int** nMatches, int nImages, vector<SImageSize> &imageSize, CPanoramaStatus &status)
{
    
    cerr << "[ Performing RANSAC ]" << endl;
    int nMatchesPerImage = min(Param.nMatchesPerImage, nImages - 1);
    float alpha          = Param.alpha;
    float beta           = Param.beta;

    vector< vector<bool> > isValid;
    isValid.resize(nImages);
    for (int i = 0; i < nImages; i++)
    { isValid[i].resize(nImages);
        for (int j = 0; j < nImages; j++)
        { isValid[i][j] = false; } }

    for (int im1ID = 0; im1ID < nImages; im1ID++)
    {
        cerr << "  Image " << im1ID << endl;
        status.Update(eRANSAC, im1ID, nImages - 1);

        vector<SnMatchesID> nMatchesSort;
        nMatchesSort.resize(nImages);
        for (int j = 0; j < nImages; j++)
        {
            nMatchesSort[j].nMatches = nMatches[im1ID][j];
            nMatchesSort[j].imageID = j;
        }
        
        // sort in descending order of nMatches
        sort(nMatchesSort.begin(), nMatchesSort.end(), greater<SnMatchesID>());
       
        // perform RANSAC for the best potential matches
        for (int j = 0; j < nMatchesPerImage; j++)
        {
            int im2ID = nMatchesSort[j].imageID;
            int nMatches12 = nMatchesSort[j].nMatches;
            CMatch2D& matches12 = matches[im1ID][im2ID];
           
            CMatrix S(3, 3);
            vector<int> inliers = RANSACSimilarity( matches12, nMatches12, S);
            
            int nInliers = (int)inliers.size();
            int nOverlap = ComputeNOverlapS(matches12, nMatches12, S, imageSize[im1ID], imageSize[im2ID]);
			
            int minNInliers = (int)(alpha + beta * nOverlap);

            cerr << " nInliers = " << nInliers;
            cerr << ", nOverlap = " << nOverlap;
            cerr << ", minNInliers = " << minNInliers;

            if (nInliers > minNInliers)
            {
                cerr << " - ACCEPT" << endl;        
                isValid[im1ID][im2ID] = true;
                CMatch2D matches12c(nInliers);
                
                for (int k = 0; k < nInliers; k++)
                {
                    matches12c[0](0, k) = matches12[0](0, inliers[k]);
                    matches12c[0](1, k) = matches12[0](1, inliers[k]);
                    matches12c[1](0, k) = matches12[1](0, inliers[k]);
                    matches12c[1](1, k) = matches12[1](1, inliers[k]);
                }
                matches[im1ID][im2ID] = matches12c;
                nMatches[im1ID][im2ID] = nInliers;
            }// end if nInliers
            else
                cerr << " - REJECT" << endl;

        }// end for j
        
    }// end for im1ID
    
    
    // delete invalid matches
    for (int i = 0; i < nImages; i++)
    {
        for (int j = 0; j < nImages; j++)
        {
            if (!isValid[i][j])
            {
                nMatches[i][j] = 0;
                CMatch2D null;
                matches[i][j] = null;
            }
        }
    }
    
}// end RANSAC

vector<int> CRANSAC::RANSACSimilarity(CMatch2D &matches12, int nMatches12, CMatrix &bestS)    
{
//    cerr << "RANSACSImilarity" << endl;

    float epsilon     = Param.epsilon;
    int maxIterations = Param.maxIterations;
    float minDist     = Param.minDistComputeSimilarity;
    float maxScaleS   = Param.maxScaleS;

    float maxScaleS2 = maxScaleS * maxScaleS;
    float minScaleS2 = 1.0f / (maxScaleS * maxScaleS);

    int bestNInliers = 0;
    vector<bool> bestInliers;
    bestInliers.resize(nMatches12);
    bestS.zeros();
    vector<bool> inliers;
    inliers.resize(nMatches12);
	
    for (int iteration = 0; iteration < maxIterations; iteration++)
    {

        // Select random sample
        int r1 = rand()%nMatches12;
        int r2 = rand()%nMatches12;
    
        float u1 = matches12[0](0, r1);
        float v1 = matches12[0](1, r1);
        float u2 = matches12[0](0, r2);
        float v2 = matches12[0](1, r2);
        float x1 = matches12[1](0, r1);
        float y1 = matches12[1](1, r1);
        float x2 = matches12[1](0, r2);
        float y2 = matches12[1](1, r2);
    
        // Compute similarity transform
        CMatrix S = ComputeSimilarity(u1, v1, x1, y1, u2, v2, x2, y2, minDist);
        //cerr << "S = " << S << endl;

	bool SisValid = false;
	float scale2 = (S(0, 0) * S(1, 1)) - (S(0, 1) * S(1, 0));
	if ((scale2 > minScaleS2) && (scale2 < maxScaleS2))
            SisValid = true;

	if (SisValid)
	{

	    // Find number of matches that are consistent with S
            int nInliers = ConsistentS(matches12, nMatches12, S, epsilon, inliers);
    
            if (nInliers > bestNInliers)
            {
                //cerr << " nInliers = " << nInliers << endl;
                bestNInliers = nInliers;
                bestInliers  = inliers;
                bestS        = S;
            }

	}// end if SisValid

    }// end for iteration

    // create vector of inlier IDs
    vector<int> inliersID;
    inliersID.reserve(bestNInliers);
    for (int i = 0; i < nMatches12; i++)
    {
        if (bestInliers[i])
            inliersID.push_back(i);
    }

    return(inliersID);
}


int CRANSAC::Consistent(CMatch2D &matches, int nMatches, CMatrix S, float epsilon, vector<bool> &consistent)
//
// return indices of matches that are consistent with S within epsilon
//
{

//    cerr << "Consistent" << endl;

    float epsilon2 = epsilon * epsilon;
    int nConsistent = 0;

    CMatrix &u1 = matches[0];
    CMatrix &u2 = matches[1];

    CMatrix u1h  = homogeneous(u1);
    CMatrix u1th = S * u1h;
    CMatrix u1t  = unhomogeneous(u1th);

    CMatrix diff = u2 - u1t;
    CMatrix diffsq = scalarpow(diff, 2);
    CMatrix sumdiffsq1 = rowsum(diffsq);
    
    CMatrix Sinv = InvertSimilarity(S);
    
    CMatrix u2h  = homogeneous(u2);
    CMatrix u2ht = Sinv * u2h;
    CMatrix u2t  = unhomogeneous(u2ht);
    
    diff   = u2t - u1;
    diffsq = scalarpow(diff, 2);
    CMatrix sumdiffsq2 = rowsum(diffsq);
 
    
    for (int i = 0; i < nMatches; i++)
    {
        float dist1i = sumdiffsq1(0, i);
        float dist2i = sumdiffsq2(0, i);
                
        if ((dist1i < epsilon2) && (dist2i < epsilon2))
        { consistent[i] = true; nConsistent++; }        
        else
        { consistent[i] = false; }

    }// end for i
    
    return(nConsistent);

}// end Consistent


int CRANSAC::ConsistentS(CMatch2D &matches, int nMatches, CMatrix S, float epsilon, vector<bool> &consistent)
//
// return indices of matches that are consistent with S within epsilon
//
{
    float epsilon2 = epsilon * epsilon;
    int nInconsistent = 0;
    for (int m = 0; m < nMatches; m++)
        consistent[m] = true;

    CMatrix &u1 = matches[0];
    CMatrix &u2 = matches[1];
	
    float a = S(0, 0);
    float b = S(0, 1);
    float c = S(0, 2);
    float d = S(1, 2);
    assert(S(1, 1) == a);
    assert(S(1, 0) == -b);
    assert(S(2, 0) == 0);
    assert(S(2, 1) == 0);
    assert(S(2, 2) == 1);

    for (int m = 0; m < nMatches; m++)
    {
        float u10 = u1(0, m);
        float u11 = u1(1, m);
        float u1t0 =  a * u10 + b * u11 + c;
        float u1t1 = -b * u10 + a * u11 + d;
        float u20 = u2(0, m);
        float u21 = u2(1, m);
		
        float d1 = u1t0 - u20;
        float d2 = u1t1 - u21;
        float diffsq = d1 * d1 + d2 * d2;

        if (diffsq > epsilon2)
        {
            consistent[m] = false;
            nInconsistent++;
        }
    }// end for m

    CMatrix Sinv = InvertSimilarity(S);

    a = Sinv(0, 0);
    b = Sinv(0, 1);
    c = Sinv(0, 2);
    d = Sinv(1, 2);
    assert(Sinv(1, 1) == a);
    assert(Sinv(1, 0) == -b);
    assert(Sinv(2, 0) == 0);
    assert(Sinv(2, 1) == 0);
    assert(Sinv(2, 2) == 1);

    for (int m = 0; m < nMatches; m++)
    {
        if (consistent[m])
        {
            float u20 = u2(0, m);
            float u21 = u2(1, m);
            float u2t0 =  a * u20 + b * u21 + c;
            float u2t1 = -b * u20 + a * u21 + d;
            float u10 = u1(0, m);
            float u11 = u1(1, m);
		
            float d1 = u2t0 - u10;
            float d2 = u2t1 - u11;
            float diffsq = d1 * d1 + d2 * d2;

            if (diffsq > epsilon2)
            {
                consistent[m] = false;
                nInconsistent++;
            }
        }// end if consistent[m]
    }// end for m

    int nConsistent = nMatches - nInconsistent;
    
    return(nConsistent);

}// end Consistent

int CRANSAC::ComputeNOverlapS(CMatch2D &matches, int nMatches, CMatrix S, SImageSize imageSize1, SImageSize imageSize2)
//
// return number of matches where both interest points project inside the corresponding image under S
//
{
    int nOutsideOverlap = 0;	
    CBoolVec inOverlap;
    inOverlap.resize(nMatches);
    for (int m = 0; m < nMatches; m++)
        inOverlap[m] = true;

    CMatrix &u1 = matches[0];
    CMatrix &u2 = matches[1];
	
    float a = S(0, 0);
    float b = S(0, 1);
    float c = S(0, 2);
    float d = S(1, 2);
    assert(S(1, 1) == a);
    assert(S(1, 0) == -b);
    assert(S(2, 0) == 0);
    assert(S(2, 1) == 0);
    assert(S(2, 2) == 1);

    for (int m = 0; m < nMatches; m++)
    {
        float u10 = u1(0, m);
        float u11 = u1(1, m);
        float u1t0 =  a * u10 + b * u11 + c;
        float u1t1 = -b * u10 + a * u11 + d;

        if ((u1t0 > 0 && u1t0 < imageSize2.nRows) &&
            (u1t1 > 0 && u1t1 < imageSize2.nCols))
        {
            // do nothing
        }
        else
        {
            inOverlap[m] = false;
            nOutsideOverlap++;
        }
    }// end for m

    CMatrix Sinv = InvertSimilarity(S);

    a = Sinv(0, 0);
    b = Sinv(0, 1);
    c = Sinv(0, 2);
    d = Sinv(1, 2);
    assert(Sinv(1, 1) == a);
    assert(Sinv(1, 0) == -b);
    assert(Sinv(2, 0) == 0);
    assert(Sinv(2, 1) == 0);
    assert(Sinv(2, 2) == 1);

    for (int m = 0; m < nMatches; m++)
    {
        if (inOverlap[m])
        {
            float u20 = u2(0, m);
            float u21 = u2(1, m);
            float u2t0 =  a * u20 + b * u21 + c;
            float u2t1 = -b * u20 + a * u21 + d;
			
            if ((u2t0 > 0 && u2t0 < imageSize1.nRows) &&
                (u2t1 > 0 && u2t1 < imageSize1.nCols))
            {
                // do nothing
            }
            else
            {
                inOverlap[m] = false;
                nOutsideOverlap++;
            }		
			
        }// end if inOverlap[m]
    }// end for m

    int nOverlap = nMatches - nOutsideOverlap;
    
    return(nOverlap);

}// end ComputeNOverlapS
