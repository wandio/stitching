//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// BoolVec.h

#pragma once

#include <vector>
#include <iostream>

using namespace std;

class CBoolVec : public vector<bool>
{
public:
    CBoolVec() { };
    ~CBoolVec() { };

    void operator=(bool b);
    bool operator==(bool b);
    bool operator!=(bool b);

    int FirstNonZeroElement(void);
    vector<int> Find(void); 
    void Set(vector<int> v);

    void XOr(CBoolVec &b);
    
};

ostream& operator<<(ostream& os, CBoolVec &b);
