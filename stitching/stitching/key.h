// key.h

// COMMERCIAL version

/* Copyright (c) 2003. David G. Lowe, University of British Columbia.
   This code may be used, distributed, or modified only under license
   from the University of British Columbia.  This notice must be retained
   in all copies.
*/

#ifndef _KEY_H
#define _KEY_H

#include "defs.h"
#include "util.h"

/* This structure describes a keypoint that has been found in an image.
*/
typedef struct KeypointSt {
    double row, col;          /* Row, column location relative to input image.  */
    double scale;             /* Scale (in sigma units for smaller DOG filter). */
    double ori;               /* Orientation in radians (-PI to PI). */
    unsigned char *ivec;      /* Vector of gradient samples for indexing.*/
    struct KeypointSt *next;  /* Links all keypoints for an image. */
    struct KeypointSt *tnext; /* Links all keypoints in a tree structure */
    double distsq;            /* Used in k-d tree */ 
    int imageID;              // imageID of this keypoint 
    int interestPointID;      // Interest point ID of this keypoint 
    struct KeypointSt **neighbours; /* Nearest Neighbours of this keypoint */
    int numNN;                /* Number of nearest neighbours for this keypoint */
    double strength;          // DOG maxima value for this keypoint (NOT USED)
} *Keypoint;

Keypoint GetKeypoints(Image image, int SkipLevels0);

#endif
