//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// MultiImageMatch.h

#pragma once

#include "Image.h"
#include "Match2D.h"
#include "SIFTMatch.h"
#include "RANSAC.h"
#include "ConnectedComponent.h"
#include "PanoramaStatus.h"

#include <vector>
using namespace std;

class CMultiImageMatch
{
public:
    CMultiImageMatch();
    ~CMultiImageMatch();
	
    CMultiImageMatch(CSIFTParameters SIFTParameters, CRANSACParameters RANSACParameters);

    void Create(string imageDir, vector<string> filenames, vector<CImage> &images, vector<SImageSize> &imageSize0, vector<int> &subsampleLevel, vector<CImage> &thumbs, vector<int> &thumbSubsampleLevel, int &SIFTImageMemory, CPanoramaStatus &status);
    
    CMatch2D **matches;
    // CMatch2D m2D = matches[i][j] 
    // CMatrix u1 = m2D[0] 
    // CMatrix u2 = m2D[1]
    // u1(0, k) = image 1 kth match row 
    // u1(1, k) = image 1 kth match col
    // u2(0, k) = image 2 kth match row
    // u2(1, k) = image 2 kth match col
    int **nMatches;
    // nMatches[i][j] is the number of image matches between images i and j
    int nImages;
    // nImage is the number of images

    CConnectedComponent ConnectedComponent;
    // Connected components of image matches
    // ConnectedComponent[i][j] is the jth image in the ith connected component    
    
    CSIFTMatch SIFTMatch;
    CRANSAC    RANSAC;

    int nFeatures; // the total number of SIFT features extracted
    int nMatchesTotal(void); // return the total number of matches
    void WriteToFile(void);
};
