//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// SIFTMatch.h

#pragma once

#include "Parameter.h"
#include "Image.h"
#include "Match2D.h"
#include <vector>
#include "PanoramaStatus.h"

class CSIFTMatch
{
 public:
    CSIFTMatch() { }
    ~CSIFTMatch() { }

    void Create(string imageDir, vector<string> filenames, vector<CImage> &images, vector<SImageSize> &imageSize0, vector<int> &subsampleLevel, vector<CImage> &thumbs, vector<int> &thumbSubsampleLevel, 
		CMatch2D** matches, int** nMatches, int &SIFTImageMemory, int &nFeatures, CPanoramaStatus &status);
        
    CSIFTParameters Param;    
};
