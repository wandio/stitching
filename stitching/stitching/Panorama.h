//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// Panorama.h

#pragma once

#include "Image.h"
#include "ByteImage.h"
#include "Camera.h"
#include "MultiImageMatch.h"
#include "BundleAdjust.h"
#include "Parameter.h"
#include "PanoramaStatus.h"

class CPanorama
{
public:
    CPanorama();
    ~CPanorama();
    
    void Create(string imageDir0, vector<string> filenames0, CByteImage &panoImage); // Create from vector of image filenames
    void Create(string imageDir0, string filename, CByteImage &panoImage); // Create from file
                           
    vector<CCamera> Cameras;
    string imageDir;
    vector<string> filenames;
    vector<SImageSize> imageSize0;    // original image sizes
    vector<CImage> images;            // SIFT images (may be deleted if too large)
    vector<int> subsampleLevel;       // number of factor 2 subsampling operations for SIFT images
    vector<CImage> thumbs;            // thumbnail images used for gain compensation
    vector<int> thumbSubsampleLevel;  // number of factor 2 subsampling operations for thumbnail images
    int SIFTImageMemory;              // memory taken up by SIFT and thumbnail images
    vector<int> panoramaImageID;

    bool isValid;
    bool needRealign;
    CPanoramaStatus status;

    CSIFTParameters   SIFTParameters;
    CRANSACParameters RANSACParameters;
    CBundleParameters BundleParameters;
    CRenderParameters RenderParameters;
    CSystemParameters SystemParameters;

    void Render(CByteImage &panoImage);

    void resetParameters(void);
    void NormaliseMatches(CMultiImageMatch &MultiImageMatch, vector<SImageSize> &imageSize);    
    void BestNextImage(int** nMatches, int nImages, vector<int> &panorama, vector<bool> &isRegistered, int &bestRegIm, int &bestNonRegIm);
    void BestNextImage0(int** nMatches, int nImages, vector<int> &panorama, int &bestRegIm, int &bestNonRegIm); 
    void AlignBasic(vector<int> &panoramaImageID); // rotate images to render coordinates
    void AlignUpVector(vector<int> &panoramaImageID); // rotate images so that the up direction is perpendicular to the plane of camera horizons
    void PreRotateImages(CMatrix R, vector<int> &panorama); // rotate all images by R
    void PostRotateImages(CMatrix R, vector<int> &panorama); // rotate all images by R
};

ostream& operator<<(ostream& os, const CPanorama &pano);
istream& operator>>(istream& is, CPanorama &pano);
