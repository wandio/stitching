//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// ConnectedComponent.cpp

#include <functional>
#include <algorithm>

#include "ConnectedComponent.h"

typedef struct {int nImages; int componentID;} SComponentID;
bool operator>(SComponentID c1, SComponentID c2) { return (c1.nImages > c2.nImages); }

void CConnectedComponent::Create(int** nMatches, int nImages)
{
    cerr << "[ Finding connected components ]" << endl;

    vector< vector<int> > ConnectedComponent0;
    vector< vector<int> > &ConnectedComponent = data;
    
    CBoolVec nodesTODO;
    nodesTODO.resize(nImages);
    nodesTODO = true;

    int nComp = 0;
    
    while( nodesTODO != false )
    {
        CBoolVec component;
        component.resize(nImages);
        component = false;
        int nodeNext = nodesTODO.FirstNonZeroElement();
        component[nodeNext] = true;

        bool componentDone = false;
        
        while (!componentDone)
        {
            CBoolVec componentOld = component;
            component = AddNeighbours(component, nMatches, nImages);
            componentDone = (component == componentOld);
        }
        
        nodesTODO.XOr(component); // remove component from nodesTODO

        vector<int> componentIntVec = component.Find();
        int componentSize = (int)componentIntVec.size();        
        if (componentSize > 1)
            ConnectedComponent0.push_back(componentIntVec);

    }

    // sort components in descending order of size    
    vector<SComponentID> componentSort;
    int nComponents = (int)ConnectedComponent0.size();
    for (int i = 0; i < nComponents; i++)
    {
        SComponentID cid;
        cid.nImages     = (int)ConnectedComponent0[i].size();
        cid.componentID = i;
        componentSort.push_back(cid);
    }
    sort(componentSort.begin(), componentSort.end(), greater< SComponentID >());
    
    for (int i = 0; i < nComponents; i++)
    {
        ConnectedComponent.push_back(ConnectedComponent0[componentSort[i].componentID]);
    }
    
    // Output connected components found
    cerr << "[ Found " << nComponents << " connected components ]" << endl;
    for (int i = 0; i < nComponents; i++)
    {
        vector<int> &component = ConnectedComponent[i];
        cerr << " ";
        for (int j = 0; j < (int)component.size(); j++)
        {
            cerr << component[j] << " ";
        }
        cerr << endl;
    }           
}

CBoolVec CConnectedComponent::AddNeighbours(CBoolVec b, int** nMatches, int nImages)
// add all neighbours which are connected to the non-zero elements in b
{
    CBoolVec neigh = b;
    int index = 0;
    CBoolVec::iterator i = b.begin();
    while (i != b.end())
    {
        if (*i == true)
        {
            for (int im = 0; im < nImages; im++)
            {
                if ((nMatches[index][im] > 0) || (nMatches[im][index] > 0))
                {
                    neigh[im] = true;
                }
            }
        }// end if *i
        i++;
        index++;
    }// end while
    
    return(neigh);
}
