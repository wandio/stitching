//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// PanoramaStatusNull.cpp

#include "PanoramaStatusNull.h"

void CPanoramaStatus::Update2(int count, int maxCount)
{
    /* Do nothing */
}

void CPanoramaStatus::Update(EStitchingStage stitchStage, int count, int maxCount)
{	
    /* Do nothing */
}
