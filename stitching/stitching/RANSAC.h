//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// RANSAC.h

#pragma once

#include "Parameter.h"
#include "Match2D.h"
#include "Matrix.h"
#include "PanoramaStatus.h"
#include "Image.h"
#include <vector>

class CRANSAC
{
public:
    CRANSAC()  { }
    ~CRANSAC() { }

    void Create(CMatch2D** matches, int** nMatches, int nImages, vector<SImageSize> &imageSize, CPanoramaStatus &status);
    // Find matches that are consistent with S
    int Consistent(CMatch2D &matches, int nMatches, CMatrix S, float epsilon, vector<bool> &consistent); // S can be homography
    int ConsistentS(CMatch2D &matches, int nMatches, CMatrix S, float epsilon, vector<bool> &consistent); // S is similarity
    int ComputeNOverlapS(CMatch2D &matches12, int nMatches12, CMatrix S, SImageSize imageSize1, SImageSize imageSize2); // S is similarity

    vector<int> RANSACSimilarity(CMatch2D &matches12, int nMatches12, CMatrix &S);    

    CRANSACParameters Param;    
};
