//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// PanoramaStatusNull.h

#pragma once

enum EStitchingStage
{
    eSIFTExtract,
    eSIFTMatch,
    eRANSAC,
    eBundleAdjust,
    eBundleAdjustFinal,
    eGainCompensation,
    eRender,
    eBlend
};

class CPanoramaStatus
{
public:
CPanoramaStatus() :
    renderOnly(false)
    { }
    ~CPanoramaStatus() { }

    void Update(EStitchingStage stitchStage, int count, int maxCount);
    void Update2(int count, int maxCount);

    bool renderOnly;
};

