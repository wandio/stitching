//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// Matrix.cpp

#include "Matrix.h"
#include "math.h"
#include "assert.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

CMatrix::CMatrix()
{
    nRows = 0;
    nCols = 0;
    data = NULL;
}

CMatrix::~CMatrix()
{
    delete[] data;
}

CMatrix::CMatrix(int rows, int cols)
{
    data = NULL;
    Create(rows, cols);
}

CMatrix::CMatrix(const CMatrix &M)
{
    data = NULL;
    this->operator=(M);     
}

void CMatrix::operator=(const CMatrix &M)
{
    Create(M.nRows, M.nCols);
    memcpy(data, M.data, nRows * nCols * sizeof(float));       
}

void CMatrix::Create(int rows, int cols)
{
    delete[] data;
    nRows = rows;
    nCols = cols;
    data = new float[nRows * nCols];
    zeros();
}

float& CMatrix::operator()(int row, int col)
{
    return data[row + col * nRows];
}

float& CMatrix::operator[](int row)
{
    return data[row];
}

const float CMatrix::operator()(int row, int col) const
{
    return data[row + col * nRows];
}

const float CMatrix::operator[](int row) const
{
    return data[row];
}

int CMatrix::rowStride(void) const
{
    return(1);
}

int CMatrix::colStride(void) const
{
    return(nRows);
}

void CMatrix::assign(const CMatrix &M1, int row, int col)
// copy the matrix M1 to row, col
{
    assert(row + M1.nRows <= nRows); 
    assert(col + M1.nCols <= nCols);

    CMatrix &M = *this;

    for (int i = 0; i < M1.nRows; i++)
    {
        for (int j = 0; j < M1.nCols; j++)
        {
            M(row + i, col + j) = M1(i, j);
        }
    }
}

CMatrix operator+(const CMatrix &M1, const CMatrix &M2)
{
    CMatrix M;

    assert(M1.nRows == M2.nRows);
    assert(M1.nCols == M2.nCols);
    
    int nRows = M1.nRows; int nCols = M1.nCols;

    M.Create(nRows, nCols);
      
    for (int i = 0; i < nRows * nCols; i++)
    {
        M.data[i] = M1.data[i] + M2.data[i];
    }

    return(M);
}

CMatrix operator-(const CMatrix &M1, const CMatrix &M2)
{
    CMatrix M;

    assert(M1.nRows == M2.nRows);
    assert(M1.nCols == M2.nCols);
    
    int nRows = M1.nRows; int nCols = M1.nCols;

    M.Create(nRows, nCols);
      
    for (int i = 0; i < nRows * nCols; i++)
    {
        M.data[i] = M1.data[i] - M2.data[i];
    }

    return(M);
}

CMatrix operator*(const CMatrix &M1, const CMatrix &M2)
{
    CMatrix M;
    
    assert(M1.nCols == M2.nRows);
    
    int n = M1.nCols;
    
    M.Create(M1.nRows, M2.nCols);

    for (int i = 0; i < M.nRows; i++)
    {
        for (int j = 0; j < M.nCols; j++)
        {
            for (int k = 0; k < n; k++)
            {
                M(i, j) += M1(i, k) * M2(k, j);
            }
        }
    }

    return(M);
}

CMatrix operator~(const CMatrix &M1)
{
    CMatrix M;
    
    M.Create(M1.nCols, M1.nRows);
    
    for (int i = 0; i < M.nRows; i++)
    {
        for (int j = 0; j < M.nCols; j++)
        {
            M(i, j) = M1(j, i);
        }
    }
    
    return(M);
}

CMatrix operator-(const CMatrix &M1)
{
    CMatrix M = M1 * -1;
    return(M);
}

CMatrix operator>(const CMatrix &M1, float f)
{
    CMatrix M(M1.nRows, M1.nCols);
    
    for (int i = 0; i < M.nRows * M.nCols; i++)
    {
        if (M.data[i] > f)
            M.data[i] = 1;
        else
            M.data[i] = 0;
    }
    
    return(M);
}

CMatrix operator<(const CMatrix &M1, float f)
{
    CMatrix M(M1.nRows, M1.nCols);
    
    for (int i = 0; i < M.nRows * M.nCols; i++)
    {
        if (M.data[i] < f)
            M.data[i] = 1;
        else
            M.data[i] = 0;
    }
    
    return(M);
}

CMatrix operator*(const CMatrix &M1, float f)
{
    CMatrix M2 = M1;
    for (int i = 0; i < M2.nRows * M2.nCols; i++)
    {
        M2.data[i] = M1.data[i] * f;
    }
    return(M2);
}

CMatrix operator/(const CMatrix &M1, float f)
{
    CMatrix M2 = M1;
    for (int i = 0; i < M2.nRows * M2.nCols; i++)
    {
        M2.data[i] = M1.data[i] / f;
    }
    return(M2);
}

CMatrix operator+(const CMatrix &M1, float f)
{
    CMatrix M2 = M1;
    for (int i = 0; i < M2.nRows * M2.nCols; i++)
    {
        M2.data[i] = M1.data[i] + f;
    }
    return(M2);
}

CMatrix operator-(const CMatrix &M1, float f)
{
    CMatrix M2 = M1;
    for (int i = 0; i < M2.nRows * M2.nCols; i++)
    {
        M2.data[i] = M1.data[i] - f;
    }
    return(M2);
}

CMatrix CMatrix::submatrix(int row1, int row2, int col1, int col2) const
{
    const CMatrix &M1 = *this;

    assert(row1 <= row2);
    assert(col1 <= col2);
    assert(row1 >= 0);
    assert(col1 >= 0);
    assert(row2 <= M1.nRows - 1);
    assert(col2 <= M1.nCols - 1);
    
    CMatrix M(row2 - row1 + 1, col2 - col1 + 1);
    
    for (int i = 0; i < M.nRows; i++)
    {
        for (int j = 0; j < M.nCols; j++)
        {
            M(i, j) = M1(i + row1, j + col1);
        }
    }
    return(M);
}

CMatrix CMatrix::row(int row)
{
    assert(row >= 0);
    assert(row <= nRows - 1);
    
    CMatrix &M1 = *this;
    CMatrix M2(1, nCols);
    
    for (int i = 0; i < M1.nCols; i++)
    {
        M2(0, i) = M1(row, i);
    }
    return(M2);
}

CMatrix CMatrix::col(int col)
{
    assert(col >= 0);
    assert(col <= nCols - 1);

    CMatrix &M1 = *this;
    CMatrix M2(M1.nRows, 1);
    
    for (int i = 0; i < M1.nCols; i++)
    {
        M2(i, 0) = M1(i, col);
    }
    return(M2);
}
    
CMatrix scalarmult(const CMatrix &M1, const CMatrix &M2)
{
    assert(M1.nRows == M2.nRows);
    assert(M1.nCols == M2.nCols);
    
    CMatrix M(M1.nRows, M1.nCols);
    
    for (int i = 0; i < M.nRows * M.nCols; i++)
    {
        M.data[i] = M1.data[i] * M2.data[i];
    }
    
    return(M);
}

CMatrix scalardiv(const CMatrix &M1, const CMatrix &M2)
{
    assert(M1.nRows == M2.nRows);
    assert(M1.nCols == M2.nCols);
    
    CMatrix M(M1.nRows, M1.nCols);
    
    for (int i = 0; i < M.nRows * M.nCols; i++)
    {
        M.data[i] = M1.data[i] / M2.data[i];
    }
    
    return(M);
}

CMatrix scalarpow(const CMatrix &M1, float power)
{
    CMatrix M(M1.nRows, M1.nCols);
    for (int i = 0; i < M.nRows * M.nCols; i++)
    {
        M.data[i] = (float)pow(M1.data[i], power);
    }
    return(M);
}

CMatrix rowsum(const CMatrix &M1)
{
    CMatrix M(1, M1.nCols);
    
    for (int j = 0; j < M1.nCols; j++)
    {
        float rowsumj = 0;
        for (int i = 0; i < M1.nRows; i++)
        {
            rowsumj += M1(i, j);
        }
        M(0, j) = rowsumj;
    }
    
    return(M);
}

CMatrix colsum(const CMatrix &M1)
{
    CMatrix M(M1.nRows, 1);
    
    for (int i = 0; i < M1.nRows; i++)
    {
        float colsumi = 0;
        for (int j = 0; j < M1.nCols; j++)
        {
            colsumi += M1(i, j);
        }
        M(i, 0) = colsumi;
    }
    
    return(M);
}

float sum(const CMatrix &M1)
{
    float s = 0;
    for (int i = 0; i < M1.nRows * M1.nCols; i++)
    {
        s += M1.data[i];
    }
    return(s);
}
           
float dot(const CMatrix &M1, const CMatrix &M2)
{
    assert(M1.nRows == M2.nRows);
    assert(M1.nCols == 1);
    assert(M2.nCols == 1);

    return(sum(scalarmult(M1, M2)));
}

CMatrix cross(const CMatrix &a, const CMatrix &b)
{
    assert(a.nRows == 3);
    assert(b.nRows == 3);
    assert(a.nCols == 1);
    assert(b.nCols == 1);
    
    CMatrix c(3, 1);
    c[0] = a[1] * b[2] - b[1] * a[2];
    c[1] = a[2] * b[0] - b[2] * a[0];
    c[2] = a[0] * b[1] - b[0] * a[1];

    return(c);
}

CMatrix transpose(const CMatrix &M)
{
    CMatrix Mt(M.nCols, M.nRows);

    float* Mcolptr = M.data;
    float* Mtptr   = Mt.data;
    int McolStride = M.colStride();

    for (int row = 0; row < M.nRows; row++)
    {
        float *Mptr = Mcolptr;

        for (int col = 0; col < M.nCols; col++)
        {
            *Mtptr = *Mptr;
            Mtptr++;
            Mptr += McolStride;
        }// end for col

        Mcolptr += M.rowStride();
    }// end for row

    return(Mt);
}


void CMatrix::zeros(void)
{
    memset(data, 0, nRows * nCols * sizeof(float));
}

void CMatrix::eye(void)
{
    CMatrix& M = *this;
    zeros();
    int n = min(nRows, nCols);
    for (int i = 0; i < n; i++)
    {
        M(i, i) = 1;
    }
}

void CMatrix::random(int mod)
{
    for (int i = 0; i < nRows * nCols; i++)
    {
        data[i] = (float)(rand()%mod);
    }
}

float CMatrix::norm(void) const
{
    assert(nCols == 1);
    const CMatrix &v = *this;
    CMatrix vt = ~v;
    CMatrix innerProd = vt * v;
    return((float)sqrt(innerProd[0]));
}

    

CMatrix homogeneous(const CMatrix &M1)
// copy matrix and set last row to 1    
{
    CMatrix M(M1.nRows + 1, M1.nCols);

    for (int i = 0; i < M1.nRows; i++)
    {
        for (int j = 0; j < M1.nCols; j++)
        {
            M(i, j) = M1(i, j);
        }
    }    
    
    for (int i = 0; i < M.nCols; i++)
    {
        M(M.nRows - 1, i) = 1;
    }
    
    return(M);
}

CMatrix unhomogeneous(const CMatrix &M1)
{
    CMatrix M(M1.nRows - 1, M1.nCols);
    
    for (int j = 0; j < M.nCols; j++)
    {
        float divj = M1(M1.nRows - 1, j);
        
        for (int i = 0; i < M.nRows; i++)
        {
            M(i, j) = M1(i, j) / divj;
        }
    }        
    return(M);
}

CMatrix normalise(const CMatrix &M1)
// divide each column so that the last row = 1
{
    CMatrix M = M1;
    
    for (int j = 0; j < M.nCols; j++)
    {
        float divj = M(M.nRows - 1, j);
        
        for (int i = 0; i < M.nRows - 1; i++)
        {
            M(i, j) /= divj;
        }
        M(M.nRows - 1, j) = 1;
    }        
    return(M);
}

CMatrix unit(const CMatrix &M)
{
    return(M / M.norm());
}

void CMatrix::vectorize(void)
{
    nRows = nRows * nCols;
    nCols = 1;
}

void CMatrix::vectorize2(void)
{
    CMatrix &M1 = *this;
    CMatrix M2(nRows * nCols, 1);
    int c = 0;
    for (int j = 0; j < nCols; j++)
    {
        for (int i = 0; i < nRows; i++)
        {
            M2[c] = M1(i, j);
            c++;
        }
    }
    M1 = M2;
}

CMatrix ComputeMtM(const CMatrix &M)
// Fast computation of ~M * M
{
    int m = M.nRows;
    int n = M.nCols;

    CMatrix MtM(n, n);
    
    float *coliptr = M.data;
    float *coljptr = M.data;

    for (int i = 0; i < n; i++)
    {

        float *coliptr0 = coliptr;
        coljptr = M.data + m * i;        
        
        for (int j = i; j < n; j++)
        {
            coliptr = coliptr0;
            
            float MtMij = 0;
            for (int k = 0; k < m; k++)
            {
                MtMij += *coliptr * *coljptr;
                coliptr++;
                coljptr++;
            }// end for k

            MtM(i, j) = MtMij;
            MtM(j, i) = MtMij;
            
        }// end for j

    }// end for i    

    return(MtM);
}


CMatrix SolveLinearSystem(CMatrix &M, CMatrix &v)
// Solve the linear system M * x = v for x
// using Gaussian Elimination with pivoting
// note: M and v are overwritten
{
    CMatrix x;
    
    assert(M.nRows == M.nCols);
    assert(M.nCols == v.nRows);
    assert(v.nCols == 1);
    
    int n = v.nRows;

    x.Create(n, 1);
    
    // Triangulate matrix with pivoting 
    
    for (int i = 0; i < n; i++)
    {
        // swap row with max coeff with row i
        float maxval = (float)fabs(M(i, i));
        int   maxrow = i;
        
        for (int row = i + 1; row < n; row++)
        {
            if ( (float)fabs(M(row, i)) > maxval)
            {
                maxval = (float)fabs(M(row, i));
                maxrow = row;
            }
        }
        
        if (maxrow != i)
        {
            // swap maxrow with row i
            for (int j = i; j < n; j++)
            {
                float tmp    = M(i, j);
                M(i, j)      = M(maxrow, j);
                M(maxrow, j) = tmp;
            }
            float tmp = v[i];
            v[i]      = v[maxrow];
            v[maxrow] = tmp;            
        }
        
        // Eliminate ith unknown from subsequent equations
        for (int j = i + 1; j < n; j++)
        {
            float scalej = M(j, i) / M(i, i);
            
            // subtract scalej * ith row from jth row
            M(j, i) = 0;
            for (int k = i + 1; k < n; k++)
            {
                M(j, k) -= M(i, k) * scalej;
            }
            v[j] -= v[i] * scalej;
        }

    }// end for i
        
    //cerr << "M = " << M << endl;
    //cerr << "v = " << v << endl;
    
    // Back substitution    
    for (int i = n - 1; i >= 0; i--)
    {
        float rhs = v(i, 0);
        for (int j = i + 1; j < n; j++)
        {
            rhs -= M(i, j) * x(j, 0);
        }
        x(i, 0) = rhs / M(i, i);
    }

    return(x);
}

CMatrix VectorRotation(const CMatrix &theta)
{
    assert(theta.nRows == 3);
    assert(theta.nCols == 1);
    
    CMatrix R(3, 3);

    float modTheta = theta.norm();
    
    if (modTheta == 0)
    {
        R.eye();
        return(R);
    }

    CMatrix thetaHat = theta / modTheta;
    
    CMatrix I3x3(3, 3);
    I3x3.eye();

    CMatrix C(3, 3);
    C = CrossMatrix( thetaHat );

    R = I3x3 + C * (float)sin( modTheta ) + C * C * ( 1 - (float)cos( modTheta ) );

    return(R);
}

CMatrix CrossMatrix(const CMatrix &thetaHat)
{
    assert(thetaHat.nRows == 3);
    assert(thetaHat.nCols == 1);
    
    float x1 = thetaHat[0];
    float x2 = thetaHat[1];
    float x3 = thetaHat[2];

    CMatrix C(3, 3);
    
    C(0, 1) = -x3; C(0, 2) = x2;
    C(1, 0) = x3;  C(1, 2) = -x1;
    C(2, 0) = -x2; C(2, 1) = x1;

    return(C);
}


CMatrix RotationBetweenVectors(const CMatrix &u1, const CMatrix &u2)
// finds the rotation matrix R such that 
// u2 = R * u1 
// where u1, u2 are unit vectors
{
    CMatrix R(3, 3);
    R.eye();
    
    float eps = 1e-16;
	
    CMatrix c = cross(u1, u2);
    float sinTheta = c.norm();
    
    if (fabs(sinTheta) < eps)
        return(R);
	
    float d = dot(u1, u2);
    float theta;
    
    if (d > 0)
        theta = (float)asin(sinTheta);
    else
        theta = (float)(M_PI - asin(sinTheta));
    
    CMatrix v = (c / c.norm()) * theta;
    
    R = VectorRotation( v );
    
    return(R);
}


ostream& operator<<(ostream &os, const CMatrix &M)
{
    for (int i = 0; i < M.nRows; i++)
    {
        for (int j = 0; j < M.nCols; j++)
        {
            os << M(i, j) << " ";
        }
        os << endl;
    }    
    return(os);
}

istream& operator>>(istream &is, CMatrix &M)
{
    for (int i = 0; i < M.nRows; i++)
    {
        for (int j = 0; j < M.nCols; j++)
        {
            is >> M(i, j);
        }
    }    
    return(is);
}

CMatrix InvertSimilarity(const CMatrix &S)
{
    assert(S.nCols == 3);
    assert(S.nRows == 3);
    assert(S(2, 0) == 0);
    assert(S(2, 1) == 0);
    assert(S(2, 2) == 1);
    
    float a, b, c, d;
    a = S(0, 0); b = S(0, 1); c = S(0, 2); d = S(1, 2); 

    CMatrix Sinv(3, 3);
    float s = a * a + b * b;
    Sinv(0, 0) = a; Sinv(0, 1) = -b; Sinv(0, 2) = -a * c + b * d;
    Sinv(1, 0) = b; Sinv(1, 1) = a;  Sinv(1, 2) = -b * c - a * d;
    Sinv(2, 0) = 0; Sinv(2, 1) = 0;  Sinv(2, 2) = s;
    
    Sinv = Sinv / s;

    return(Sinv);
}

float sgn(float x)
{
    if (x < 0)
        return(-1.0f);
    else
        return(1.0f);
}
