//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// Image.h

#pragma once

#include <vector>

using namespace std;

typedef struct {int nRows; int nCols; int nBands;} SImageSize;

class CImage
{
public:
    CImage(); 
    ~CImage();

    CImage(int rows, int cols, int bands);
    
    void Create(const char* filename);
    void Create(int rows, int cols, int bands);
    void Clear(void);

    CImage(const CImage &im);
    void operator=(const CImage &im);

    float &operator()(int row, int col, int band);
    const float operator()(int row, int col, int band) const;

    void operator*(vector<float> f);

    int rowStride(void) const;
    int colStride(void) const;

    CImage subimage(int row1, int row2, int col1, int col2) const;
    void assign(const CImage &im1, int row, int col);
    
    void WriteJPEG(const char* filename, int quality);
    
    void TruncateIntensity(void);
    void ConvertToGrey(void);

    int nRows;
    int nCols;
    int nBands;
    float *data;
};

CImage SubSample2Block(const CImage &cim); // subsample image by 2x2 block averaging
CImage ConvolveGaussian(const CImage &cim, float sigma); // convolve image with gaussian sd sigma length 2 * sigma + 1
CImage ConvolveGaussianBounded(const CImage &im, float sigma, vector<int> &rowBegin, vector<int> &rowEnd, vector<int> &colBegin, vector<int> &colEnd);

void CreateGaussKernel(float *kernel, float sigma, int length); 
// Create a gaussian kernel size = 2 * length + 1
CImage ConvolveRows(const CImage &im, float *kernel, int length);
CImage ConvolveCols(const CImage &im, float *kernel, int length);
CImage ConvolveSeparable(const CImage &im, float *kernel, int length);

CImage ConvolveRowsBounded(const CImage &im, float *kernel, int length, vector<int> &rowBegin, vector<int> &rowEnd);

CImage SubSample2Rows(const CImage &im, float *kernel, int length);
CImage SubSample2(const CImage &im, float *kernel, int length);

CImage ConvolveRows0(const CImage &im, float *kernel, int length);
CImage ConvolveCols0(const CImage &im, float *kernel, int length);
CImage ConvolveSeparable0(const CImage &im, float *kernel, int length); 
CImage ConvolveSeparable1(const CImage &im, float *kernel, int length); 
CImage ConvolveSeparable2(const CImage &im, float *kernel, int length); 

CImage Transpose(const CImage &im);
CImage ConvertToGrey(const CImage &im);
CImage ConvertToColour(const CImage &im);
CImage Abs(const CImage &im);

CImage operator-(const CImage &im1, const CImage &im2);
CImage operator>=(const CImage &im1, const CImage &im2);
CImage mask(const CImage &im, const CImage &mask);

void ImageExtend(CImage &im, vector<int> &rowBegin, vector<int> &rowEnd, vector<int> &colBegin, vector<int> &colEnd);
void ZeroInvalid(CImage &im, vector<int> &rowBegin, vector<int> &rowEnd, vector<int> &colBegin, vector<int> &colEnd);

CImage operator*(const CImage &im1, float f);
CImage operator+(const CImage &im1, float f);
CImage operator*(const CImage &im1, vector<float> f);

