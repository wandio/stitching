//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// Camera.h

#ifndef CAMERA_H
#define CAMERA_H

#include "Matrix.h"

class CCamera
{
public:
    CCamera() { }
    ~CCamera() { }

    CMatrix R;
    float f;
    CMatrix T;    
};

#endif
