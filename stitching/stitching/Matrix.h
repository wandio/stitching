//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// Matrix.h

#pragma once

#include <iostream>

using namespace std;

class CMatrix
{
 public:
    CMatrix();
    ~CMatrix();
    
    CMatrix(int rows, int cols);
    
    CMatrix(const CMatrix &M);
    void operator=(const CMatrix &M);

    void Create(int rows, int cols);
    
    float& operator()(int row, int col);
    float& operator[](int row);
    
    const float operator()(int row, int col) const;
    const float operator[](int row) const;

	int rowStride(void) const;
	int colStride(void) const;
        
    CMatrix submatrix(int row1, int row2, int col1, int col2) const;
    CMatrix row(int row);
    CMatrix col(int col);

    void assign(const CMatrix &M1, int row, int col = 0);
    
    void zeros(void);
    void eye(void);
    void random(int mod);
    void vectorize(void);
    void vectorize2(void);
    
    float norm(void) const; 
    
    float* data;
    int nRows;
    int nCols;
};

CMatrix operator+(const CMatrix &M1, const CMatrix &M2); 
CMatrix operator-(const CMatrix &M1, const CMatrix &M2);
CMatrix operator*(const CMatrix &M1, const CMatrix &M2);

CMatrix operator~(const CMatrix &M1);
CMatrix operator-(const CMatrix &M1);

CMatrix operator>(const CMatrix &M1, float f);
CMatrix operator<(const CMatrix &M1, float f);

CMatrix operator*(const CMatrix &M1, float f);
CMatrix operator/(const CMatrix &M1, float f);
CMatrix operator+(const CMatrix &M1, float f);
CMatrix operator-(const CMatrix &M1, float f);

CMatrix scalarmult(const CMatrix &M1, const CMatrix &M2);
CMatrix scalardiv(const CMatrix &M1, const CMatrix &M2);
CMatrix scalarpow(const CMatrix &M1, float power);

CMatrix rowsum(const CMatrix &M1);
CMatrix colsum(const CMatrix &M1);
float   sum(const CMatrix &M1);
float   dot(const CMatrix &M1, const CMatrix &M2);
CMatrix cross(const CMatrix &a, const CMatrix &b);
CMatrix transpose(const CMatrix &M);

CMatrix homogeneous(const CMatrix &M1); 
CMatrix unhomogeneous(const CMatrix &M1);
CMatrix normalise(const CMatrix &M1);

CMatrix unit(const CMatrix &M);

CMatrix ComputeMtM(const CMatrix &M);
CMatrix SolveLinearSystem(CMatrix &M, CMatrix &v);

CMatrix InvertSimilarity(const CMatrix &S);
CMatrix VectorRotation(const CMatrix &theta);
CMatrix CrossMatrix(const CMatrix &thetaHat);
CMatrix RotationBetweenVectors(const CMatrix &u1, const CMatrix &u2);

ostream& operator<<(ostream &os, const CMatrix &M);
istream& operator>>(istream &is, CMatrix &M);

float sgn(float x);
