//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// RenderPanorama.h

#pragma once

#include "Parameter.h"
#include "Image.h"
#include "ByteImage.h"
#include "Camera.h"
#include "Panorama.h"
#include <vector>

typedef struct { float thetaMax; float thetaMin; float phiMax; float phiMin; } sAngularRange;

class CImagePieceRenderInfo
{ 
public:
    CImagePieceRenderInfo(float thetaMin0, float thetaMax0, float phiMin0, float phiMax0)
    { 
        thetaMin = thetaMin0;
        thetaMax = thetaMax0;
        phiMin   = phiMin0;
        phiMax   = phiMax0;
    }

    // isValid flag is set if this piece occurs in the current rendering
    bool isValid;

    // range in theta/phi
    float thetaMin; 
    float thetaMax; 
    float phiMin; 
    float phiMax; 

    // range in rendering
    int xmin;
    int xmax;
    int ymin;
    int ymax;

    // images
    CImage image;
    CImage weightLinear;
    CImage weight;
    
    // range for images
    vector<int> IrowBegin;
    vector<int> IrowEnd;
    vector<int> IcolBegin;
    vector<int> IcolEnd;

    vector<int> WrowBegin;
    vector<int> WrowEnd;
    vector<int> WcolBegin;
    vector<int> WcolEnd;
};


class CImageRenderInfo
{
public:
    vector<CImagePieceRenderInfo> piece;

    //float level; // number of times the image has been downsampled factor 2
    //float sigma; // downsampling scale relative to the downsampled image

    // isValid flag is set if this image appears in the current rendering
    bool isValid;

    // theta/phi distances between corners
    // for estimating downsampling rate    
    float dtheta1;
    float dphi1;
    float dtheta2;
    float dphi2;

    // pixel distances between corners in render frame
    // used to estimate downsample rate
    float d1;
    float d2; 
};


class CRenderPanorama
{
public:
//    CRenderPanorama() { }
    ~CRenderPanorama() { }

CRenderPanorama(CRenderParameters& RenderParameters):Param(RenderParameters) {};

    void Render(CByteImage &panoImage, CPanorama &pano);

    void RenderMultiBandBlend(CImage &panoImage, CByteImage &panoAlpha, CPanorama &pano, vector<CImageRenderInfo> &ImageRenderInfo, int nRows, int nCols, const vector< vector<float> > &gain);
    void RenderPiece(CImage &weightMax, CImage &image, CImagePieceRenderInfo &piece, CMatrix P);
    void RenderPieceSampleBilinear(CImage &weightMax, CByteImage &alpha, CImage &image, CImagePieceRenderInfo &piece, CMatrix P);
    void RenderPieceSampleCubicBSpline(CImage &weightMax, CImage &image, CImagePieceRenderInfo &piece, CMatrix P);
    void ComputeWeightIsMax(CImage &weightMax, CImagePieceRenderInfo &piece);
    void ComputeNonZeroRange(CImage &im, vector<int> &rowBegin, vector<int> &rowEnd, vector<int> &colBegin, vector<int> &colEnd);
    void BlendPiece(CImage &num, CImage &den, CImagePieceRenderInfo &piece, CImage &image, CImage &weight);
    void AddBand(CImage &pano, CImage &num, CImage &den);



    void RenderLinearBlend(CImage &panoImage, CByteImage &panoAlpha, CPanorama &pano, vector<CImageRenderInfo> &ImageRenderInfo, int nRows, int nCols, const vector< vector<float> > &gain);
    void RenderNoBlend(CImage &panoImage, CByteImage &panoAlpha, CPanorama &pano, vector<CImageRenderInfo> &ImageRenderInfo, int nRows, int nCols, const vector< vector<float> > &gain);

    void ComputeRenderRangeThetaPhi(CImageRenderInfo &ImageRenderInfo, CCamera &camera, SImageSize imageSize0);
    void ComputeRenderRangeOutputImage(CImageRenderInfo &ImageRenderInfo, sAngularRange angularRange, SImageSize panoImageSize, float dthetady, float dphidx, CImage &image, int subsampleLevel);
    CImageRenderInfo TruncateRenderRange(CImageRenderInfo &ImageRenderInfo0, int xmin, int xmax, int ymin, int ymax, float dthetady, float dphidx, int blockDilate);
    void ResampleImageForRendering(CImage &renderImage, CCamera &renderCamera, int imageID, vector<CImage> &images, vector<int> &subsampleLevel, CImageRenderInfo &ImageRenderInfo, string imageDir, vector<string> &filenames);

    void RenderPieceNoBlend(CImage &pano, CByteImage &alpha, CImage &image, CImagePieceRenderInfo &piece, CMatrix P);
    void RenderPieceLinearBlend(CImage &num, CImage &den, CByteImage &alpha, CImage &image, CImagePieceRenderInfo &piece, CMatrix P);
    
    vector< vector<float> > ComputeImageGain(string imageDir, vector<string> filenames, vector<CImage> &images, vector<SImageSize> &imageSize, 
                                             vector<CCamera> &Cameras, vector<int> &panoramaImageID, vector<int> &subsampleLevel,
                                             vector<CImage> &thumbs, vector<int> &thumbSubsampleLevel, CPanoramaStatus &status);
    bool OverlapTheta(float theta, float &thetaMinOverlap, float &thetaMaxOverlap, 
                      const vector<CImageRenderInfo> &ImageRenderInfo);

    static CMatrix T(int r); // Coordinate scaling matrix for 2x2 block average
    float R(int i, float x) // Cubic B-spline interpolation function
    {	  
        float a = 0.25;

        if (i == -1) return((                  -a * (x * x * x) +          (3.0f * a) * (x * x) - (3.0f * a) * x +            a)/6.0f); 	
        if (i ==  0) return((  (12.0f - 9.0f * a) * (x * x * x) + (12.0f * a - 18.0f) * (x * x)                  + 6.0f - 2 * a)/6.0f);
        if (i ==  1) return((  (9.0f * a - 12.0f) * (x * x * x) + (18.0f - 15.0f * a) * (x * x) + (3.0f * a) * x +            a)/6.0f);
        if (i ==  2) return((                   a * (x * x * x)                                                                )/6.0f);

        cerr << "Error: Cubic B-spline R(.) requires -1 <= i <= 2" << endl;
        return(0);
    }

    // Angle per pixel
    float dthetady;
    float dphidx;
    
    CRenderParameters& Param;
};
