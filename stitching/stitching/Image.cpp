//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// Image.cpp

#include "Image.h"
#include "jpegio.h"
#include "math.h"
#include <iostream>
#include "assert.h"
#include <stdio.h>
#include <string.h>

using namespace std;

CImage::CImage()
{
    nRows = 0;
    nCols = 0;
    nBands = 0;
    data = NULL;
}

CImage::CImage(int rows, int cols, int bands)
{
    data = NULL;
    Create(rows, cols, bands);
}

CImage::~CImage()
{
    delete[] data;
}

CImage::CImage(const CImage &im)
{
    data = NULL;
    this->operator=(im);     
}


void CImage::operator=(const CImage &im)
{
    Create(im.nRows, im.nCols, im.nBands);
    memcpy(data, im.data, nRows * nCols * nBands * sizeof(float));       
}

float &CImage::operator()(int row, int col, int band)
{
    return data[row * nCols * nBands + col * nBands + band];
}

const float CImage::operator()(int row, int col, int band) const
{
    return data[row * nCols * nBands + col * nBands + band];
}

void CImage::operator*(vector<float> f)
// in place vector multiplication
{
    assert(f.size() == nBands);

    float *imptr = data;

    for (int i = 0; i < nRows * nCols; i++)
    {
        for (int b = 0; b < nBands; b++)	
        {
            *imptr *= f[b];
            imptr++;
        }
    }
}

int CImage::rowStride(void) const
{
    return(nCols * nBands);
}

int CImage::colStride(void) const
{
    return(nBands);
}

void CImage::Create(const char* filename)
{
    delete[] data;
    unsigned char *uchar_data;
    ReadJPEG0(uchar_data, nRows, nCols, nBands, filename);
    
    if (nBands == 1)
        *this = ConvertToColour(*this);

    int nData = nRows * nCols * nBands;
    data = new float[nData];
    for (int i = 0; i < nData; i++)
    {
        data[i] = (float)uchar_data[i];
    }

    delete[] uchar_data;
}

void CImage::Create(int rows, int cols, int bands)
{
    delete[] data;
    
    nRows = rows; 
    nCols = cols;
    nBands = bands;
    
    int nData = nRows * nCols * nBands;

    data = new float[nData];
    memset(data, 0, nData * sizeof(float));
}

void CImage::Clear(void)
{
    delete[] data;
    nRows = 0; 
    nCols = 0;
    nBands = 0;
    data = NULL;
}

void CImage::WriteJPEG(const char* filename, int quality)
{
    unsigned char* uchar_data;
    int nData = nRows * nCols * nBands;
    uchar_data = new unsigned char[nData];

    for (int i = 0; i < nData; i++)
    {
        uchar_data[i] = (unsigned char)data[i];
    }

    WriteJPEG0(uchar_data, nRows, nCols, nBands, quality, filename);

    delete[] uchar_data; 
}

CImage ConvertToGrey(const CImage &im)
{
    CImage greyIm(im.nRows, im.nCols, 1);
    float* imptr     = im.data;
    float* greyImptr = greyIm.data;
    
    for (int i = 0; i < im.nRows * im.nCols; i++)
    {
        float greyvalue = 0;
        for (int b = 0; b < im.nBands; b++)
        {
            greyvalue += *imptr;
            imptr++;
        }
        *greyImptr = greyvalue / im.nBands;
        greyImptr++;
    }
    return(greyIm);
}

CImage ConvertToColour(const CImage &gim)
{
    CImage im(gim.nRows, gim.nCols, 3);
    int nPixels = gim.nRows * gim.nCols;

    float *gimptr = gim.data;
    float *imptr  =  im.data;
    
    for (int i = 0; i < nPixels; i++)
    {
        for (int b = 0; b < 3; b++)
        {
            *imptr = *gimptr;
            imptr++;
        }
        gimptr++;
    }

    return(im);
}

CImage Abs(const CImage &im)
{
    CImage aim(im.nRows, im.nCols, im.nBands);

    float *imptr = im.data;
    float *aimptr = aim.data;

    for (int i = 0; i < im.nRows * im.nCols * im.nBands; i++)
    {
        *aimptr = (float)fabs(*imptr);
        imptr++;
        aimptr++;
    }

    return(aim);
}


void CImage::TruncateIntensity(void)
// truncate image intensities to 0 -> 255
{
    for (int i = 0; i < nRows * nCols * nBands; i++)
    {
        data[i] = max(min(255.0f, data[i]), 0.0f);
    }
}

void CImage::ConvertToGrey(void)
{

    for (int i = 0; i < nRows * nCols; i++)
    {
        float greyvalue = 0;
        for (int j = 0; j < nBands; j++)
        {
            greyvalue += data[i * nBands + j];
        }// end for j
        greyvalue /= (float)nBands;
        
        for (int j = 0; j < nBands; j++)
        {
            data[i * nBands + j] = greyvalue;
        }// end for j
        
    }// end for i 
    
}

CImage Transpose(const CImage &im)
// swap rows and columns
{
    //cerr << "Transpose" << endl;
    int nRows  = im.nRows;
    int nCols  = im.nCols;
    int nBands = im.nBands;

    int nRowst = nCols;
    int nColst = nRows;
    
    CImage imt(nRowst, nColst, nBands);
    
    float *imptr    = im.data;
    float *imcolptr = imptr;
    float *imtptr   = imt.data;
   
    for (int i = 0; i < nRowst; i++)
    {
        imptr = imcolptr;
        for (int j = 0; j < nColst; j++)
        {       
            memcpy(imtptr, imptr, nBands * sizeof(float));
            imtptr += nBands;
            imptr  += nCols * nBands;
        }
        imcolptr += nBands;
    }
    
    return(imt);
}

CImage SubSample2Block(const CImage &im)
// Subsample image using 2x2 block average
{
    int nRows  = im.nRows;
    int nCols  = im.nCols;
    int nBands = im.nBands;
    int nRows2 = (int)floor(nRows / 2.0f);
    int nCols2 = (int)floor(nCols / 2.0f);
    
    CImage sim(nRows2, nCols2, nBands);

    float *simptr   = sim.data;
    float *imptr    = im.data;
    float *imrowptr = im.data;
    int imRowStride = im.rowStride();
    int imRowStride2 = imRowStride * 2;

    for (int i = 0; i < nRows2; i++)
    {
        imptr = imrowptr;

        for (int j = 0; j < nCols2; j++)
        {
            for (int b = 0; b < nBands; b++)
            {
                float *imptrtmp;
                imptrtmp = imptr;
                float avg = 0;
                
                avg += *imptrtmp;
                imptrtmp += nBands;
                avg += *imptrtmp;
                imptrtmp += imRowStride;
                avg += *imptrtmp;
                imptrtmp -= nBands;
                avg += *imptrtmp;
                
                *simptr = avg / 4.0f;
                
                simptr++;
                imptr++;
            }
            
            imptr += nBands;

        }// end for j

        imrowptr += imRowStride2;
    }// end for i

    return(sim);
}

CImage ConvolveGaussian(const CImage &im, float sigma)
{
    //int length = (int)floor(2 * sigma + 0.5);
    int length = (int)floor(3 * sigma + 0.5);
    if (length < 1)
    {
        return(im);
    }
    
    float *kernel = new float[2 * length + 1];
    CreateGaussKernel(kernel, sigma, length);

    CImage cim = ConvolveSeparable(im, kernel, length);

    delete[] kernel;
   
    return(cim);
}

CImage ConvolveGaussianBounded(const CImage &im, float sigma, vector<int> &rowBegin, vector<int> &rowEnd, vector<int> &colBegin, vector<int> &colEnd)
{
    CImage cim = im;
    ImageExtend(cim, rowBegin, rowEnd, colBegin, colEnd);
    cim = ConvolveGaussian(cim, sigma);
    ZeroInvalid(cim, rowBegin, rowEnd, colBegin, colEnd);
    return(cim);
}

CImage ConvolveRows(const CImage &im, float *kernel, int length)
{
    //cerr << "ConvolveRows" << endl;
    int nRows  = im.nRows;
    int nCols  = im.nCols;
    int nBands = im.nBands;

    CImage cim(nRows, nCols, nBands);

    float *cimptr   = cim.data;
    float *imptr    =  im.data;
    float *imrowptr =  imptr;
    float *kptr;

    float *buffer = new float[(nCols + 2 * length) * nBands];

    int ksize = 2 * length + 1;

    for (int row = 0; row < nRows; row++)
    {
        
        //
        // Copy row into buffer extending the end values
        //

        float *bufferptr = buffer;
        for (int i = 0; i < length; i++)
        {
            imptr = imrowptr;
            for (int b = 0; b < nBands; b++)
            {
                *bufferptr = *imptr;
                bufferptr++;
                imptr++;
            }
        }
            
        memcpy(bufferptr, imrowptr, nCols * nBands * sizeof(float));
        
        bufferptr += nCols * nBands;
        float *imrowendptr = imrowptr + (nCols - 1) * nBands;
        
        for (int i = 0; i < length; i++)
        {
            imptr = imrowendptr;
            for (int b = 0; b < nBands; b++)
            {
                *bufferptr = *imptr;
                bufferptr++;
                imptr++;
            }
        }
        
        //
        // Convolve buffer with kernel
        //
        
        float *bufferptr0 = buffer;
        
        for (int col = 0; col < nCols; col++)
        {                        
            
            for (int b = 0; b < nBands; b++)
            {                
                bufferptr = bufferptr0 + b;
                kptr  = kernel + ksize - 1;
                
                for (int k = 0; k < ksize; k++)
                {
                    *cimptr += *bufferptr * *kptr;
                    bufferptr += nBands;
                    kptr--;
                }// end for k
                
                cimptr++;
                
            }// end for b
            
            bufferptr0 += nBands;
                
        }// end for col
                
        imrowptr += nCols * nBands;
        
    }// end for row
    
    return(cim);   
}

CImage ConvolveRowsBounded(const CImage &im, float *kernel, int length, vector<int> &rowBegin, vector<int> &rowEnd)
{
    //cerr << "ConvolveRows" << endl;
    int nRows  = im.nRows;
    int nCols  = im.nCols;
    int nBands = im.nBands;

    assert(rowBegin.size() == nRows);
    assert(rowEnd.size() == nRows);

    CImage cim(nRows, nCols, nBands);

    float *cimptr    = cim.data;
    float *cimrowptr = cimptr;
    float *imptr     =  im.data;
    float *imrowptr  =  imptr;
    float *kptr;

    float *buffer = new float[(nCols + 2 * length) * nBands];

    int ksize = 2 * length + 1;

    for (int row = 0; row < nRows; row++)
    {        
        //
        // Copy row into buffer extending the end values
        //

        float *imrowbeginptr = imrowptr + nBands * rowBegin[row];
        imptr = imrowbeginptr;

        float *bufferptr = buffer;
        for (int i = 0; i < length + rowBegin[row]; i++)
        {
            imptr = imrowbeginptr;
            for (int b = 0; b < nBands; b++)
            {
                *bufferptr = *imptr;
                bufferptr++;
                imptr++;
            }
        }

        int   rowSize = rowEnd[row] - rowBegin[row] + 1;
        
        memcpy(bufferptr, imrowbeginptr, rowSize * nBands * sizeof(float));
        
        bufferptr += rowSize * nBands;
        float *imrowendptr = imrowptr + rowEnd[row] * nBands;
        
        for (int i = 0; i < length + (nCols - 1 - rowEnd[row]); i++)
        {
            imptr = imrowendptr;
            for (int b = 0; b < nBands; b++)
            {
                *bufferptr = *imptr;
                bufferptr++;
                imptr++;
            }
        }
        
        //
        // Convolve buffer with kernel
        //
        
        float *bufferptr0 = buffer;
        bufferptr0 += rowBegin[row] * nBands;
        cimptr = cimrowptr + rowBegin[row] * nBands;

        for (int col = rowBegin[row]; col <= rowEnd[row]; col++)
        {                        
            for (int b = 0; b < nBands; b++)
            {                
                bufferptr = bufferptr0 + b;
                kptr  = kernel + ksize - 1;
                
                for (int k = 0; k < ksize; k++)
                {
                    *cimptr += *bufferptr * *kptr;
                    bufferptr += nBands;
                    kptr--;
                }// end for k
                
                cimptr++;
                
            }// end for b
            
            bufferptr0 += nBands;
                
        }// end for col
                
        imrowptr += nCols * nBands;
        cimrowptr += nCols * nBands;

    }// end for row

    cerr << "return cim" << endl;

    delete[] buffer;
    
    return(cim);   
}


CImage ConvolveRows0(const CImage &im, float *kernel, int length)
{
    int nRows  = im.nRows;
    int nCols  = im.nCols;
    int nBands = im.nBands;

    CImage cim(nRows, nCols, nBands);

    float *cimptr   = cim.data;
    float *imptr    =  im.data;
    float *imrowptr =  imptr;
    float *kptr;

    int ksize = 2 * length + 1;

    for (int row = 0; row < nRows; row++)
    {
        //
        // Left side
        //

        for (int col = 0; col < length; col++)
        {                        

            int nPad = length - col;
            
            for (int b = 0; b < nBands; b++)
            {
                imptr = imrowptr + b;
                kptr  = kernel + ksize - 1;
                int k = 0;
                
                while (k < nPad)
                {
                    *cimptr += *imptr * *kptr;
                    kptr--;
                    k++;
                }
                while (k < ksize)
                {
                    *cimptr += *imptr * *kptr;
                    kptr--;
                    imptr += nBands;
                    k++;
                }

                cimptr++;          
            
            }// end for b
            
        }// end for col
        
        //
        // Centre
        //
        
        float *imptr0 = imrowptr;            
        
        for (int col = length; col < nCols - length; col++)
        {                        
            
            for (int b = 0; b < nBands; b++)
            {                
                imptr = imptr0 + b;
                kptr  = kernel + ksize - 1;
                
                for (int k = 0; k < ksize; k++)
                {
                    *cimptr += *imptr * *kptr;
                    imptr += nBands;
                    kptr--;
                }// end for k
                
                cimptr++;
                
            }// end for b
            
            imptr0 += nBands;
                
        }// end for col
        
        //
        // Right side
        //
        
        float *imrowendptr = imrowptr + nCols * nBands - nBands;
        
        for (int col = nCols - length; col < nCols; col++)
        {                        
            
            int nPad = length - nCols + col + 1;
            
            for (int b = 0; b < nBands; b++)
            {
                imptr = imrowendptr + b;
                kptr  = kernel + ksize - 1;
                int k = 0;
                
                while (k < nPad)
                {
                    *cimptr += *imptr * *kptr;
                    kptr--;
                    k++;
                }
                while (k < ksize)
                {
                    *cimptr += *imptr * *kptr;
                    kptr--;
                    imptr -= nBands;
                    k++;
                }
                
                cimptr++;          
                
            }// end for b
            
        }// end for col
        
        imrowptr += nCols * nBands;
        
    }// end for row
        
    return(cim);   
}


CImage ConvolveCols(const CImage &im, float *kernel, int length)
{
    int nRows  = im.nRows;
    int nCols  = im.nCols;
    int nBands = im.nBands;

    CImage cim(nRows, nCols, nBands);

    float *cimptr    = cim.data;
    float *cimcolptr = cimptr; 
    float *imptr     =  im.data;
    float *imcolptr  =  imptr;
    float *kptr;

    float *buffer = new float[(nRows + 2 * length) * nBands];

    int ksize = 2 * length + 1;

    for (int col = 0; col < nCols; col++)
    {
        
        //
        // Copy col into buffer extending the end values
        //
        
        float *bufferptr = buffer;
        for (int i = 0; i < length; i++)
        {
            imptr = imcolptr;
            for (int b = 0; b < nBands; b++)
            {
                *bufferptr = *imptr;
                bufferptr++;
                imptr++;
            }
        }

        float *imptr0 = imcolptr;
        for (int i = 0; i < nRows; i++)
        {
            imptr = imptr0;
            for (int b = 0; b < nBands; b++)
            {
                *bufferptr = *imptr;
                bufferptr++;
                imptr++;
            }
            imptr0 += nCols * nBands;
        }
            
        float *imcolendptr = imcolptr + (nRows - 1) * nCols * nBands;
        
        for (int i = 0; i < length; i++)
        {
            imptr = imcolendptr;
            for (int b = 0; b < nBands; b++)
            {
                *bufferptr = *imptr;
                bufferptr++;
                imptr++;
            }
        }
        
        //
        // Convolve buffer with kernel
        //
        
        float *bufferptr0 = buffer;
        float *cimptr0 = cimcolptr;

        for (int row = 0; row < nRows; row++)
        {                        
            cimptr = cimptr0;

            for (int b = 0; b < nBands; b++)
            {                
                bufferptr = bufferptr0 + b;
                kptr  = kernel + ksize - 1;
                
                for (int k = 0; k < ksize; k++)
                {
                    *cimptr += *bufferptr * *kptr;
                    bufferptr += nBands;
                    kptr--;
                }// end for k
                
                cimptr++;
                
            }// end for b
            
            bufferptr0 += nBands;
            cimptr0 += nCols * nBands;
    
        }// end for row
                
        imcolptr  += nBands;
        cimcolptr += nBands;
        
    }// end for col
    
    return(cim);   
}


CImage ConvolveCols0(const CImage &im, float *kernel, int length)
{
    int nRows  = im.nRows;
    int nCols  = im.nCols;
    int nBands = im.nBands;

    CImage cim(nRows, nCols, nBands);

    float *cimptr    = cim.data;
    float *cimcolptr = cimptr;
    float *imptr     =  im.data;
    float *imcolptr  =  imptr;
    float *kptr;

    int rowStep = nCols * nBands;

    int ksize = 2 * length + 1;
    
    for (int col = 0; col < nCols; col++)
    {
        //
        // Left side
        //

        float *cimptr0 = cimcolptr;
        float *imptr0  = imcolptr;

        for (int row = 0; row < length; row++)
        {                        
            int nPad = length - row;

            cimptr  = cimptr0;

            for (int b = 0; b < nBands; b++)
            {
                imptr  = imcolptr + b;
                kptr   = kernel + ksize - 1;
                int k = 0;
                
                while (k < nPad)
                {
                    *cimptr += *imptr * *kptr;
                    kptr--;
                    k++;
                }
                while (k < ksize)
                {
                    *cimptr += *imptr * *kptr;
                    kptr--;
                    imptr += rowStep;
                    k++;
                }
                
                cimptr++;          
            
            }// end for b
            
            cimptr0 += rowStep;

        }// end for row
      
        //
        // Centre
        //
        
        imptr0  = imcolptr;            
        cimptr0 = cimcolptr + length * rowStep;
        
        for (int row = length; row < nRows - length; row++)
        {                        
            cimptr = cimptr0;
    
            for (int b = 0; b < nBands; b++)
            {                
                imptr  = imptr0 + b;
                kptr  = kernel + ksize - 1;
                
                for (int k = 0; k < ksize; k++)
                {
                    *cimptr += *imptr * *kptr;
                    imptr += nBands * nCols;
                    kptr--;
                }// end for k
                
                cimptr++;
                
            }// end for b
            
            imptr0  += nBands * nCols;
            cimptr0 += nBands * nCols;
                
        }// end for row
        
        //
        // Right side
        //
        
        float *imcolendptr = imcolptr + (nRows - 1) * rowStep;
        
        for (int row = nRows - length; row < nRows; row++)
        {                        
            
            int nPad = length - nRows + row + 1;
            cimptr = cimptr0; 
            
            for (int b = 0; b < nBands; b++)
            {
                imptr  = imcolendptr + b;
                kptr  = kernel + ksize - 1;
                int k = 0;
                
                while (k < nPad)
                {
                    *cimptr += *imptr * *kptr;
                    kptr--;
                    k++;
                }
                while (k < ksize)
                {
                    *cimptr += *imptr * *kptr;
                    kptr--;
                    imptr -= rowStep;
                    k++;
                }
                
                cimptr++;          
                
            }// end for b

            cimptr0 += rowStep;
            
        }// end for row

        imcolptr  += nBands;
        cimcolptr += nBands;

    }// end for col
    
    return(cim);   
}


CImage SubSample2Rows(const CImage &im, float *kernel, int length)
{
    //cerr << "SubSample2Rows" << endl;
    int nRows  = im.nRows;
    int nCols  = im.nCols;
    int nBands = im.nBands;

    int nCols2 = (int)ceil(nCols / 2.0f);
    CImage cim(nRows, nCols2, nBands);

    float *cimptr   = cim.data;
    float *imptr    =  im.data;
    float *imrowptr =  imptr;
    float *kptr;

    float *buffer = new float[(nCols + 2 * length) * nBands];

    int ksize = 2 * length + 1;

    for (int row = 0; row < nRows; row++)
    {
        
        //
        // Copy row into buffer extending the end values
        //

        float *bufferptr = buffer;
        for (int i = 0; i < length; i++)
        {
            imptr = imrowptr;
            for (int b = 0; b < nBands; b++)
            {
                *bufferptr = *imptr;
                bufferptr++;
                imptr++;
            }
        }
            
        memcpy(bufferptr, imrowptr, nCols * nBands * sizeof(float));
        
        bufferptr += nCols * nBands;
        float *imrowendptr = imrowptr + (nCols - 1) * nBands;
        
        for (int i = 0; i < length; i++)
        {
            imptr = imrowendptr;
            for (int b = 0; b < nBands; b++)
            {
                *bufferptr = *imptr;
                bufferptr++;
                imptr++;
            }
        }
        
        //
        // Convolve buffer with kernel
        //
        
        float *bufferptr0 = buffer;
        
        for (int col = 0; col < nCols2; col++)
        {                        
            
            for (int b = 0; b < nBands; b++)
            {                
                bufferptr = bufferptr0 + b;
                kptr  = kernel + ksize - 1;
                
                for (int k = 0; k < ksize; k++)
                {
                    *cimptr += *bufferptr * *kptr;
                    bufferptr += nBands;
                    kptr--;
                }// end for k
                
                cimptr++;
                
            }// end for b
            
            bufferptr0 += 2 * nBands;
                
        }// end for col
                
        imrowptr += nCols * nBands;
        
    }// end for row
    
    return(cim);   
}

CImage SubSample2(const CImage &im, float *kernel, int length)
{
    return (Transpose(SubSample2Rows(Transpose(SubSample2Rows(im, kernel, length)), kernel, length)));
}

//
// 2D Separable Convolutions
// ... in speed order 
// ConvolveRows is faster than ConvolveCols due to less cache misses
//

CImage ConvolveSeparable(const CImage &im, float *kernel, int length)
{
    return (Transpose(ConvolveRows((Transpose(ConvolveRows(im, kernel, length))), kernel, length)));
}

CImage ConvolveSeparable0(const CImage &im, float *kernel, int length)
{
    return (Transpose(ConvolveRows0((Transpose(ConvolveRows0(im, kernel, length))), kernel, length)));
}

CImage ConvolveSeparable1(const CImage &im, float *kernel, int length)
{
    return (ConvolveCols(ConvolveRows(im, kernel, length), kernel, length));
}

CImage ConvolveSeparable2(const CImage &im, float *kernel, int length)
{
    return (ConvolveCols0(ConvolveRows0(im, kernel, length), kernel, length));
}
    
void CreateGaussKernel(float* kernel, float sigma, int length)
// Create a gaussian kernel size = 2 * length + 1
{
    float c = 1.0f / ( sigma * (float)sqrt( 2.0f * (float)M_PI ) );
    
    float sum = 0.0f;
    
    kernel[length] = c;
    sum = c;
    for (int i = 1; i <= length; i++)
    {
        float ki = c * (float)exp( - 0.5f * ((i * i) / (sigma * sigma)) );
        kernel[length + i] = ki;
        kernel[length - i] = ki;     
        sum += 2 * ki;
    }            
    
    // Normalise sum to 1
    for (int i = 0; i < 2 * length + 1; i++)
    {
        kernel[i] /= sum;
    }
}

CImage CImage::subimage(int row1, int row2, int col1, int col2) const
{
    const CImage &im = *this;

    assert(row1 <= row2);
    assert(col1 <= col2);
    assert(row1 >= 0);
    assert(col1 >= 0);
    assert(row2 <= im.nRows - 1);
    assert(col2 <= im.nCols - 1);

    CImage sim(row2 - row1 + 1, col2 - col1 + 1, im.nBands);
    
    for (int i = 0; i < sim.nRows; i++)
    {
        for (int j = 0; j < sim.nCols; j++)
        {
            for (int b = 0; b < sim.nBands; b++)
            {
                sim(i, j, b) = im(i + row1, j + col1, b);
            }
        }
    }

    return(sim);
}

void CImage::assign(const CImage &im1, int row, int col)
// copy the image im1 to row, col in this im
{
    assert(im1.nBands == nBands); 
    assert(row + im1.nRows <= nRows); 
    assert(col + im1.nCols <= nCols);

    CImage &im = *this;

    for (int i = 0; i < im1.nRows; i++)
    {
        for (int j = 0; j < im1.nCols; j++)
        {
            for (int b = 0; b < im1.nBands; b++) 
            {
                im(row + i, col + j, b) = im1(i, j, b);
            }
        }
    }
}

CImage operator-(const CImage &im1, const CImage &im2)
{
    assert(im1.nRows  == im2.nRows);
    assert(im1.nCols  == im2.nCols);
    assert(im1.nBands == im2.nBands);
    
    CImage im(im1.nRows, im1.nCols, im1.nBands);
    float *im1ptr = im1.data;
    float *im2ptr = im2.data;
    float *imptr  = im.data;

    for (int i = 0; i < im1.nRows * im1.nCols * im1.nBands; i++)
    {
        *imptr = *im1ptr - *im2ptr;
        imptr++;
        im1ptr++;
        im2ptr++;        
    }

    return(im);
}

CImage operator>=(const CImage &im1, const CImage &im2)
{
    assert(im1.nRows  == im2.nRows);
    assert(im1.nCols  == im2.nCols);
    assert(im1.nBands == im2.nBands);
    
    CImage im(im1.nRows, im1.nCols, im1.nBands);
    
    float *im1ptr = im1.data;
    float *im2ptr = im2.data;
    float *imptr  = im.data;

    for (int i = 0; i < im1.nRows * im1.nCols * im1.nBands; i++)
    {
        if (*im1ptr >= *im2ptr)            
            *imptr = 1;
        
        im1ptr++;
        im2ptr++;
        imptr++;
    }    

    return(im);
}

CImage mask(const CImage &im, const CImage &mask)
{
    assert(im.nRows == mask.nRows);
    assert(im.nCols == mask.nCols);
    assert(mask.nBands == 1);

    CImage mim(im.nRows, im.nCols, im.nBands);
    
    float *imptr   = im.data;
    float *maskptr = mask.data;
    float *mimptr  = mim.data;

    for (int i = 0; i < im.nRows * im.nCols; i++)
    {
        if (*maskptr > 0)
        {
            memcpy(mimptr, imptr, im.nBands * sizeof(float));
        }
        
        mimptr += im.nBands;
        imptr += im.nBands;
        maskptr++;
    }

    return(mim);
}


CImage operator*(const CImage &im1, float f)
{
    CImage im(im1.nRows, im1.nCols, im1.nBands);

    float *im1ptr = im1.data;
    float *imptr = im.data;

    for (int i = 0; i < im.nRows * im.nCols * im.nBands; i++)
    {
        *imptr = *im1ptr * f;
        im1ptr++;
        imptr++;
    }

    return(im);
}

CImage operator*(const CImage &im1, vector<float> f)
{
    assert(f.size() == im1.nBands);

    CImage im(im1.nRows, im1.nCols, im1.nBands);

    float *im1ptr = im1.data;
    float *imptr = im.data;

    for (int i = 0; i < im.nRows * im.nCols; i++)
    {
        for (int b = 0; b < im1.nBands; b++)	
        {
            *imptr = *im1ptr * f[b];
            imptr++;
            im1ptr++;
        }
    }

    return(im);
}

CImage operator+(const CImage &im1, float f)
{
    CImage im(im1.nRows, im1.nCols, im1.nBands);

    float *im1ptr = im1.data;
    float *imptr = im.data;
	 
    for (int i = 0; i < im.nRows * im.nCols * im.nBands; i++)
    {
        *imptr = *im1ptr + f;
        im1ptr++;
        imptr++;
    }

    return(im);
}

void ImageExtend(CImage &im, vector<int> &rowBegin, vector<int> &rowEnd, vector<int> &colBegin, vector<int> &colEnd)
// simple method of extending an image outside its boundaries
// by adding half the nearest row / column value
{
    int nRows = im.nRows;
    int nCols = im.nCols;
    int nBands = im.nBands;

    assert(nRows == rowBegin.size());
    assert(nRows == rowEnd.size());
    assert(nCols == colBegin.size());
    assert(nCols == colEnd.size());
    
    float *fillptr;
    float *rowptr = im.data;    
    float *rowptr0 = rowptr;
    float *colptr = im.data;    
    float *colptr0 = colptr;
        
    for (int row = 0; row < nRows; row++)
    {
        fillptr = &im(row, rowBegin[row], 0);
        rowptr = rowptr0;
        
        for (int col = 0; col < rowBegin[row]; col++)
        {
            for (int b = 0; b < nBands; b++)
            {
                *rowptr = 0.5f * *fillptr;
                rowptr++;
                fillptr++;
            }
            fillptr -= nBands;
        }
        
        fillptr = &im(row, rowEnd[row], 0);
        rowptr = rowptr0 + (rowEnd[row] + 1) * nBands;
        
        
        for (int col = rowEnd[row] + 1; col < nCols; col++)
        {
            for (int b = 0; b < nBands; b++)
            {
                *rowptr = 0.5f * *fillptr;
                rowptr++;
                fillptr++;
            }
            fillptr -= nBands;
        }    
        
        rowptr0 += nCols * nBands;
    }

    for (int col = 0; col < nCols; col++)
    {
        fillptr = &im(colBegin[col], col, 0);
        colptr = colptr0;
        
        for (int row = 0; row < colBegin[col]; row++)
        {
            for (int b = 0; b < nBands; b++)
            {
                *colptr += 0.5f * *fillptr;
                colptr++;
                fillptr++;
            }
            fillptr -= nBands;
            colptr -= nBands;
            colptr += nCols * nBands;
        }
            
        fillptr = &im(colEnd[col], col, 0);
        colptr = colptr0 + (nRows - 1) * nCols * nBands;
        
        for (int row = nRows - 1; row > colEnd[col]; row--)
        {
            for (int b = 0; b < nBands; b++)
            {
                *colptr += 0.5f * *fillptr;
                colptr++;
                fillptr++;
            }
            fillptr -= nBands;
            colptr -= nBands;
            colptr -= nCols * nBands;
        }    
        
        colptr0 += nBands;
    }
    
//    cerr << "end image extend" << endl;
}// end ImageExtend

void ZeroInvalid(CImage &im, vector<int> &rowBegin, vector<int> &rowEnd, vector<int> &colBegin, vector<int> &colEnd)
{
    int nRows = im.nRows;
    int nCols = im.nCols;
    int nBands = im.nBands;

    assert(nRows == rowBegin.size());
    assert(nRows == rowEnd.size());
    assert(nCols == colBegin.size());
    assert(nCols == colEnd.size());

    float *rowptr = im.data;
    float *colptr = im.data;
    float *colptr0 = colptr;
    
    for (int row = 0; row < nRows; row++)
    {        
        if (rowBegin[row] > 0)
        {
            memset(rowptr, 0, nBands * rowBegin[row] * sizeof(float));
        }
        
        if (rowEnd[row] < nCols - 1)
        {
            memset(rowptr + (rowEnd[row] + 1) * nBands, 0, (nCols - 1 - rowEnd[row]) * nBands * sizeof(float));
        }

        rowptr += nCols * nBands;
    }// end for row

    for (int col = 0; col < nCols; col++)
    {
        colptr = colptr0;
        
        for (int row = 0; row < colBegin[col]; row++)
        {
            memset(colptr, 0, nBands * sizeof(float));
            colptr += nBands * nCols;
        }
            
        colptr = colptr0 + (nRows - 1) * nCols * nBands;
        
        for (int row = nRows - 1; row > colEnd[col]; row--)
        {
            memset(colptr, 0, nBands * sizeof(float));
            colptr -= nBands * nCols;
        }
        
        colptr0 += nBands;
    }// end for col

}// end ZeroInvalid


