//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// Panorama.cpp

#include "Panorama.h"
#include "RenderPanorama.h"
#include "MatrixHelpers.h"
#include <string>
#include <algorithm>
#include <fstream>

CPanorama::CPanorama() : isValid(false)
{ }

CPanorama::~CPanorama()
{ }

void CPanorama::resetParameters(void)
{
    SIFTParameters.reset();
    RANSACParameters.reset();
    BundleParameters.reset();
    RenderParameters.reset();
    SystemParameters.reset();
    needRealign = true;
}

void CPanorama::Create(string imageDir0, string filename, CByteImage &panoImage) 
// Create from file
{
    Cameras.clear();
    imageDir = imageDir0;
    filenames.clear();
    imageSize0.clear();
    images.clear();
    subsampleLevel.clear();
    thumbs.clear();
    thumbSubsampleLevel.clear();
    SIFTImageMemory = 0;
    panoramaImageID.clear();
    isValid = true;
    needRealign = true;

    // Read filenames, Cameras and imageSize0
    ifstream is(filename.c_str());
    is >> *this;
    is.close();

    int nImages = (int)filenames.size();

    images.resize(nImages);
    thumbs.resize(nImages);
    subsampleLevel.resize(nImages);
    thumbSubsampleLevel.resize(nImages);
    for (int i = 0; i < nImages; i++)
    {
        subsampleLevel[i] = 0;
        thumbSubsampleLevel[i] = 0;
    }

    CBoolVec imageIsValid;
    imageIsValid.resize(nImages);
    for (int i = 0; i < nImages; i++)
    {
        if (Cameras[i].f == 0)
            imageIsValid[i] = false;
        else 
            imageIsValid[i] = true;
    }
    panoramaImageID = imageIsValid.Find();

    isValid = true;
    needRealign = false;

    status.renderOnly = true;
    Render(panoImage);
    status.renderOnly = false;
}

void CPanorama::Create(string imageDir0, vector<string> filenames0, CByteImage &panoImage)
{    
    //
    // Clear parameters
    //
    Cameras.clear();
    imageDir = imageDir0;
    filenames = filenames0;
    imageSize0.clear();
    images.clear();
    subsampleLevel.clear();
    thumbs.clear();
    thumbSubsampleLevel.clear();
    SIFTImageMemory = 0;
    panoramaImageID.clear();
    isValid = true;
    needRealign = true;

    int nImages = (int)filenames.size();

    SIFTParameters.nMatchesPerFeature = min(SIFTParameters.nMatchesPerFeature, nImages - 1);

    //
    // Match images
    //
    SIFTParameters.maxMemory = (int)(SystemParameters.maxMemory * SystemParameters.SIFTImageMemoryFrac);
    CMultiImageMatch MultiImageMatch(SIFTParameters, RANSACParameters);
    MultiImageMatch.Create(imageDir, filenames, images, imageSize0, subsampleLevel, thumbs, thumbSubsampleLevel, SIFTImageMemory, status);

    // No image matches found
    if (MultiImageMatch.ConnectedComponent.data.size() == 0)
    {
        cerr << " No image matches found" << endl;
        return;	
    }

    //MultiImageMatch.WriteToFile();
    cerr << " Extracted " << MultiImageMatch.nFeatures << " SIFT features" << endl;
    cerr << " Found " << MultiImageMatch.nMatchesTotal() << " consistent matches" << endl; 
        	
    //cerr << " SIFT Image memory = " << SIFTImageMemory << endl;

    vector<SImageSize> imageSize;
    imageSize.resize(nImages);
    for (int i = 0; i < nImages; i++)
    {
        float scale         = (float)pow(2.0f, subsampleLevel[i]);
        imageSize[i].nRows  = (int)(imageSize0[i].nRows / scale);
        imageSize[i].nCols  = (int)(imageSize0[i].nCols / scale);
        imageSize[i].nBands = imageSize0[i].nBands;
    }

    // Initialise cameras
    Cameras.resize(nImages);

    // Normalise coordinates so relative to principal point 
    NormaliseMatches(MultiImageMatch, imageSize);

    // Transform camera matrix so relative to original images
    for (int i = 0; i < nImages; i++)
    {
        Cameras[i].T = CRenderPanorama::T(subsampleLevel[i]) * Cameras[i].T;
    }

    // Select largest connected component of image matches
    panoramaImageID = MultiImageMatch.ConnectedComponent[0];

    //
    // Bundle adjust 
    //

    CBundleAdjust BundleAdjust(BundleParameters);
	
    EErrorFunction errorFunction               = BundleParameters.initialErrorFunction;
    float outlierDistance                      = BundleParameters.initialOutlierDistance;
    EOutlierDistanceUnits outlierDistanceUnits = BundleParameters.initialOutlierDistanceUnits;

    int nPanorama = (int)panoramaImageID.size();
    vector<bool> isRegistered;
    isRegistered.resize(nImages);
    for (int i = 0; i < nImages; i++)
        isRegistered[i] = false;
    float f0 = BundleAdjust.Param.f0;

    int bestRegIm, bestNonRegIm;
    
    cerr << "[ Bundle Adjustment ]" << endl;
    cerr << " Camera 0 " << endl;
    BestNextImage0(MultiImageMatch.nMatches, MultiImageMatch.nImages, panoramaImageID, bestRegIm, bestNonRegIm);
    Cameras[bestRegIm].R.Create(3, 3);
    Cameras[bestRegIm].R.eye();	
    if (BundleParameters.autoFocalLengthInit)
        f0 = (float)max(imageSize[bestRegIm].nRows, imageSize[bestRegIm].nCols);	
    // heuristic focal length initialisation - assume FOV is 2 * arctan(0.5) = 53 degrees
    Cameras[bestRegIm].f = f0;
    cerr << "f0 = " << f0 << endl;
    isRegistered[bestRegIm] = true;
    int nRegisteredImages = 1;

    while ( nRegisteredImages < nPanorama )
    {
        status.Update(eBundleAdjust, nRegisteredImages, nPanorama - 1);
		
        cerr << " Camera " << nRegisteredImages << endl;
        
        BestNextImage(MultiImageMatch.nMatches, MultiImageMatch.nImages, panoramaImageID, isRegistered, bestRegIm, bestNonRegIm);
        
        cerr << " Best next image = " << bestNonRegIm;
        cerr << ", Best registered image = " << bestRegIm << endl;

        Cameras[bestNonRegIm].R = Cameras[bestRegIm].R;
        Cameras[bestNonRegIm].f = Cameras[bestRegIm].f;
        
        vector<int> TODOImages;
        TODOImages.push_back(bestRegIm);
        TODOImages.push_back(bestNonRegIm);

        BundleAdjust.BundleAdjust(MultiImageMatch, TODOImages, Cameras, errorFunction, outlierDistance, outlierDistanceUnits);

        cerr << " f = " << Cameras[bestNonRegIm].f << endl;

        isRegistered[bestNonRegIm] = true;
        nRegisteredImages++;  

        if (RenderParameters.renderEachIteration)
        {
            CRenderParameters previewRenderParameters = RenderParameters;		
            previewRenderParameters.width = 500;
            previewRenderParameters.blendingMethod = eNoBlend;
            previewRenderParameters.autoCrop = eNone;

            CByteImage previewImage;

            char filename[20];
            sprintf(filename, "tmp%d.jpg", nRegisteredImages);

            CRenderPanorama previewRenderPanorama(previewRenderParameters);
            previewRenderPanorama.Render(previewImage, *this);

            previewImage.WriteJPEG(filename, 75);
        }

    }// end while nRegisteredImages

    // Global bundle adjustment
    cerr << "[ Global Bundle Adjustment ]" << endl;
    status.Update(eBundleAdjustFinal, 1, 3);

    BundleAdjust.BundleAdjust(MultiImageMatch, panoramaImageID, Cameras, errorFunction, outlierDistance, outlierDistanceUnits);
    status.Update(eBundleAdjustFinal, 2, 3);

    cerr << "[ Final Bundle Adjustment ]" << endl;

    errorFunction        = BundleParameters.finalErrorFunction;
    outlierDistance      = BundleParameters.finalOutlierDistance;
    outlierDistanceUnits = BundleParameters.finalOutlierDistanceUnits;

    BundleAdjust.BundleAdjust(MultiImageMatch, panoramaImageID, Cameras, errorFunction, outlierDistance, outlierDistanceUnits);
    status.Update(eBundleAdjustFinal, 3, 3);


    needRealign = false;

    //
    // Rendering
    //
	
    cerr << "[ Render Panorama ]" << endl;
    Render(panoImage);	
}

void CPanorama::NormaliseMatches(CMultiImageMatch &MultiImageMatch, vector<SImageSize> &imageSize)
{ 
    CMatch2D** matches = MultiImageMatch.matches;
    int**     nMatches = MultiImageMatch.nMatches;
    int       nImages  = MultiImageMatch.nImages;

    vector<CMatrix> Tinv;
    Tinv.resize(nImages);
     
    for (int i = 0; i < nImages; i++)
    {
        int nRowsi = imageSize[i].nRows;
        int nColsi = imageSize[i].nCols;
        
        Cameras[i].T.Create(3, 3);
        Cameras[i].T.eye();
        Cameras[i].T(0, 2) = (nRowsi - 1)/2.0f;
        Cameras[i].T(1, 2) = (nColsi - 1)/2.0f;

        Tinv[i].Create(3, 3);
        Tinv[i].eye();
        Tinv[i](0, 2) = -(nRowsi - 1)/2.0f;
        Tinv[i](1, 2) = -(nColsi - 1)/2.0f;
    }        

    for (int i = 0; i < nImages; i++)
    {
        for (int j = 0; j < nImages; j++)
        {
            if (nMatches[i][j] > 0)
            {
                CMatrix matchij0 = matches[i][j][0];
                matchij0 = homogeneous(matchij0);
                matchij0 = Tinv[i] * matchij0;
                matches[i][j][0] = unhomogeneous(matchij0);

                CMatrix matchij1 = matches[i][j][1];
                matchij1 = homogeneous(matchij1);
                matchij1 = Tinv[j] * matchij1;
                matches[i][j][1] = unhomogeneous(matchij1);
            }// end if nMatches[i][j] > 0

        }// end for j

    }// end for i

    
}// end NormaliseMatches

void CPanorama::BestNextImage(int** nMatches, int nImages, vector<int> &panorama, vector<bool> &isRegistered, int &bestRegIm, int &bestNonRegIm)
// return index of image with maximum number of matches to the registered images
{
    int nPanorama = (int)panorama.size();

    vector<int> registeredImages;
    vector<int> nonRegisteredImages;
    for (int i = 0; i < nPanorama; i++)
    {
        if (isRegistered[panorama[i]])
            registeredImages.push_back(panorama[i]);
        else
            nonRegisteredImages.push_back(panorama[i]);
    }
    
    // find maximum number of matches between registered and non registered
    
    int maxNMatches = 0;
    bestRegIm = 0; 
    bestNonRegIm = 0;
    
    for (int i = 0; i < (int)registeredImages.size(); i++)
    {
        int regID = registeredImages[i];
        for (int j = 0; j < (int)nonRegisteredImages.size(); j++)
        {
            int nonRegID = nonRegisteredImages[j];
            
            int maxNMatches12 = max(nMatches[regID][nonRegID], nMatches[nonRegID][regID]);
            
            if (maxNMatches12 > maxNMatches)
            {
                maxNMatches = maxNMatches12;
                bestRegIm = regID;
                bestNonRegIm = nonRegID;
            }// end if
            
        } // end for j
    }// end for i
    
}// end BestNextImage
    
void CPanorama::BestNextImage0(int** nMatches, int nImages, vector<int> &panorama, int &bestRegIm, int &bestNonRegIm)
{
    int maxNMatches = 0;
    bestRegIm = 0; 
    bestNonRegIm = 0;

    int nPanorama = (int)panorama.size();
    
    for (int i = 0; i < nPanorama; i++)
    {
        int im1ID = panorama[i];
        for (int j = 0; j < nPanorama; j++)
        {
            int im2ID = panorama[j];
            if (nMatches[im1ID][im2ID] > maxNMatches)
            {
                maxNMatches  = nMatches[im1ID][im2ID];
                bestRegIm    = im1ID;
                bestNonRegIm = im2ID;
            }                           
        }// end for j
    }// end for i

}

void CPanorama::PreRotateImages(CMatrix R, vector<int> &panorama)
{
    vector<int>::iterator it = panorama.begin();
    
    while(it != panorama.end())
    {
        int imageID = *it;
        Cameras[imageID].R = R * Cameras[imageID].R;
        it++;
    }
}  

void CPanorama::PostRotateImages(CMatrix R, vector<int> &panorama)
{
    vector<int>::iterator it = panorama.begin();
    
    while(it != panorama.end())
    {
        int imageID = *it;
        Cameras[imageID].R = Cameras[imageID].R * R;
        it++;
    }
}  

void CPanorama::AlignBasic(vector<int> &panoramaImageID)
// Rotate images so that R[0] = I
{

    // get the first image
    int imageID0 = *min_element(panoramaImageID.begin(), panoramaImageID.end());
    CMatrix R0 = Cameras[imageID0].R;
    CMatrix R0t = ~R0;

    // apply image rotation
    EImageRotation imageRotation = RenderParameters.imageRotation;
    CMatrix Rim(3, 3);
    
    switch( imageRotation )
    {
    case eUpright:       Rim.eye();         break;
    case eClockwise:     Rim = Rz((float)-M_PI/2); break;
    case eAntiClockwise: Rim = Rz((float)M_PI/2);  break;
    case eUpsideDown:    Rim = Rz((float)M_PI);    break;
    }

    // rotate images
    CMatrix R = R0t * Rim;
    PostRotateImages(R, panoramaImageID);
}


void CPanorama::AlignUpVector(vector<int> &panoramaImageID) 
// rotate images so that the up direction is perpendicular to the plane of camera horizons
{

    EImageRotation imageRotation = RenderParameters.imageRotation;

    switch( imageRotation )
    {
    case eUpright:       /* do nothing */                        break;
    case eClockwise:     PreRotateImages(Rz((float)M_PI/2), panoramaImageID);  break;
    case eAntiClockwise: PreRotateImages(Rz((float)-M_PI/2), panoramaImageID); break;
    case eUpsideDown:    PreRotateImages(Rz((float)-M_PI), panoramaImageID);   break;
    }
    
    // compute covariance matrix for camera Y vectors
    
    CMatrix C(3, 3);

    int nImages = (int)panoramaImageID.size();
    for (int i = 0; i < nImages; i++)
    {
        int imageID = panoramaImageID[i];
        CMatrix Yi = Cameras[imageID].R.row(1);
        C = C + (~Yi) * Yi;
    }

    // compute null vector of covariance matrix
    CMatrix u = RightNull3x3(C);

    // find direction so that images are the right way up
    int nGt0 = 0;
    for (int i = 0; i < nImages; i++)
    {
        int imageID = panoramaImageID[i];
        float udotx = dot(u, ~(Cameras[imageID].R.row(0)));
        if (udotx > 0)
            nGt0++;
    }
    
    if (nGt0 > 0.5 * nImages)
    {
        // do nothing
    }
    else 
    {
        u = -u;
    }
    
    // verify that camera z axes are mostly perpendicular to u
    // (require that average magnitude of angle between u and z is greater than minAngle)
    
    float minAngle = 35.0f;
    float angSum = 0.0f;
        
    for (int i = 0; i < nImages; i++)
    {
        int imageID = panoramaImageID[i];
        CMatrix Zi = ~(Cameras[imageID].R.row(2));
        float udotzi = fabs(dot(Zi, u));
        float angi   = acos(udotzi) * 180 / M_PI;
        angSum += angi;
        //cerr << "angi = " << angi << endl;
    }
    float angAv = angSum / (float)nImages;
    //printf("average angle z->u = %.1f degrees\n", angAv);

    if (angAv < minAngle)
    {
        printf("AlignUpVector: failed to find a good up vector\n");
        printf(" average angle between camera pointing and gravity = %.1f degrees\n", angAv);
        //printf(" Reverting to AlignBasic...\n");
        //AlignBasic(panoramaImageID);
        //return;

        printf(" using average camera Y axis as up vector\n");
        CMatrix sumX(3, 1);
        sumX.zeros();        
        for (int i = 0; i < nImages; i++)
        {
            int imageID = panoramaImageID[i];
            CMatrix Xi = ~(Cameras[imageID].R.row(0));
            sumX = sumX + Xi;
        }
        u = sumX / sumX.norm();
    }

    // rotate images using the up vector
    CMatrix X0(3, 1); 
    X0[0] = 1;

    switch( imageRotation )
    {
    case eUpright:       /* do nothing */ break;
    case eClockwise:     X0 = Rz((float)-M_PI/2) * X0; PreRotateImages(Rz((float)-M_PI/2), panoramaImageID); break;
    case eAntiClockwise: X0 = Rz((float)M_PI/2) * X0;  PreRotateImages(Rz((float)M_PI/2), panoramaImageID);  break;
    case eUpsideDown:    X0 = Rz((float)M_PI) * X0;    PreRotateImages(Rz((float)M_PI), panoramaImageID);    break;
    }

    CMatrix R0 = RotationBetweenVectors(u, X0);
    CMatrix R0t = (~R0);

    CMatrix Rim(3, 3);

    switch( imageRotation )
    {
    case eUpright:       Rim.eye();         break;
    case eClockwise:     Rim = Rz((float)-M_PI/2); break;
    case eAntiClockwise: Rim = Rz((float)M_PI/2);  break;
    case eUpsideDown:    Rim = Rz((float)M_PI);    break;
    }

    // rotate images
    CMatrix R = R0t * Rim;
    PostRotateImages(R, panoramaImageID);
    
    // Centre the first image
    int imageID0 = *min_element(panoramaImageID.begin(), panoramaImageID.end());
    CMatrix Z0 = ~(Cameras[imageID0].R.row(2));
    Z0[0] = 0;
    Z0 = Z0 / Z0.norm();
    CMatrix Z(3, 1);
    Z[2] = 1;
    CMatrix Rc = RotationBetweenVectors(Z0, Z);
    CMatrix Rct = (~Rc);
    PostRotateImages(Rct, panoramaImageID);    
}

void CPanorama::Render(CByteImage &panoImage)
{
    if (!RenderParameters.render)
        return;

    RenderParameters.maxMemory = SystemParameters.maxMemory - SIFTImageMemory;
    
    // Up-vector Alignment 
    bool autoStraighten = RenderParameters.autoStraighten;
    if (autoStraighten)
        AlignUpVector(panoramaImageID);
    else
        AlignBasic(panoramaImageID);
    
    // Extra rotations
    float thetaExtra = RenderParameters.thetaExtra;
    float phiExtra   = RenderParameters.phiExtra;
    float psiExtra   = RenderParameters.psiExtra;
    
    if (thetaExtra != 0.0f)
        PostRotateImages(Rx(thetaExtra), panoramaImageID);
    if (phiExtra != 0.0f)
        PostRotateImages(Ry(phiExtra), panoramaImageID);
    if (psiExtra != 0.0f)
        PostRotateImages(Rz(psiExtra), panoramaImageID);
    
    CRenderPanorama RenderPanorama(RenderParameters);
    RenderPanorama.Render(panoImage, *this);
}

ostream& operator<<(ostream& os, const CPanorama &pano)
{

    int nImages = (int)pano.filenames.size();
	
    bool outputHomography = pano.SystemParameters.outputHomography;
    const vector<CCamera> &Cameras = pano.Cameras;

    CBoolVec imageIsValid; 
    imageIsValid.resize(nImages);

    imageIsValid.Set(pano.panoramaImageID);

    for (int i = 0; i < (int)pano.filenames.size(); i++)
    {
        os << pano.filenames[i].c_str() << endl;
        os << pano.imageSize0[i].nCols << " " << pano.imageSize0[i].nRows << endl;
		
        if (outputHomography)
        {
            if (imageIsValid[i])
            {	
                CMatrix P = Cameras[i].T * K(Cameras[i].f) * Cameras[i].R;
                os << P << endl;
            }
            else
            {
                CMatrix Zero3x3(3, 3); Zero3x3.zeros();
                os << Zero3x3 << endl;
            }
        }
        else // if !outputHomography
        {
            if (imageIsValid[i])
            {
                os << endl;			
                os << Cameras[i].T << endl;
                os << Cameras[i].R << endl;
                os << Cameras[i].f << endl;
            }
            else
            {
                CMatrix Zero3x3(3, 3); Zero3x3.zeros();
                os << Cameras[i].T << endl;
                os << Zero3x3 << endl;
                os << 0 << endl;
            }

            os << endl;
        } // end if !outputHomography

    }// end for i

    return(os);
}

istream& operator>>(istream &is, CPanorama &pano)
{
    string filename;
    is >> filename;
    while( is )
    {
        CCamera camera;
        camera.R.Create(3, 3);
        camera.T.Create(3, 3);

        SImageSize imSize;

        is >> imSize.nCols >> imSize.nRows;

        is >> camera.T;
        is >> camera.R;		
        is >> camera.f;

        pano.filenames.push_back(filename);
        pano.imageSize0.push_back(imSize);
        pano.Cameras.push_back(camera);

        is >> filename;
    }//end while( is )

    return(is);
}
