//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// ConnectedComponent.h

#pragma once

#include "BoolVec.h"
#include <vector>

class CConnectedComponent
{
public:
    CConnectedComponent() { }
    ~CConnectedComponent() { }
    
    void Create(int** nMatches, int nImages);
    CBoolVec AddNeighbours(CBoolVec b, int** nMatches, int nImages);

    const vector<int> &operator[](int i) const { return data[i]; }
    
    vector< vector<int> > data;     
};
