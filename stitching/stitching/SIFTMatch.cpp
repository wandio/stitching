//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// SIFTMatch.cpp

#include <string>

#include "SIFTMatch.h"

#include "key.h"
#include "util.h"
#include "nn.h"

void CSIFTMatch::Create(string imageDir, vector<string> filenames, vector<CImage> &images, vector<SImageSize> &imageSize0, vector<int> &subsampleLevel, vector<CImage> &thumbs, vector<int> &thumbSubsampleLevel, 
                        CMatch2D** matches, int** nMatches, int &SIFTImageMemory, int &nFeatures, CPanoramaStatus &status)
// 
// Find matches between images 
// using SIFT features and k-d tree 
//
{
    //
    // Load images
    //
    cerr << "[ Load images and extract SIFT features ]" << endl;

    int nImages = (int)filenames.size();
    int minImageDim = Param.minImageDim;
    int minImageDimThumb = Param.minImageDimThumb;
    int smallImageDim = Param.smallImageDim;
    images.resize(nImages);
    thumbs.resize(nImages);
    imageSize0.resize(nImages);
    subsampleLevel.resize(nImages);
    thumbSubsampleLevel.resize(nImages);
    for (int i = 0; i < nImages; i++)
    {
        subsampleLevel[i] = 0;
        thumbSubsampleLevel[i] = 0;
    }
    Keypoint *keys = new Keypoint[nImages];	

    //
    // Subsample images by 2x2 block averages
    //
    SIFTImageMemory = 0;
    for (int i = 0; i < nImages; i++)
    {
        cerr << "  Image " << i << endl;
        status.Update(eSIFTExtract, i, nImages - 1);

        int SkipLevels = Param.SkipLevels;

        string imageFilenamei = imageDir + filenames[i];
        images[i].Create(imageFilenamei.c_str());

        imageSize0[i].nRows  = images[i].nRows;
        imageSize0[i].nCols  = images[i].nCols;
        imageSize0[i].nBands = images[i].nBands;

        if (Param.sizeSelector == eMinDim)
        {
            while( (images[i].nRows >= 2 * minImageDim) && (images[i].nCols >= 2 * minImageDim) )
            {
                images[i] = SubSample2Block(images[i]);
                subsampleLevel[i]++;
            }
        }
        else // if Param.sizeSelector == eRelativeScale
        {
            while( (images[i].nRows >= 2 * Param.relativeScale * imageSize0[i].nRows) &&
                   (images[i].nCols >= 2 * Param.relativeScale * imageSize0[i].nCols))
            {
                images[i] = SubSample2Block(images[i]);
                subsampleLevel[i]++;
            }
        }// end else sizeSelector == eRelativeScale

        //
        // Compute thumbnail image
        //
		
        // downsample SIFT image
        if ( (images[i].nRows >= 2 * minImageDimThumb) && (images[i].nCols >= 2 * minImageDimThumb) )
        {
            thumbs[i] = SubSample2Block(images[i]);
            thumbSubsampleLevel[i] = subsampleLevel[i] + 1;
        }
        else
        {
            thumbs[i] = images[i];
            thumbSubsampleLevel[i] = subsampleLevel[i];
        }

        // continue to downsample as required
        while( (thumbs[i].nRows >= 2 * minImageDimThumb) && (thumbs[i].nCols >= 2 * minImageDimThumb) )
        {
            thumbs[i] = SubSample2Block(thumbs[i]);
            thumbSubsampleLevel[i]++;
        }
        SIFTImageMemory += thumbs[i].nRows * thumbs[i].nCols * thumbs[i].nBands * sizeof(float);

        // Find more SIFT features for small images
        if (min(images[i].nRows, images[i].nCols) < smallImageDim)
            SkipLevels = 1;
        if (min(images[i].nRows, images[i].nCols) < smallImageDim/sqrt(2.0f))
            SkipLevels = 0;

        // Convert to greyscale image in David Lowe's format
        Image greyIm;
        CImage &colIm = images[i];
        int nRows     = colIm.nRows;
        int nCols     = colIm.nCols;
        int nBands    = colIm.nBands;
        float* data   = colIm.data;

        //greyIm       = CreateImage(nRows, nCols, PERM_POOL);
        greyIm       = CreateImage(nRows, nCols, IMAGE_POOL);
        greyIm->rows = nRows;
        greyIm->cols = nCols; 
        
        float* rowptr = data;

        for (int r = 0; r < nRows; r++)
        {
            float *colptr = rowptr;
            for (int c = 0; c < nCols; c++)
            {
                float greyvalue = 0;               
                float *bandptr = colptr;

                for (int b = 0; b < nBands; b++)
                {
                    greyvalue += *bandptr;
                    bandptr++;
                }// end for b
                greyvalue /= (float)nBands;
                greyIm->pixels[r][c] = greyvalue;

                colptr += nBands;
            }// end for c

            rowptr += nCols * nBands;
        }// end for r

        //
        // Extract SIFT features 
        //
        //keys[i] = GetKeypoints(greyIm, SkipLevels, Param.maxNInterestPointsImage, Param.crobust); 
        keys[i] = GetKeypoints(greyIm, SkipLevels);

        // Erase this image if it takes up too much memory
        if ((nRows * nCols) > (Param.maxMemory/(nBands * (int)sizeof(float) * nImages)))
        {
            //cerr << " Clearing image to save memory" << endl;
            images[i].Clear();
        }
        else	
        {
            SIFTImageMemory += nRows * nCols * nBands * sizeof(float);
        }

        FreeStoragePool(IMAGE_POOL);

    }// end for i = 1:nImages
    
   
    // Link keypoints 
    Keypoint k = keys[0]; 
    nFeatures = 0;
    int im = 0;
    while (k == NULL && im < nImages - 1)
    { 
        im++; 
        k = keys[im]; 
    }
    if (k == NULL) // no keypoints!
    { 
        cerr <<  "No features found!" << endl;
        return; 
    }    
    
    Keypoint kfirst = k;
    Keypoint klast = k;
   
    while (k != NULL)
    {
        k->imageID = im;
        klast = k;
        k = k->next;
        nFeatures++;
    }
    im++;

    while (im < nImages)
    {
        Keypoint k = keys[im];
        klast->next = k;
        
        while (k != NULL)
        {
            k->imageID = im;
            klast = k;
            k = k->next;
            nFeatures++;
        }
        
        im++;
    }

    // Build k-d tree 
    cerr << "[ Build k-d tree           ]" << endl;
    BuildAccessTree(kfirst);
    
    // Find nearest neighbours
    cerr << "[ Find nearest neighbours  ]" << endl;              
    int nMatchesPerFeature = Param.nMatchesPerFeature;
    int nOverlap = Param.nOverlap;
    nOverlap = max(nOverlap, nMatchesPerFeature + 1);

    int nImage = -1;
    for (k = kfirst; k != NULL; k = k->next)
    {
        if (k->imageID != nImage)
        { 
            nImage = k->imageID; 
            cerr << "  Image " <<  nImage << endl;
            status.Update(eSIFTMatch, nImage, nImages - 1);
        }
        
        // NOTE: calling new for every keypoint is slow, 
        // use pooled storage allocator or fixed constant here
        //k->neighbours = new Keypoint[nOverlap];
        k->neighbours = (Keypoint *)MallocPool(nOverlap * sizeof(Keypoint), KEY_POOL);
				
        FindNeighbours(k->neighbours, nOverlap, k);

        // Reject matches that are > outlierDist * doutlier
        float outlierDist = Param.outlierDist;
        float doutlier = (float)k->neighbours[nOverlap - 1]->distsq;
        bool isValid = true; 
        int numNN = 0;
        while (isValid && (numNN < nMatchesPerFeature))
        {
            isValid = (k->neighbours[numNN]->distsq < (outlierDist * doutlier));
            if (isValid) numNN++;
        }
        k->numNN = numNN;

        // increment nMatches array
        for (int i = 0; i < numNN; i++)
            nMatches[k->imageID][k->neighbours[i]->imageID]++;      
    }
    
    cerr << "[ Build match array        ]" << endl;

    // Allocate space for match array
    
    cerr << "[ Allocate Space for match array ]" << endl;
    for (int i = 0; i < nImages; i++)
    {
        for (int j = 0; j < nImages; j++)
        {
            matches[i][j].Create(nMatches[i][j]);
        }
    }
 
    // Fill match arr
    int **nMatchesCount = new int*[nImages];
    for (int i = 0; i < nImages; i++) {
        nMatchesCount[i] = new int[nImages];
        for (int j = 0; j < nImages; j++) {
            nMatchesCount[i][j] = 0; 
        }
    }

    nImage = -1;
    for (k = kfirst; k != NULL; k = k->next)
    {
        if (k->imageID != nImage)
        { 
            nImage = k->imageID; 
            cerr << "  Image " <<  nImage << endl;
        }
		
        int imageID1 = k->imageID;
        float i1    = (float)k->row;
        float j1    = (float)k->col;
        
        for (int i = 0; i < k->numNN; i++)
        {
            int imageID2 = k->neighbours[i]->imageID;
            float i2    = (float)k->neighbours[i]->row;
            float j2    = (float)k->neighbours[i]->col;

            int n = nMatchesCount[imageID1][imageID2];
            
            CMatch2D &MatchIJ = matches[imageID1][imageID2];
            MatchIJ[0](0, n) = i1;
            MatchIJ[0](1, n) = j1;
            MatchIJ[1](0, n) = i2;
            MatchIJ[1](1, n) = j2;
            nMatchesCount[imageID1][imageID2]++;
        }
    }
    
    // Delete allocated memory
    cerr << "Free SIFT storage" << endl;  
    for (int i = 0; i < nImages; i++)
    {
        delete[] nMatchesCount[i];
    }
    delete[] nMatchesCount;
	
    //for (k = kfirst; k != NULL; k = k->next)
    //{ delete[] k->neighbours; }
    delete[] keys;   

    // Free storage pools used by SIFT
    //FreeStoragePool(PERM_POOL);
    FreeStoragePool(IMAGE_POOL);
    //FreeStoragePool(MODEL_POOL);
    FreeStoragePool(TREE_POOL);
    FreeStoragePool(KEY_POOL);

}// end CSIFTMatch::Create

