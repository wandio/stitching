//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// BoolVec.cpp

#include "BoolVec.h"
#include "assert.h"
#include <algorithm>

ostream& operator << (ostream &os, CBoolVec &b)
{    
    for (int i = 0; i < (int)b.size(); i++)
    {
        os << (int)b[i] << " ";
    }
    
    return(os);
}

void CBoolVec::operator=(bool b)
{
    CBoolVec::iterator i = this->begin();
    while(i != this->end())
    {
        *i = b;
        i++;
    }
}

bool CBoolVec::operator==(bool b)
{
    CBoolVec::iterator i = this->begin();
  
    if (i == this->end()) return false;	// vector is empty
	
    while (i != this->end())
    {
        if (*i != b) return false;
        i++;
    }

    return true;
}

bool CBoolVec::operator!=(bool b)
{
    return (!((operator==)(b)));
}
    
int CBoolVec::FirstNonZeroElement(void)
// returns index of first non-zero element in BoolVec
// returns -1 for a vector of zeros
{
    CBoolVec& b = *this;
    
    for (int i = 0; i < (int)b.size(); i++)
    {
        if (b[i] == true)
            return(i);
    }

    return(-1);
}

vector<int> CBoolVec::Find(void)
// returns a vector of ints containing the indices of non-zero elements in the BoolVec
{
    vector<int> indices;
    CBoolVec::iterator i = begin();
    int index = 0;
    while(i != end())
    {
        if (*i == true)
            indices.push_back(index);        
        index++;
        i++;
    }
    
    return(indices);
}

void CBoolVec::Set(vector<int> v)
{
    int m = *max_element(v.begin(), v.end());

    if ((int)size() < m + 1)
        this->resize(m + 1);
	
    (*this) = false;
    operator=(false);
    vector<int>::iterator vi;
    for (vi = v.begin(); vi != v.end(); vi++)
    {
        (*this)[*vi] = true;
    }
}


void CBoolVec::XOr(CBoolVec &b2)
{
    CBoolVec& b1 = *this;
    int b1size = (int)b1.size();
    int b2size = (int)b2.size();
    
    assert(b1size == b2size);
    
    for (int i = 0; i < b1size; i++)
    {
        // exclusive or
        b1[i] = (b1[i] && !b2[i]) || (!b1[i] && b2[i]);
    }    
}

