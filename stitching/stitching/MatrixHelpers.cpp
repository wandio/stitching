//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// MatrixHelpers.cpp

#include "MatrixHelpers.h"
#include "math.h"

CMatrix ComputeSimilarity(float u1, float v1, float x1, float y1, float u2, float v2, float x2, float y2, float minDist)
//
// Compute similarity transform from a pair of image matches 
// match1 = u1 v1 x1 y1
// match2 = u2 v2 x2 y2 
// such that [x, y] = S * [u, v]
//
{
//    cerr << "ComputeSimilarity" << endl;

    CMatrix S(3, 3);
        
    float dist1 = (float)sqrt((u1 - u2) * (u1 - u2) + (v1 - v2) * (v1 - v2));
    float dist2 = (float)sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    
    if ((dist1 < minDist) || (dist2 < minDist))
    {
        S.eye();
        return(S);
    }
                 
    CMatrix M(2, 2);
    M(0, 0) = u1 - u2; M(0, 1) = v1 - v2;
    M(1, 0) = v1 - v2; M(1, 1) = u2 - u1;
    CMatrix v(2, 1);
    v[0] = x1 - x2;
    v[1] = y1 - y2;

    CMatrix ab = SolveLinearSystem(M, v);
  
    float a = ab[0];
    float b = ab[1];
    float c = x1 - u1 * a - v1 * b;
    float d = y1 - v1 * a + u1 * b;
    
    S(0, 0) = a;  S(0, 1) = b; S(0, 2) = c;
    S(1, 0) = -b; S(1, 1) = a; S(1, 2) = d;
    S(2, 0) = 0;  S(2, 1) = 0; S(2, 2) = 1;

    return(S);
}

CMatrix K(float f)
{
    CMatrix Kf(3, 3);
    Kf(0, 0) = f;
    Kf(1, 1) = f;
    Kf(2, 2) = 1;
    return(Kf);
}

CMatrix Rx(float theta)
{
    CMatrix R(3, 3);
    
    R(0, 0) = 1.0f; R(0, 1) = 0.0f;              R(0, 2) = 0.0f;
    R(1, 0) = 0.0f; R(1, 1) = (float)cos(theta); R(1, 2) = -(float)sin(theta);
    R(2, 0) = 0.0f; R(2, 1) = (float)sin(theta); R(2, 2) =  (float)cos(theta);

    return(R);
}

CMatrix Ry(float theta)
{
    CMatrix R(3, 3);
    
    R(0, 0) = (float)cos(theta);  R(0, 1) = 0.0f; R(0, 2) = (float)sin(theta);
    R(1, 0) = 0.0f;               R(1, 1) = 1.0f; R(1, 2) = 0.0f;
    R(2, 0) = -(float)sin(theta); R(2, 1) = 0.0f; R(2, 2) = (float)cos(theta);

    return(R);
}

CMatrix Rz(float theta)
{
    CMatrix R(3, 3);
    
    R(0, 0) = (float)cos(theta); R(0, 1) = -(float)sin(theta); R(0, 2) = 0.0f;
    R(1, 0) = (float)sin(theta); R(1, 1) = (float)cos(theta);  R(1, 2) = 0.0f;
    R(2, 0) = 0.0f;              R(2, 1) = 0.0f;               R(2, 2) = 1.0f;

    return(R);
}

CMatrix RightNull3x3(const CMatrix &A)
// Find x such that  A x = 0
// with |x| = 1
// by solving linear equations with 
// x1 = 1, x2 = 1 and x3 = 1 respectively
{
    CMatrix AtA;
    CMatrix c;

    // x1 = 1
    CMatrix Acol23 = A.submatrix(0, 2, 1, 2);
    CMatrix Acol1  = A.submatrix(0, 2, 0, 0);
    AtA = (~Acol23) * Acol23;
    c   = (~Acol23) * -Acol1;
    
    CMatrix u23 = SolveLinearSystem( AtA, c);
    CMatrix u1(3, 1);
    u1[0] = 1; u1[1] = u23[0]; u1[2] = u23[1];
    u1 = u1 / u1.norm();
    
    // x2 = 1
    CMatrix Acol13(3, 2);
    for (int i = 0; i < 3; i++)
    {
        Acol13(i, 0) = A(i, 0);
        Acol13(i, 1) = A(i, 2);
    }
    CMatrix Acol2  = A.submatrix(0, 2, 1, 1);
    AtA = (~Acol13) * Acol13;
    c   = (~Acol13) * -Acol2;
    
    CMatrix u13 = SolveLinearSystem( AtA, c);
    CMatrix u2(3, 1);
    u2[0] = u13[0]; u2[1] = 1; u2[2] = u13[1];
    u2 = u2 / u2.norm();
    
    // x3 = 1
    CMatrix Acol12 = A.submatrix(0, 2, 0, 1);
    CMatrix Acol3  = A.submatrix(0, 2, 2, 2);
    AtA = (~Acol12) * Acol12;
    c   = (~Acol12) * -Acol3;
    
    CMatrix u12 = SolveLinearSystem( AtA, c);
    CMatrix u3(3, 1);
    u3[0] = u12[0]; u3[1] = u12[1]; u3[2] = 1;
    u3 = u3 / u3.norm();

    // Compute A * u1, A * u2, A * u3 
    // and return u that minimises this

    float minAu = (A * u1).norm();
    CMatrix minu = u1;
    if ((A * u2).norm() < minAu)
    { minAu = (A * u2).norm(); minu = u2; }
    if ((A * u3).norm() < minAu)
    { minAu = (A * u3).norm(); minu = u3; }
    
    return(minu);
}
