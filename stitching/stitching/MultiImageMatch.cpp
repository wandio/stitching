//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// MultiImageMatch.cpp

#include "MultiImageMatch.h"

#include <iostream>
#include <fstream>

CMultiImageMatch::CMultiImageMatch()
{
    matches = NULL;
    nMatches = 0;
    nImages = 0;
}

CMultiImageMatch::~CMultiImageMatch()
{
    
    for (int i = 0; i < nImages; i++)
    {
        delete[] matches[i];
        delete[] nMatches[i];
    }

    delete[] matches;
    delete[] nMatches;
}

CMultiImageMatch::CMultiImageMatch(CSIFTParameters SIFTParameters, CRANSACParameters RANSACParameters)
{
    matches = NULL;
    nMatches = 0;
    nImages = 0;
    SIFTMatch.Param = SIFTParameters;
    RANSAC.Param = RANSACParameters;
    nFeatures = 0;
}

void CMultiImageMatch::Create(string imageDir, vector<string> filenames, vector<CImage> &images, vector<SImageSize> &imageSize0, vector<int> &subsampleLevel, 
                              vector<CImage> &thumbs, vector<int> &thumbSubsampleLevel, int &SIFTImageMemory, CPanoramaStatus &status)
//
// Find geometrically consistent feature matches in images
//
{

    // Initialise matches data structure
    nImages = (int)filenames.size();

    nMatches = new int*[nImages];
    matches = new CMatch2D*[nImages];
    for (int i = 0; i < nImages; i++)
    { 
        nMatches[i] = new int[nImages];
        matches[i]  = new CMatch2D[nImages];
    }
    for (int i = 0; i < nImages; i++)
    {
        for (int j = 0; j < nImages; j++)
        {
            nMatches[i][j] = 0;
        }
    }

    // Find SIFT feature matches between the images
    SIFTMatch.Create(imageDir, filenames, images, imageSize0, subsampleLevel, thumbs, thumbSubsampleLevel, matches, nMatches, SIFTImageMemory, nFeatures, status);

    // Compute size of subsampled images
    vector<SImageSize> imageSize;
    imageSize.resize(nImages);
    for (int i = 0; i < nImages; i++)
    {
        float scale         = (float)pow(2.0f, subsampleLevel[i]);
        imageSize[i].nRows  = (int)(imageSize0[i].nRows / scale);
        imageSize[i].nCols  = (int)(imageSize0[i].nCols / scale);
        imageSize[i].nBands = imageSize0[i].nBands;
    }

    if (RANSAC.Param.autoAlpha)
    {
        float &alpha = RANSAC.Param.alpha;
        float alphaMax = RANSAC.Param.alphaMax;
        float alphaMin = RANSAC.Param.alphaMin;
        alpha = 0.15f * 0.1f * nFeatures / nImages;
        alpha = max(min(alpha, alphaMax), alphaMin);
        cerr << " alpha = " << RANSAC.Param.alpha << endl;
    }
	
    // Perform RANSAC
    RANSAC.Create(matches, nMatches, nImages, imageSize, status);

    // Find connected components
    ConnectedComponent.Create(nMatches, nImages);

}// end Create



int CMultiImageMatch::nMatchesTotal(void)
{
    int nMatchesTot = 0;
    for (int i = 0; i < nImages; i++)
    {
        for (int j = 0; j < nImages; j++)
        {
            nMatchesTot += nMatches[i][j];
        }
    }

    return(nMatchesTot);
};



void CMultiImageMatch::WriteToFile(void)
{
    ofstream os("nMatches.txt");
    for (int i = 0; i < nImages; i++)
    {
        for (int j = 0; j < nImages; j++)
        {
            os << nMatches[i][j] << " ";
        }// end for j
        os << endl;
    }// end for i
    os.close();

    os.open("matches.txt");
    for (int i = 0; i < nImages; i++)
    {
        for (int j = 0; j < nImages; j++)
        {
            CMatch2D& m2D = matches[i][j];                            
            for (int k = 0; k < nMatches[i][j]; k++)
            {
                os << m2D[0](0, k) << " " << m2D[0](1, k) << " " << m2D[1](0, k) << " " << m2D[1](1, k) << " ";
            }// end for k
            os << endl;
        }// end for j
    }// end for i
    os.close(); 
}





