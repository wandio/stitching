//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// PanoramaStatus.h

#pragma once

#ifdef WIN32
#include "PanoramaStatusWindows.h"
#endif

#ifndef WIN32
#include "PanoramaStatusNull.h"
#endif

