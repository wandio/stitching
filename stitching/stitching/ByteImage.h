//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// ByteImage.h

#pragma once

#include "Image.h"

#include <vector>

using namespace std;

class CByteImage
{
public:
    CByteImage(); 
    ~CByteImage();

    CByteImage(int rows, int cols, int bands);
    
    void Create(char* filename);
    void Create(int rows, int cols, int bands);
    void Create(const CImage &im);
    void Clear(void);
	
    CByteImage(const CByteImage &im);
    void operator=(const CByteImage &im);

    unsigned char &operator()(int row, int col, int band);
    const unsigned char operator()(int row, int col, int band) const;
    int rowStride(void) const;
    int colStride(void) const;

    void assign(const CByteImage &im1, int row, int col);
    void assign(const CImage &im1, int row, int col);
    
    void Copy(int rowDst, int colDst, const CImage &imSrc, int rowSrcMin, int colSrcMin, int rowSrcMax, int colSrcMax);
    void Copy(int rowDst, int colDst, const CByteImage &imSrc, int rowSrcMin, int colSrcMin, int rowSrcMax, int colSrcMax);
    
    void WriteJPEG(const char* filename, int quality);

    int nRows;
    int nCols;
    int nBands;
    unsigned char *data;
};

CByteImage ConvertToColour(const CByteImage &gim);
CByteImage operator*(const CByteImage &im1, unsigned char f);
void ByteImageToImage(CImage &im, const CByteImage &bim);

void DrawRect(CByteImage &im, int row0, int col0, int row1, int col1, vector<float> colour);
void DrawRect3(CByteImage &im, int row0, int col0, int row1, int col1, vector<float> colour);

void SubSample2Block(CByteImage &sim, const CByteImage &im);

