//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// ImageHelpers.cpp

#include "ImageHelpers.h"

vector<float> SumRectangle(CImage &im, int rowMin, int rowMax, int colMin, int colMax)
// compute sum of pixel values in specified rectangle
// returns a vector of length nBands containing the sum for each band
{
	vector<float> sum;
	sum.resize(im.nBands);
	for (int b = 0; b < im.nBands; b++)
		sum[b] = 0;

	float *imptr0 = &im(rowMin, colMin, 0);
	
	for (int row = rowMin; row <= rowMax; row++)
	{
		float *imptr = imptr0;
		
		for (int col = colMin; col <= colMax; col++)
		{
			for (int b = 0; b < im.nBands; b++)
			{
				sum[b] += *imptr;
				imptr++;
			}// end for b

		}// end for col
	
		imptr0 += im.rowStride();
	
	}// end for row

	return(sum);
}
