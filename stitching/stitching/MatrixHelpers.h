//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// MatrixHelpers.h

#pragma once

#include "Matrix.h"

CMatrix ComputeSimilarity(float u1, float v1, float x1, float y1, float u2, float v2, float x2, float y2, float minDist); 
// Compute Similarity transform from 2 image matches
CMatrix K(float f); // camera matrix
CMatrix Rx(float theta); // rotation matrix of theta about x axis
CMatrix Ry(float theta); // rotation matrix of theta about y axis
CMatrix Rz(float theta); // rotation matrix of theta about z axis
CMatrix RightNull3x3(const CMatrix &A); // find right nullspace of 3x3 matrix
