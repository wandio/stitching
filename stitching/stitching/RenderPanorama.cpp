//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// RenderPanorama.cpp

#include "RenderPanorama.h"
#include "MatrixHelpers.h"
#include "ImageHelpers.h"
#include <fstream>
#include "assert.h"
#include <string>
#include <algorithm>
#include <string.h>

string IntToString ( int i )
{    
    assert( i >= 0 );
    
    if ( i == 0 )  { return ("0"); }
    
    string str;
    
    while ( i != 0 )
    {
        int  itmp = i % 10;
        char ctmp = itmp + '0';
        str = ctmp + str;
        i /= 10;
    }
    
    return str;
}

int StringToInt ( const string str )
{
    int i = 0;
    int pow = 1;
    int size = (int)str.size();

    for ( int j = size - 1; j >= 0; j--)
    {
        char ctmp = str[j];
        assert ( ('0' <= ctmp) && (ctmp <= '9') ); 
        int itmp = ctmp - '0';
        i += itmp * pow;
        pow *= 10;
    }
     
    return i;    
}

CMatrix CRenderPanorama::T(int r)
// Coordinate scaling matrix for 2x2 block average
// x = T(r) x_r
// where x is the original image coordinate
// and x_r the coordinate in a r times (factor 2)
// subsampled image
{
    float scale = (float)pow(2.0f, r);
    CMatrix T(3, 3);
    T(0, 0) = scale; T(0, 2) = 0.5f * (scale - 1);
    T(1, 1) = scale; T(1, 2) = 0.5f * (scale - 1);
    T(2, 2) = 1;
    return(T);
}


vector< vector<float> > CRenderPanorama::ComputeImageGain(string imageDir, vector<string> filenames, vector<CImage> &images, vector<SImageSize> &imageSize,  
							  vector<CCamera> &Cameras, vector<int> &panoramaImageID, vector<int> &subsampleLevel, 
							  vector<CImage> &thumbs, vector<int> &thumbSubsampleLevel, CPanoramaStatus &status)
{
    cerr << "[ Compute Image Gain ]" << endl;

    // Resample images 
    int nImages = (int)images.size();
    int nPanorama = (int)panoramaImageID.size();
    int minImageDim = Param.minImageDimThumb;

    for (int im = 0; im < nPanorama; im++)
    {
        cerr << "  Image " << im << endl;
        status.Update(eGainCompensation, im, nPanorama - 1);	

        int imID = panoramaImageID[im];

        if (thumbs[imID].nRows > 0)
	{
            /* do nothing */
	}
        else
	{
            // downsample SIFT image
            if (images[imID].nRows > 0)
	    {
                // copy image
                thumbs[imID] = images[imID];
                thumbSubsampleLevel[imID] = subsampleLevel[imID];
	    }
            else	
	    {
                // reload image
                string imageFilenamei = imageDir + filenames[imID];
                thumbs[imID].Create(imageFilenamei.c_str());
                thumbSubsampleLevel[imID] = 0;
	    }

            // Subsample image by 2x2 block averages
            while( (thumbs[imID].nRows >= 2 * minImageDim) && (thumbs[imID].nCols >= 2 * minImageDim) )
	    {
                thumbs[imID] = SubSample2Block(thumbs[imID]);
                thumbSubsampleLevel[imID]++;
	    }

	}// end else 

    }// end for im

    const int nBands = 3;
    vector< vector< vector<float> > > Ibar; 
    Ibar.resize(nImages);
    for (int i = 0; i < nImages; i++)
    { Ibar[i].resize(nImages);
        for (int j = 0; j < nImages; j++)
            Ibar[i][j].resize(nBands);}
	

    CMatrix N(nImages, nImages); N.zeros();

    for (int im1 = 0; im1 < nPanorama; im1++)
    {
        int imID1 = panoramaImageID[im1];

        for (int im2 = 0; im2 < nPanorama; im2++)
	{
            int imID2 = panoramaImageID[im2];

            if (im1 == im2) continue;

            // Compute homography between im1 im2
	
            CMatrix T1 = Cameras[imID1].T;
            T1 = T(-thumbSubsampleLevel[imID1]) * T1;
            float f1   = Cameras[imID1].f;
            CMatrix R1 = Cameras[imID1].R;
			
            CMatrix T2 = Cameras[imID2].T;
            T2 = T(-thumbSubsampleLevel[imID2]) * T2;
            float f2   = Cameras[imID2].f;
            CMatrix R2 = Cameras[imID2].R;

            CMatrix T2inv(3, 3);
            T2inv.zeros();
            T2inv(0, 0) = 1.0f/T2(0, 0);
            T2inv(1, 1) = 1.0f/T2(1, 1);
            T2inv(0, 2) = -T2(0, 2)/T2(0, 0);
            T2inv(1, 2) = -T2(1, 2)/T2(1, 1);
            T2inv(2, 2) = 1;

            CMatrix Him1im2 = T1 * K(f1) * R1 * transpose(R2) * K(1/f2) * T2inv;

            // Project corners of im2 in im1
            int nRows1 = thumbs[imID1].nRows; int nCols1 = thumbs[imID1].nCols;
            int nRows2 = thumbs[imID2].nRows; int nCols2 = thumbs[imID2].nCols;

            CMatrix im2Corners(2, 4);
            im2Corners(0, 0) = 0; im2Corners(0, 1) = nRows2 - 1.0f; im2Corners(0, 2) = 0;             im2Corners(0, 3) = nRows2 - 1.0f;
            im2Corners(1, 0) = 0; im2Corners(1, 1) = 0;             im2Corners(1, 2) = nCols2 - 1.0f; im2Corners(1, 3) = nCols2 - 1.0f;

            CMatrix im2CornersTh = Him1im2 * homogeneous(im2Corners);

            // Check that this camera 2 is not behind camera 1
            bool positiveZ = true;
            for (int i = 0; i < 4; i++)
                if (im2CornersTh(2, i) < 0) positiveZ = false;

            if (positiveZ)
	    {
	
                CMatrix im2CornersT = unhomogeneous(im2CornersTh);

                int rowMin = nRows1 - 1;
                int rowMax = 0; 
                int colMin = nCols1 - 1;
                int colMax = 0;
		
                // find min/max projected row/col
                for (int i = 0; i < 4; i++)
		{
                    rowMin = (int)floor(min((float)rowMin, im2CornersT(0, i)) );
                    rowMax = (int)ceil (max((float)rowMax, im2CornersT(0, i)) );
                    colMin = (int)floor(min((float)colMin, im2CornersT(1, i)) );
                    colMax = (int)ceil (max((float)colMax, im2CornersT(1, i)) );
		}// end for i

                // truncate to im1 
                rowMin = max(0, min(rowMin, nRows1 - 1));
                rowMax = max(0, min(rowMax, nRows1 - 1));
                colMin = max(0, min(colMin, nCols1 - 1));
                colMax = max(0, min(colMax, nCols1 - 1));

                if ((rowMax > rowMin) && (colMax > colMin))
		{
                    //cerr << " Images " << imID1 << " and " << imID2 << " overlap" << endl;

                    vector<float> sum = SumRectangle(thumbs[imID1], rowMin, rowMax, colMin, colMax);

                    float nOverlap12 = (rowMax - rowMin + 1.0f) * (colMax - colMin + 1.0f);
                    N(imID1, imID2) = nOverlap12;
                    for (int b = 0; b < nBands; b++)
                        Ibar[imID1][imID2][b] = sum[b]/nOverlap12;
		}

	    }// end if positiveZ


	}// end for im2

    }// end for im1

    // Setup linear system

    float sigmag = Param.sigmaGain;
    float sigmaN = 255.0f/30.0f;

    CMatrix M(nPanorama, nPanorama);
    CMatrix v(nPanorama, 1);
	
    vector< vector<float> > gain;
    gain.resize(nImages);
    for (int i = 0; i < nImages; i++)
        gain[i].resize(nBands);

    for (int band = 0; band < nBands; band++)
    {
        M.zeros();
        v.zeros();
		
        for (int im1 = 0; im1 < nPanorama; im1++)
	{
            int imID1 = panoramaImageID[im1];

            for (int im2 = 0; im2 < nPanorama; im2++)
	    {
                int imID2 = panoramaImageID[im2];

                if (im1 == im2) continue;
			
                if ((N(imID1, imID2) > 0) && (N(imID2, imID1) > 0))
		{
					
                    M(im1, im2) += N(imID1, imID2) * Ibar[imID1][imID2][band] * Ibar[imID2][imID1][band] / (sigmaN * sigmaN);
                    M(im1, im1) -= N(imID1, imID2) * Ibar[imID1][imID2][band] * Ibar[imID1][imID2][band] / (sigmaN * sigmaN);

                    M(im1, im1) -= N(imID1, imID2)/(sigmag * sigmag);	
                    v[im1]      -= N(imID1, imID2)/(sigmag * sigmag);

		}// end if 

	    }// end for im2

	}// end for im1

        CMatrix g = SolveLinearSystem(M, v);
        float gmean = sum(g) / nPanorama;
	
        cerr << endl;
        cerr << " band = " << band << endl;

        cerr << endl;
        cerr << " g = " << endl;
        cerr << g << endl;
        cerr << endl;

        cerr << " g mean = " << endl;
        cerr << gmean << endl;
        cerr << endl;			

        for (int i = 0; i < nPanorama; i++)
	{
            int imID = panoramaImageID[i];
            gain[imID][band] = g[i] * Param.meanGain / gmean;
	}

    }// end for band

    return(gain);
}

bool CRenderPanorama::OverlapTheta(float theta, float &thetaMinOverlap, float &thetaMaxOverlap, 
				   const vector<CImageRenderInfo> &ImageRenderInfo)
{
    int nImages = (int)ImageRenderInfo.size();

    bool foundOverlap = false;
    thetaMinOverlap = (float)M_PI;
    thetaMaxOverlap = -(float)M_PI;

    for (int i = 0; i < nImages; i++)
    {
        const vector<CImagePieceRenderInfo> &piece = ImageRenderInfo[i].piece;
        int nPieces = (int)piece.size();
		
        for (int p = 0; p < nPieces; p++)
        {
            if ((theta <= piece[p].thetaMax) && 
                (theta >= piece[p].thetaMin))
	    {
                foundOverlap = true;
                thetaMinOverlap = min(thetaMinOverlap, piece[p].thetaMin);
                thetaMaxOverlap = max(thetaMaxOverlap, piece[p].thetaMax);
	    }// end if
	}// end for p
    }// end for i

    return(foundOverlap);
}


void CRenderPanorama::Render(CByteImage &panoImage, CPanorama &pano)
{
    vector<CCamera> &Cameras         = pano.Cameras;
    vector<SImageSize> &imageSize0   = pano.imageSize0;
    vector<CImage> &images           = pano.images;	
    vector<int> &subsampleLevel      = pano.subsampleLevel;
    vector<CImage> &thumbs           = pano.thumbs;
    vector<int> &thumbSubsampleLevel = pano.thumbSubsampleLevel;
    vector<int> &panoramaImageID     = pano.panoramaImageID;
    vector<string> &filenames        = pano.filenames;
    string         &imageDir         = pano.imageDir;
    CPanoramaStatus  &status         = pano.status;


    // Perform gain compensation if required
    vector< vector<float> > gain;
    if (Param.gainCompensation)
        gain = ComputeImageGain(imageDir, filenames, images, imageSize0, Cameras, panoramaImageID, subsampleLevel, thumbs, thumbSubsampleLevel, status);


    // Overall render range of panorama
    float thetaMin;
    float thetaMax;
    float phiMin; 
    float phiMax;
    int nCols;
    int nRows;
    int nBands = 3;
	
    //
    // Compute render range of each image and its constituent pieces in theta and phi
    //

    vector<CImageRenderInfo> ImageRenderInfo0;
    int nImages = (int)panoramaImageID.size();
    ImageRenderInfo0.resize(nImages);
            
    for (int i = 0; i < nImages; i++)
    {
        int imageID = pano.panoramaImageID[i];
        ComputeRenderRangeThetaPhi(ImageRenderInfo0[i], Cameras[imageID], imageSize0[imageID]);
    }

    //
    // Auto centre
    //

    // (Centre the panorama so that it does not split across theta)

    float theta = -(float)M_PI;
    float thetaMinOverlap, thetaMaxOverlap;
    float unused;
    bool split = OverlapTheta(theta, thetaMinOverlap, thetaMaxOverlap, ImageRenderInfo0);

    if (split)
    {
        cerr << " pano is split" << endl;	

        theta = -(float)M_PI;
        thetaMaxOverlap = theta;
        bool foundOverlap = true;
		
        while (foundOverlap)
	{
            float thetaMaxOverlap0 = thetaMaxOverlap;		
            foundOverlap = OverlapTheta(theta, unused, thetaMaxOverlap, ImageRenderInfo0);
            if (thetaMaxOverlap == thetaMaxOverlap0) foundOverlap = false;
            theta = thetaMaxOverlap;
            //cerr << "thetaMaxOverlap = " << thetaMaxOverlap << endl;
	}
	
        theta = (float)M_PI;
        thetaMinOverlap = theta;
        foundOverlap = true;

        while (foundOverlap)
	{
            float thetaMinOverlap0 = thetaMinOverlap;
            foundOverlap = OverlapTheta(theta, thetaMinOverlap, unused, ImageRenderInfo0);
            if (thetaMinOverlap == thetaMinOverlap0) foundOverlap = false;
            theta = thetaMinOverlap;
            //cerr << "thetaMinOverlap = " << thetaMinOverlap << endl;
	}

        if (thetaMinOverlap > thetaMaxOverlap)
	{
            float thetaMiddle = 0.5f * thetaMaxOverlap + (float)M_PI + 0.5f * thetaMinOverlap;
			
            //cerr << "theta middle = " << thetaMiddle << endl;
 		
            CMatrix R = Rx(-thetaMiddle);
            vector<int>::iterator it = panoramaImageID.begin();
    
            while(it != panoramaImageID.end())
	    {
                int imageID = *it;
                Cameras[imageID].R = Cameras[imageID].R * R;
                it++;
	    }
		
            // Recompute render ranges
            ImageRenderInfo0.clear(); ImageRenderInfo0.resize(nImages);
            for (int i = 0; i < nImages; i++)
	    {
                int imageID = panoramaImageID[i];
                ComputeRenderRangeThetaPhi(ImageRenderInfo0[i], Cameras[imageID], imageSize0[imageID]);
	    }
		
	}// end if thetaMin > thetaMax
    }

    //
    // Auto Crop
    //
	
    if (Param.autoCrop == eInterior || Param.autoCrop == eExterior)
    {
        thetaMin = (float)M_PI;
        thetaMax = (float)-M_PI;
        phiMin   = (float)M_PI/2;
        phiMax   = (float)-M_PI/2;

        for (int i = 0; i < nImages; i++)
        {
            vector<CImagePieceRenderInfo> &piece = ImageRenderInfo0[i].piece;
            int nPieces = (int)piece.size();

            for (int p = 0; p < nPieces; p++)
            {
                thetaMin = min(piece[p].thetaMin, thetaMin);
                thetaMax = max(piece[p].thetaMax, thetaMax);
                phiMin   = min(piece[p].phiMin, phiMin);
                phiMax   = max(piece[p].phiMax, phiMax);
            }// end for p
        }// end for i
    }// end autoCrop
    else // if !autoCrop)
    {
        thetaMin = Param.thetaMin;
        thetaMax = Param.thetaMax;
        phiMin   = Param.phiMin;
        phiMax   = Param.phiMax;
    }
    
    //
    // Set output size
    //

    if (Param.sizeSelector == eWidth)
    {
        nCols = Param.width;
        nRows = (int)((nCols - 1) * (phiMax - phiMin) / (thetaMax - thetaMin)) + 1; 
        // note that (nCols - 1) / (nRows - 1) = (thetaMax - thetaMin) / (phiMax - phiMin)
        dthetady = (thetaMax - thetaMin) / (nCols - 1);
        dphidx   = (phiMin - phiMax)     / (nRows - 1);
        // note dthetady = -dphidx in this case
    }
    else if (Param.sizeSelector == eHeight)
    {
        nRows = Param.height;
        nCols = (int)((nRows - 1) * (thetaMax - thetaMin) / (phiMax - phiMin)) + 1;
        // note that (nCols - 1) / (nRows - 1) = (thetaMax - thetaMin) / (phiMax - phiMin)
        dthetady = (thetaMax - thetaMin) / (nCols - 1);
        dphidx   = (phiMin - phiMax)     / (nRows - 1);
        // note dthetady = -dphidx in this case
    }
    else if (Param.sizeSelector == eRelativeSize)
    {
        //int imageID1 = pano.panoramaImageID[0];		
        int imageID1 = *min_element(panoramaImageID.begin(), panoramaImageID.end());
        int imageNum = 0;
        for (int i = 0; i < (int)panoramaImageID.size(); i++)
	{if (imageID1 == panoramaImageID[i])
                imageNum = i;}

        float d1 = imageSize0[imageNum].nRows * Param.relativeSize;
        float d2 = imageSize0[imageNum].nCols * Param.relativeSize;
	
        float dtheta1 = ImageRenderInfo0[imageNum].dtheta1;
        float dtheta2 = ImageRenderInfo0[imageNum].dtheta2;
        float dphi1   = ImageRenderInfo0[imageNum].dphi1;
        float dphi2   = ImageRenderInfo0[imageNum].dphi2;
		
        float dy1 = (thetaMax - thetaMin) * d1 / (float)sqrt(dtheta1 * dtheta1 + dphi1 * dphi1);
        float dy2 = (thetaMax - thetaMin) * d2 / (float)sqrt(dtheta2 * dtheta2 + dphi2 * dphi2);

        nCols = (int)max(dy1, dy2);
        nRows = (int)((nCols - 1) * (phiMax - phiMin) / (thetaMax - thetaMin)) + 1; 
        // note that (nCols - 1) / (nRows - 1) = (thetaMax - thetaMin) / (phiMax - phiMin)
        dthetady = (thetaMax - thetaMin) / (nCols - 1);
        dphidx   = (phiMin - phiMax)     / (nRows - 1);
        // note dthetady = -dphidx in this case
    }

    // Set angular range and output image size structs
    sAngularRange angularRange;
    angularRange.thetaMin = thetaMin;
    angularRange.thetaMax = thetaMax;
    angularRange.phiMin   = phiMin;
    angularRange.phiMax   = phiMax;
    SImageSize panoImageSize;
    panoImageSize.nBands = nBands;
    panoImageSize.nCols  = nCols;
    panoImageSize.nRows  = nRows;

    //
    // Compute render range of each image and its constituent pieces in the output image
    //

    for (int i = 0; i < nImages; i++)
    {
        int imageID = panoramaImageID[i];
        ComputeRenderRangeOutputImage(ImageRenderInfo0[i], angularRange, panoImageSize, dthetady, dphidx, images[imageID], subsampleLevel[imageID]);
    }
	
    // Set size parameters 

    int imageID1 = *min_element(panoramaImageID.begin(), panoramaImageID.end());
    int imageNum = 0;
    for (int i = 0; i < (int)panoramaImageID.size(); i++)
    {if (imageID1 == panoramaImageID[i])
            imageNum = i;}
    float d1 = ImageRenderInfo0[imageNum].d1;
    float d10 = (float)imageSize0[imageID1].nRows;
    float d2 = ImageRenderInfo0[imageNum].d2;
    float d20 = (float)imageSize0[imageID1].nCols;
    float relSize = min(d1/d10, d2/d20);

    Param.relativeSize = relSize;
    Param.width  = nCols;	
    Param.height = nRows;
    Param.thetaMax = thetaMax;
    Param.thetaMin = thetaMin;
    Param.phiMax = phiMax;
    Param.phiMin = phiMin;

    //
    // Render (in blocks if neccessary)
    //

    // Compute block size based on available memory

    int blockSize;
    int blockSize2 = (Param.maxMemory - nRows * nCols * nBands) / (13 * sizeof(float)); 
    if (blockSize2 > 0)
    {
        blockSize = (int)sqrt((float)blockSize2);
        int log2BlockSize = (int)floor(log((float)blockSize)/log(2.0f)); 
        blockSize = (int)pow(2.0f, (int)floor((float)log2BlockSize));
        blockSize = min(max(blockSize, Param.minBlockSize), Param.maxBlockSize);
    }	
    else
    {
        blockSize = Param.minBlockSize;
    }
    //cerr << "Max memory = " << Param.maxMemory << endl;
    cerr << " Render Block Size = " << blockSize << endl;

    vector<CImageRenderInfo> ImageRenderInfo;
    ImageRenderInfo.resize(nImages);
    int xmin, xmax, ymin, ymax;
    EBlendingMethod blendingMethod = Param.blendingMethod;


    //
    // Render in blocks
    //

    panoImage.Create(nRows, nCols, nBands);
    CByteImage panoAlphaImage;
    panoAlphaImage.Create(nRows, nCols, 1);
   
    int nRowBlocks = nRows/blockSize;
    if ((nRows % blockSize) != 0)
        nRowBlocks++;
    int nColBlocks = nCols/blockSize;
    if ((nCols % blockSize) != 0)
        nColBlocks++;
       
    int nBlocks = nRowBlocks * nColBlocks;
		
    int BlockCounter = 0;
    int maxCount = nRowBlocks * nColBlocks;
    int count = 0;
    pano.status.Update2(count, maxCount);

    int blockDilate = 0;
    int kernelL = (int)floor(3 * Param.sigmaBlend + 0.5);
    if (blendingMethod == eMultiBand && Param.blockDilate)
        blockDilate = kernelL * Param.nBlendingBands;

    for (int r = 0; r < nRowBlocks; r++)
    {	
        xmin = r * blockSize;
        xmax = min(nRows - 1, xmin + blockSize - 1);

        for (int c = 0; c < nColBlocks; c++)
	{
            BlockCounter++;
            cerr << "[ Block " << BlockCounter << " of " << nBlocks << " ]" << endl;
            count++;
            pano.status.Update2(count, maxCount);

            ymin = c * blockSize;
            ymax = min(nCols - 1, ymin + blockSize - 1);

            for (int i = 0; i < nImages; i++)
	    {
                ImageRenderInfo[i] = TruncateRenderRange(ImageRenderInfo0[i], xmin, xmax, ymin, ymax, dthetady, dphidx, blockDilate);
	    }

            CImage panoBlock;
            CByteImage panoAlphaBlock;
            int nRowsBlock = xmax - xmin + 1 + blockDilate * 2; 
            int nColsBlock = ymax - ymin + 1 + blockDilate * 2;
	
            if (blendingMethod == eNoBlend)				
                RenderNoBlend(panoBlock, panoAlphaBlock, pano, ImageRenderInfo, nRowsBlock, nColsBlock, gain);
            else if (blendingMethod == eLinear)
                RenderLinearBlend(panoBlock, panoAlphaBlock, pano, ImageRenderInfo, nRowsBlock, nColsBlock, gain);
            else // if (blendingMethod == eMultiBand
                RenderMultiBandBlend(panoBlock, panoAlphaBlock, pano, ImageRenderInfo, nRowsBlock, nColsBlock, gain);

            panoBlock.TruncateIntensity();
            panoImage.Copy(xmin, ymin, panoBlock, blockDilate, blockDilate, panoBlock.nRows - blockDilate - 1, panoBlock.nCols - blockDilate - 1);
            panoAlphaImage.Copy(xmin, ymin, panoAlphaBlock, blockDilate, blockDilate, panoBlock.nRows - blockDilate - 1, panoBlock.nCols - blockDilate - 1);
	}// end for c
    }// end for r  	
}

CImageRenderInfo CRenderPanorama::TruncateRenderRange(CImageRenderInfo &ImageRenderInfo0, int xmin, int xmax, int ymin, int ymax, float dthetady, float dphidx, int blockDilate)
{
    CImageRenderInfo ImageRenderInfo = ImageRenderInfo0;

    ImageRenderInfo.isValid = false; 
    vector<CImagePieceRenderInfo> &piece = ImageRenderInfo.piece;
    int nPieces = (int)piece.size();

    // Apply block dilation to bounds
    xmin -= blockDilate;
    xmax += blockDilate;
    ymin -= blockDilate;
    ymax += blockDilate;

    for (int p = 0; p < nPieces; p++)
    {
        piece[p].isValid = false;

        // Apply block dilation to render piece
        piece[p].xmin -= blockDilate;
        piece[p].xmax += blockDilate;
        piece[p].ymin -= blockDilate;
        piece[p].ymax += blockDilate;

        piece[p].thetaMin -= dthetady * blockDilate;
        piece[p].thetaMax += dthetady * blockDilate;
        piece[p].phiMin   += dphidx   * blockDilate;
        piece[p].phiMax   -= dphidx   * blockDilate;

        // Compute truncated range
        int xmint = max(xmin, piece[p].xmin);
        int xmaxt = min(xmax, piece[p].xmax);
        int ymint = max(ymin, piece[p].ymin);
        int ymaxt = min(ymax, piece[p].ymax);

        if ((xmaxt > xmint) & (ymaxt > ymint))
	{
            piece[p].isValid = true;
            ImageRenderInfo.isValid = true;
            // Adjust x,y and theta, phi
            int dxmin = xmint - piece[p].xmin;
            int dxmax = xmaxt - piece[p].xmax;
            int dymin = ymint - piece[p].ymin;
            int dymax = ymaxt - piece[p].ymax;

            piece[p].xmin      = xmint - xmin;
            piece[p].xmax      = xmaxt - xmin;
            piece[p].ymin      = ymint - ymin;
            piece[p].ymax      = ymaxt - ymin;

            piece[p].thetaMin += dthetady * dymin;
            piece[p].thetaMax += dthetady * dymax;
            piece[p].phiMin   += dphidx   * dxmax;
            piece[p].phiMax   += dphidx   * dxmin;
	}// end if (xmaxt > xmint) ...

    }// end for p				

    return (ImageRenderInfo);
}

void CRenderPanorama::ComputeRenderRangeOutputImage(CImageRenderInfo &ImageRenderInfo, sAngularRange angularRange, SImageSize panoImageSize, float dthetady, float dphidx, CImage &image, int subsampleLevel)
// Given angular ranges for each image and piece, and the output angular range and size
// compute output image ranges for each image and piece
{
    float thetaMin = angularRange.thetaMin;
    float thetaMax = angularRange.thetaMax;
    float phiMin   = angularRange.phiMin;
    float phiMax   = angularRange.phiMax;
    int   nRows    = panoImageSize.nRows;
    int   nCols    = panoImageSize.nCols;

    float &d1     = ImageRenderInfo.d1;
    float &d2     = ImageRenderInfo.d2;
    float dtheta1 = ImageRenderInfo.dtheta1;
    float dtheta2 = ImageRenderInfo.dtheta2;
    float dphi1   = ImageRenderInfo.dphi1;
    float dphi2   = ImageRenderInfo.dphi2;

    d1 = (float)sqrt((dtheta1 * dtheta1)/(dthetady * dthetady) + (dphi1 * dphi1)/(dphidx * dphidx));
    d2 = (float)sqrt((dtheta2 * dtheta2)/(dthetady * dthetady) + (dphi2 * dphi2)/(dphidx * dphidx));

    vector<CImagePieceRenderInfo> &piece = ImageRenderInfo.piece;
    int nPieces = (int)piece.size();

    for (int p = 0; p < nPieces; p++)
    {
        // set render range
        float yminf = ((piece[p].thetaMin - thetaMin) / (thetaMax - thetaMin)) * ((float)nCols - 1); 
        float ymaxf = ((piece[p].thetaMax - thetaMin) / (thetaMax - thetaMin)) * ((float)nCols - 1);
        float xminf = ((phiMax - piece[p].phiMax) / (phiMax - phiMin)) * ((float)nRows - 1);
        float xmaxf = ((phiMax - piece[p].phiMin) / (phiMax - phiMin)) * ((float)nRows - 1);
        // truncate render range to integer value
        int ymini = (int)ceil(yminf); yminf -= ymini;
        // NOTE: truncation is no longer needed. Was between above 2 expressions.
        //ymini = max(min(nCols - 1, ymini), 0);
        int ymaxi = (int)floor(ymaxf); ymaxf -= ymaxi;
        //ymaxi = max(min(nCols - 1, ymaxi), 0); 
        int xmini = (int)ceil(xminf); xminf -= xmini;
        //xmini = max(min(nRows - 1, xmini), 0); 
        int xmaxi = (int)floor(xmaxf); xmaxf -= xmaxi;
        //xmaxi = max(min(nRows - 1, xmaxi), 0); 
        piece[p].xmin = xmini;
        piece[p].xmax = xmaxi;
        piece[p].ymin = ymini;
        piece[p].ymax = ymaxi;
        // make piece.thetaMin correspond to ymin etc.
        piece[p].thetaMin -= yminf * dthetady; 
        piece[p].thetaMax -= ymaxf * dthetady; 
        piece[p].phiMax   -= xminf * dphidx;   
        piece[p].phiMin   -= xmaxf * dphidx;
    }
}

void CRenderPanorama::RenderMultiBandBlend(CImage &panoImage, CByteImage &panoAlpha, CPanorama &pano, vector<CImageRenderInfo> &ImageRenderInfo, int nRows, int nCols, const vector< vector<float> > &gain)
{  
    vector<CCamera> &Cameras       = pano.Cameras;
    vector<CImage>  &images        = pano.images;
    string imageDir                = pano.imageDir;
    vector<string> &filenames      = pano.filenames;
    vector<int> &subsampleLevel    = pano.subsampleLevel;
    vector<SImageSize> &imageSize0 = pano.imageSize0;
    vector<int> &panoramaImageID   = pano.panoramaImageID;

    // Sample image pieces and weights
    cerr << "  Sample images  " << endl;
    panoAlpha.Create(nRows, nCols, 1);
    CImage weightMax(nRows, nCols, 1);
    int nImages = (int)panoramaImageID.size();
	
    for (int i = 0; i < nImages; i++)
    {	
        pano.status.Update(eRender, i, nImages - 1);
		
        if (ImageRenderInfo[i].isValid)
	{
            int imageID = panoramaImageID[i];
		 
            CImage renderImage;
            CCamera renderCamera = Cameras[imageID];

            ResampleImageForRendering(renderImage, renderCamera, imageID, images, subsampleLevel, ImageRenderInfo[i], imageDir, filenames);
        
            // Apply gain
            if (Param.gainCompensation)
                renderImage * gain[imageID];

            CMatrix R = renderCamera.R;
            float   f = renderCamera.f;
            CMatrix T = renderCamera.T;
            CMatrix P = T * K(f) * R;

            cerr << "   Image " << imageID << endl;
 
            int nPieces = (int)ImageRenderInfo[i].piece.size();
            for (int p = 0; p < nPieces; p++)
	    {
                CImagePieceRenderInfo &piecei = ImageRenderInfo[i].piece[p];
                if (piecei.isValid)
		{	
                    //RenderPiece(weightMax, renderImage, piecei, P);
                    RenderPieceSampleBilinear(weightMax, panoAlpha, renderImage, piecei, P);
                    //					RenderPieceSampleCubicBSpline(weightMax, renderImage, piecei, P);
                    ComputeNonZeroRange(piecei.weightLinear, piecei.IrowBegin, piecei.IrowEnd, piecei.IcolBegin, piecei.IcolEnd);				
				
		}// end if piecei.isValid
	    }// end for p
	}// end if ImageRenderInfo[i].isValid
    }// end for i

    // Find max weights
    for (int i = 0; i < nImages; i++)
    {
        int imageID = panoramaImageID[i];        
        int nPieces = (int)ImageRenderInfo[i].piece.size();
        for (int p = 0; p < nPieces; p++)
        {
            CImagePieceRenderInfo &piecei = ImageRenderInfo[i].piece[p];
            if (piecei.isValid)       
                ComputeWeightIsMax(weightMax, piecei);    				
        }// end for p
    }// end for i
    weightMax.Clear();

    cerr << "  Multi-band blending  " << endl;
    
    panoImage.Create(nRows, nCols, 3);
    CImage num;
    CImage den;
    
    int nBlendingBands = Param.nBlendingBands;
    float sigma        = Param.sigmaBlend;
       
    int maxCount = nBlendingBands * nImages - 1;
    int count = 0;

    int b = 0;
    while (b < nBlendingBands - 1)
    {
        cerr << "   Band " << b + 1 << endl;

        num.Create(nRows, nCols, 3);
        den.Create(nRows, nCols, 1);
        
        for (int i = 0; i < nImages; i++)
        {
            pano.status.Update(eBlend, count, maxCount);
            count++;

            if (ImageRenderInfo[i].isValid)
	    {
                int imageID = panoramaImageID[i];        
                cerr << "    Image " << imageID << endl;
                int nPieces = (int)ImageRenderInfo[i].piece.size();
			
                for (int p = 0; p < nPieces; p++)
		{
                    CImagePieceRenderInfo &piece = ImageRenderInfo[i].piece[p];
                    if (piece.isValid)
		    {
                        CImage blur;
								
                        if (Param.useBoundedConvolve)
                            blur = ConvolveGaussianBounded(piece.image, sigma, piece.IrowBegin, piece.IrowEnd, piece.IcolBegin, piece.IcolEnd);
                        else
                            blur = ConvolveGaussian(piece.image, sigma);
						
                        CImage band = piece.image - blur;
						
                        piece.weight = ConvolveGaussian(piece.weight, sigma);
						
                        BlendPiece(num, den, piece, band, piece.weight);
                        piece.image = blur;

		    }// end if piece.isValid
		}// end for p
	    }// end if ImageRenderInfo[i].isValid

            
        }// end for i

        AddBand(panoImage, num, den);

        b++;
    }// end while b

    num.Create(nRows, nCols, 3);
    den.Create(nRows, nCols, 1);

    cerr << "   Band " << b + 1 << endl;;

    for (int i = 0; i < nImages; i++)
    {
        count++;
        pano.status.Update(eBlend, count, maxCount);
		
        if (ImageRenderInfo[i].isValid)
	{
            int imageID = panoramaImageID[i];        
            cerr << "    Image " << imageID << endl;
            int nPieces = (int)ImageRenderInfo[i].piece.size();
            for (int p = 0; p < nPieces; p++)
	    {
                CImagePieceRenderInfo &piece = ImageRenderInfo[i].piece[p];
				
                if (piece.isValid)
                    BlendPiece(num, den, piece, piece.image, piece.weightLinear);

	    }// end for p
				
	}// end if ImageRenderInfo[i].isValid

    }// end for i

    AddBand(panoImage, num, den);

}

void CRenderPanorama::RenderLinearBlend(CImage &panoImage, CByteImage &panoAlpha, CPanorama &pano, vector<CImageRenderInfo> &ImageRenderInfo, int nRows, int nCols, const vector< vector<float> > &gain)
{
	
    vector<CCamera> &Cameras       = pano.Cameras;
    vector<CImage>  &images        = pano.images;
    string imageDir                = pano.imageDir;
    vector<string> &filenames      = pano.filenames;
    vector<int> &subsampleLevel    = pano.subsampleLevel;
    vector<SImageSize> &imageSize0 = pano.imageSize0;
    vector<int> &panoramaImageID   = pano.panoramaImageID;

    panoImage.Create(nRows, nCols, 3);
    panoAlpha.Create(nRows, nCols, 1);
    CImage &num = panoImage;

    CImage den(nRows, nCols, 1);

    int nImages = (int)panoramaImageID.size();
    for (int i = 0; i < nImages; i++)
    {
        pano.status.Update(eRender, i, nImages - 1);

        if (ImageRenderInfo[i].isValid)
	{
            int imageID = panoramaImageID[i];
     
            CImage renderImage;
            CCamera renderCamera = Cameras[imageID];

            ResampleImageForRendering(renderImage, renderCamera, imageID, images, subsampleLevel, ImageRenderInfo[i], imageDir, filenames);
		   
            // Apply gain
            if (Param.gainCompensation)
                renderImage * gain[imageID];

            CMatrix R = renderCamera.R;
            float   f = renderCamera.f;
            CMatrix T = renderCamera.T;
            CMatrix P = T * K(f) * R;

            cerr << "  Image " << imageID << endl;
 
            int nPieces = (int)ImageRenderInfo[i].piece.size();
            for (int p = 0; p < nPieces; p++)
	    {			
                CImagePieceRenderInfo &piecei = ImageRenderInfo[i].piece[p];
                if (piecei.isValid)
                    RenderPieceLinearBlend(num, den, panoAlpha, renderImage, piecei, P);           
	    }		
	}// end if ImageRenderInfo.isValid
    }// end for i
    
    //
    // Linearly blend image and truncate to 0-255
    //

    pano.status.Update(eBlend, 1, 2);

    float *dptr = den.data;
    float *nptr = num.data;

    for (int i = 0; i < nRows; i++)
    {
        for (int j = 0; j < nCols; j++)
        {
            for (int b = 0; b < 3; b++)
            {
                if (*dptr > 0)
                {
                    *nptr /= *dptr;
                    //*nptr = min(max(0.0f, *nptr), 255.0f);
                }
                nptr++;
            } 
            dptr++;
        }
    }

    pano.status.Update(eBlend, 2, 2);

}

void CRenderPanorama::RenderNoBlend(CImage &panoImage, CByteImage &panoAlpha, CPanorama &pano, vector<CImageRenderInfo> &ImageRenderInfo, int nRows, int nCols, const vector< vector<float> > &gain)
{
	
    vector<CCamera> &Cameras       = pano.Cameras;
    vector<CImage>  &images        = pano.images;
    string imageDir                = pano.imageDir;
    vector<string> &filenames      = pano.filenames;
    vector<int> &subsampleLevel    = pano.subsampleLevel;
    vector<SImageSize> &imageSize0 = pano.imageSize0;
    vector<int> &panoramaImageID   = pano.panoramaImageID;

    panoImage.Create(nRows, nCols, 3);
    panoAlpha.Create(nRows, nCols, 1);

    int nImages = (int)panoramaImageID.size();
    for (int i = 0; i < nImages; i++)
    {
        pano.status.Update(eRender, i, nImages - 1);
		
        if (ImageRenderInfo[i].isValid)
	{
            int imageID = panoramaImageID[i];
     
            CImage renderImage;
            CCamera renderCamera = Cameras[imageID];

            ResampleImageForRendering(renderImage, renderCamera, imageID, images, subsampleLevel, ImageRenderInfo[i], imageDir, filenames);
		   
            // Apply gain
            if (Param.gainCompensation)
                renderImage * gain[imageID];

            CMatrix R = renderCamera.R;
            float   f = renderCamera.f;
            CMatrix T = renderCamera.T;
            CMatrix P = T * K(f) * R;

            cerr << "  Image " << imageID << endl;
 
            int nPieces = (int)ImageRenderInfo[i].piece.size();
            for (int p = 0; p < nPieces; p++)
	    {			
                CImagePieceRenderInfo &piecei = ImageRenderInfo[i].piece[p];
                if (piecei.isValid)
                    RenderPieceNoBlend(panoImage, panoAlpha, renderImage, piecei, P);           
	    }		
	}// end if ImageRenderInfo.isValid
    }// end for i
}


void CRenderPanorama::ResampleImageForRendering(CImage &renderImage, CCamera &renderCamera, int imageID, vector<CImage> &images, vector<int> &subsampleLevel, CImageRenderInfo &ImageRenderInfo, string imageDir, vector<string> &filenames)
{
    //
    // Resample image to the correct size for rendering
    //

    bool reload = false; // test if we need to reload the image

    float d1 = ImageRenderInfo.d1;
    float d2 = ImageRenderInfo.d2;
    float downsample;

    CImage &image = images[imageID];
    if (image.nRows > 0) 
        // image is valid
    {
        float d1p = (float)image.nRows;
        float d2p = (float)image.nCols;
        downsample = max( d1p/d1, d2p/d2 );

        if (downsample >= 1)
	{
            reload = false;
	}
        else // if downsample < 1
	{
            if (subsampleLevel[imageID] > 0)
	    {
                reload = true;
	    }
            else // subsampleLevel[imageID] == 0
	    {
                reload = false;
	    }
	}
    }
    else // if image not valid
    {
        reload = true;
    }

    if (!reload)
    {
        renderImage = images[imageID];
        renderCamera.T = T(-subsampleLevel[imageID]) * renderCamera.T;

        if (downsample > 1)
	{
            int dlevel = (int)floor(log(downsample)/log(2.0f));
            dlevel = max(0, dlevel);

            int dlevel0 = dlevel;
            while (dlevel0 > 0)
	    {
                renderImage = SubSample2Block(renderImage);
                dlevel0--;
	    }
            
            float sigma = downsample / (float)(pow(2.0f, dlevel));
     
            renderImage = ConvolveGaussian(renderImage, Param.downsampleSmoothing * sigma);        

            renderCamera.T = T(-dlevel) * renderCamera.T;
	}
        else if (downsample <= 1)
	{
            // upsample using bilinear interpolation
            // (if using nearest neighbour resampling)
	}
    }
    else // if (reload)
    {
        // Load image
        //cerr << "Loading..." << endl;
        string imageFilenamei = imageDir + filenames[imageID];
        renderImage.Create(imageFilenamei.c_str());

        float d1p = (float)renderImage.nRows;
        float d2p = (float)renderImage.nCols;
        downsample = max( d1p/d1, d2p/d2 );

        if (downsample > 1)
	{
            int level = (int)floor(log(downsample)/log(2.0f));
            level = max(0, level);
            
            int level0 = level;
            while (level0 > 0)
	    {
                renderImage = SubSample2Block(renderImage);
                level0--;
	    }
            
            float sigma = downsample / (float)(pow(2.0f, level));

            renderImage = ConvolveGaussian(renderImage, Param.downsampleSmoothing * sigma);        
             
            renderCamera.T = T(-level) * renderCamera.T;              
	}
        else // if downsample <= 1
	{
            // upsample using bilinear interpolation to stop repeat sampling with nearest neighbour interp
	}

    }// end if (reload)
		     
}

void CRenderPanorama::ComputeRenderRangeThetaPhi(CImageRenderInfo &ImageRenderInfo, CCamera &camera, SImageSize imageSize0)
// Compute the render range of this image and its constituent pieces in theta and phi
{
    vector<CImagePieceRenderInfo> &piece = ImageRenderInfo.piece;

    // image dimensions
    int nColsImage = imageSize0.nCols;
    int nRowsImage = imageSize0.nRows;
    
    CMatrix R = camera.R;
    float   f = camera.f;
    CMatrix T = camera.T;

    // Compute image corners in theta, phi
    CMatrix Tinv(3, 3);
    Tinv(0, 0) = 1/T(0, 0);
    Tinv(1, 1) = Tinv(0, 0);
    Tinv(0, 2) = -T(0, 2)/T(0, 0);
    Tinv(1, 2) = -T(1, 2)/T(0, 0);
    Tinv(2, 2) = 1;
    CMatrix Kinv = K(1/f);
    CMatrix Pinv = (~R) * Kinv * Tinv;
    
    CMatrix imCorner[8];
    imCorner[0].Create(2, 1); imCorner[0][0] = 0.0f;                  imCorner[0][1] = 0.0f;
    imCorner[1].Create(2, 1); imCorner[1][0] = nRowsImage - 1.0f;     imCorner[1][1] = 0.0f; 
    imCorner[2].Create(2, 1); imCorner[2][0] = 0.0f;                  imCorner[2][1] = nColsImage - 1.0f; 
    imCorner[3].Create(2, 1); imCorner[3][0] = nRowsImage - 1.0f;     imCorner[3][1] = nColsImage - 1.0f; 
    
    //
    // Test whether this image is split across theta/phi
    //

    bool quadrant[8];
    for (int i = 0; i < 8; i++) quadrant[i] = false;
    
    for (int i = 0; i < 4; i++)
    {
        CMatrix X = unit(Pinv * homogeneous(imCorner[i]));
        
        float X0 = X[0];
        float X1 = X[1];
        float X2 = X[2];
  
        int q = (int)(4 * (1 - sgn(X0))/2 + 2 * (1 - sgn(X1))/2 + (1 - sgn(X2))/2);
        
        quadrant[q] = true;
    }
    
    bool splitPhiMin = (quadrant[0] && quadrant[1] && quadrant[2] && quadrant[3]);
    bool splitPhiMax = (quadrant[4] && quadrant[5] && quadrant[6] && quadrant[7]);
    bool splitPhi = splitPhiMax || splitPhiMin;
    bool splitTheta = (!splitPhi) && ((quadrant[1] && quadrant[3]) || (quadrant[5] && quadrant[7]));

    //
    // Find bounding box in theta/phi plane
    // (also use midpoints of each edge to form bounding box)
    //

    imCorner[4].Create(2, 1); imCorner[4][0] = (nRowsImage - 1.0f)/2.0f; imCorner[4][1] = 0.0f;
    imCorner[5].Create(2, 1); imCorner[5][0] = (nRowsImage - 1.0f)/2.0f; imCorner[5][1] = nColsImage - 1.0f; 
    imCorner[6].Create(2, 1); imCorner[6][0] = 0.0f;                     imCorner[6][1] = (nColsImage - 1.0f)/2.0f; 
    imCorner[7].Create(2, 1); imCorner[7][0] = nRowsImage - 1.0f;        imCorner[7][1] = (nColsImage - 1.0f)/2.0f; 
    
    CMatrix X(3, 1);

    float cornerThetaMax = (float)-M_PI;
    float cornerThetaMin = (float)M_PI;
    float cornerPhiMax   = (float)-M_PI/2;
    float cornerPhiMin   = (float)M_PI/2;
    
    float cornerThetaGt0Min = (float)M_PI;
    float cornerThetaLt0Max = (float)-M_PI;
    float cornerPhiGt0Min   = (float)M_PI/2;
    float cornerPhiLt0Max   = (float)-M_PI/2;

    for (int i = 0; i < 8; i++)
    {
        X = unit(Pinv * homogeneous(imCorner[i]));
        
        float X0 = X[0];
        float X1 = X[1];
        float X2 = X[2];
        
        float sqrtX12X22 = (float)sqrt(X1*X1 + X2*X2);
        
        float phi   = - sgn(X0) * (float)acos(sqrtX12X22);
        float theta =   sgn(X1) * (float)acos( X2 / sqrtX12X22 ); 
        
        cornerThetaMax = max(cornerThetaMax, theta);
        cornerThetaMin = min(cornerThetaMin, theta);
        cornerPhiMax   = max(cornerPhiMax,   phi  );
        cornerPhiMin   = min(cornerPhiMin,   phi  );        

        // in case this image is split over theta/phi
        if (theta > 0)
            cornerThetaGt0Min = min(cornerThetaGt0Min, theta);
        else // if theta <= 0
            cornerThetaLt0Max = max(cornerThetaLt0Max, theta);
        if (phi > 0)
            cornerPhiGt0Min   = min(cornerPhiGt0Min, phi);
        else // if phi <= 0
            cornerPhiLt0Max   = max(cornerPhiLt0Max, phi);
    }
    
    if (splitTheta)
    {
        cornerThetaMin = cornerThetaGt0Min;
        cornerThetaMax = cornerThetaLt0Max;
    }

    if (splitPhi)
    {
        cornerPhiMin = cornerPhiGt0Min;
        cornerPhiMax = cornerPhiLt0Max;
    }
    
    //
    // Handle the 4 cases
    //

    if (!splitTheta && !splitPhi)
    {        
        CImagePieceRenderInfo piece1(cornerThetaMin, cornerThetaMax, cornerPhiMin, cornerPhiMax);
        piece.push_back(piece1);
    }
    else if (splitTheta && !splitPhi)
    {
        CImagePieceRenderInfo piece1((float)-M_PI, cornerThetaMax, cornerPhiMin, cornerPhiMax);
        CImagePieceRenderInfo piece2(cornerThetaMin, (float)M_PI, cornerPhiMin, cornerPhiMax);
        piece.push_back(piece1);
        piece.push_back(piece2);
    }
    else if (splitPhiMax)
    {
        CImagePieceRenderInfo piece1((float)-M_PI, (float)M_PI, cornerPhiMin, (float)M_PI/2);
        piece.push_back(piece1);
    }

    else if (splitPhiMin)
    {
        CImagePieceRenderInfo piece1((float)-M_PI, (float)M_PI, (float)-M_PI/2, cornerPhiMax);
        piece.push_back(piece1);
    }

    // Compute theta/phi distances between corners
    // for downsample computation

    float theta[3];
    float phi[3];

    float &dtheta1 = ImageRenderInfo.dtheta1;
    float &dtheta2 = ImageRenderInfo.dtheta2;
    float &dphi1   = ImageRenderInfo.dphi1;
    float &dphi2   = ImageRenderInfo.dphi2;

    for (int i = 0; i < 3; i++)
    {
        X = unit(Pinv * homogeneous(imCorner[i]));
        
        float X0 = X[0];
        float X1 = X[1];
        float X2 = X[2];
        
        float sqrtX12X22 = (float)sqrt(X1*X1 + X2*X2);
        
        phi[i]   = - sgn(X0) * (float)acos(sqrtX12X22);
        theta[i] =   sgn(X1) * (float)acos( X2 / sqrtX12X22 ); 
    }
  
    if (!splitTheta && !splitPhi)
    {	
        dtheta1 = (float)fabs(theta[1] - theta[0]);
        dtheta2 = (float)fabs(theta[2] - theta[0]);
        dphi1   = (float)fabs(phi[1] - phi[0]);
        dphi2   = (float)fabs(phi[2] - phi[0]);
    }
    else if (splitTheta && !splitPhi)
    {
        dtheta1 = (float)fabs(theta[1] - theta[0]);
        if (dtheta1 > (float)M_PI)
            dtheta1 = (float)fabs(min(theta[1], theta[0]) + 2.0f * (float)M_PI - max(theta[1], theta[0]));
        dtheta2 = (float)fabs(theta[2] - theta[0]);
        if (dtheta2 > (float)M_PI)
            dtheta2 = (float)fabs(min(theta[2], theta[0]) + 2.0f * (float)M_PI - max(theta[2], theta[0]));
        dphi1   = (float)fabs(phi[1] - phi[0]);
        dphi2   = (float)fabs(phi[2] - phi[0]);
    }
    else if (splitPhiMax)
    {
        dtheta1 = 2.0f * (float)M_PI;
        //dtheta2 = 2.0f * (float)M_PI;
        dtheta2 = 0;
        dphi1 = 0;
        //dphi1   = (M_PI/2) - cornerPhiMin;
        dphi2   = ((float)M_PI/2.0f) - cornerPhiMin;
    }
    else if (splitPhiMin)
    {
        dtheta1 = 2.0f * (float)M_PI;
        //dtheta2 = 2.0f * (float)M_PI;
        dtheta2 = 0;
        dphi1   = 0;
        //dphi1   = cornerPhiMax - (-M_PI/2);
        dphi2   = cornerPhiMax - (-(float)M_PI/2.0f);
    }
    // Note on edits above: if the images are split over theta
    // the downsample distances are computed as dtheta1^2 and dphi2^2 instead

}

void CRenderPanorama::ComputeNonZeroRange(CImage &im, vector<int> &rowBegin, vector<int> &rowEnd, vector<int> &colBegin, vector<int> &colEnd)
{

    rowBegin.resize(im.nRows);
    rowEnd.resize(im.nRows);
    colBegin.resize(im.nCols);
    colEnd.resize(im.nCols);
    assert(im.nBands == 1);

    //
    // Compute row bounds
    //

    float *imrowptr = im.data;
    bool done = false;
    
    for (int row = 0; row < im.nRows; row++)
    {
        float *imptr = imrowptr;

        int col = 0;
        done = false;
        while (!done && col < im.nCols)
        {
            if (*imptr > 0) 
                done = true;
           
            imptr++;
            col++;   
        }
        col--;
        rowBegin[row] = col;
        int colbegin = col;

        imptr = imrowptr + (im.nCols - 1);
        col = im.nCols - 1;
        done = false;
        while (!done && col >= colbegin)
        {
            if (*imptr > 0)
                done = true;
   
            imptr--;
            col--;
        }
        col++;
        rowEnd[row] = col;

        imrowptr += im.nCols;
    }// end for row

    // 
    // Compute col bounds
    //

    float *imcolptr = im.data;
    
    for (int col = 0; col < im.nCols; col++)
    {
        float *imptr = imcolptr;

        int row = 0;
        done = false;
        while (!done && row < im.nRows)
        {
            if (*imptr > 0)
                done = true;

            imptr += im.nCols;
            row++;
        }
        row--;
        colBegin[col] = row;
        int rowbegin = row;

        imptr = imcolptr + (im.nRows - 1) * im.nCols;
        row = im.nRows - 1;
        done = false;
        while (!done && row >= rowbegin)
        {
            if (*imptr > 0)
                done = true;

            imptr -= im.nCols;
            row--;
        }
        row++;
        colEnd[col] = row;
        
        imcolptr++;
    }// end for row

}// end ComputeNonZeroRange    


void CRenderPanorama::ComputeWeightIsMax(CImage &weightMax, CImagePieceRenderInfo &piece)
{
    assert(weightMax.nBands == 1);
    int nColsPano     = weightMax.nCols;
    int nRowsPano     = weightMax.nRows;

    float *weightMaxptr = weightMax.data + piece.xmin * nColsPano + piece.ymin;
    float *weightMaxcolptr = weightMaxptr;

    float *weightLinearptr = piece.weightLinear.data;
    piece.weight.Create(piece.xmax - piece.xmin + 1, piece.ymax - piece.ymin + 1, 1);
    float *weightptr = piece.weight.data;
    
    for (int x = piece.xmin; x <= piece.xmax; x++)
    {
        weightMaxptr = weightMaxcolptr;

        for (int y = piece.ymin; y <= piece.ymax; y++)
        {
            if (*weightLinearptr >= *weightMaxptr && *weightMaxptr > 0)
                *weightptr = 1;
            else
                *weightptr = 0;
            
            weightLinearptr++;
            weightptr++;
            weightMaxptr++;            

        }// end for y        

        weightMaxcolptr += nColsPano;
    }// end for x

}// end WeightIsMax
    

void CRenderPanorama::RenderPiece(CImage &weightMax, CImage &image, CImagePieceRenderInfo &piece, CMatrix P)
{
    // panorama dimensions
    assert(weightMax.nBands == 1);
    int nColsPano     = weightMax.nCols;
    int nRowsPano     = weightMax.nRows;
    int nBands        = image.nBands;

    int nRowsImage  = image.nRows;
    int nColsImage  = image.nCols;
    float nRows2    = (nRowsImage - 1)/2.0f;
    float nCols2    = (nColsImage - 1)/2.0f;
    float nRowsCols = (nRowsImage - 1) * (nColsImage - 1) / 4.0f;

    float P00 = P(0, 0); float P01 = P(0, 1); float P02 = P(0, 2);
    float P10 = P(1, 0); float P11 = P(1, 1); float P12 = P(1, 2);
    float P20 = P(2, 0); float P21 = P(2, 1); float P22 = P(2, 2);
    
    float *weightMaxptr = weightMax.data + piece.xmin * nColsPano + piece.ymin;
    float *weightMaxcolptr = weightMaxptr;

    int pieceRows = piece.xmax - piece.xmin + 1;
    int pieceCols = piece.ymax - piece.ymin + 1;

    piece.image.Create(pieceRows, pieceCols, nBands);
    piece.weightLinear.Create(pieceRows, pieceCols, 1);

    float *impieceptr = piece.image.data;
    float *weightptr  = piece.weightLinear.data;

    float theta = piece.thetaMin;
    float phi   = piece.phiMax;
            
    for (int x = piece.xmin; x <= piece.xmax; x++)
    {
        weightMaxptr = weightMaxcolptr;
        theta        = piece.thetaMin;
   
        for (int y = piece.ymin; y <= piece.ymax; y++)
        {
            float x0 = -(float)sin(phi);
            float x1 =  (float)cos(phi) * (float)sin(theta);
            float x2 =  (float)cos(phi) * (float)cos(theta);

            float u0 = P00 * x0 + P01 * x1 + P02 * x2;
            float u1 = P10 * x0 + P11 * x1 + P12 * x2;
            float u2 = P20 * x0 + P21 * x1 + P22 * x2;
            
            int ui = (int)floor((u0/u2) + 0.5);
            int vi = (int)floor((u1/u2) + 0.5);
            
            if ( (ui >= 0) && (ui < nRowsImage) && (vi >= 0) && (vi < nColsImage) )
            {
                float eps = (float)1E-6;
                float weight = (nRows2 - (float)fabs(ui - nRows2)) * (nCols2 - (float)fabs(vi - nCols2)) / nRowsCols; 
                weight += eps;

                float* imptr = &image(ui, vi, 0);
                for (int b = 0; b < nBands; b++)
                {
                    *impieceptr = *imptr;
                    impieceptr++;
                    imptr++;
                }
                *weightptr    = weight;
                *weightMaxptr = max(*weightMaxptr, weight);
            }
            else 
            {
                impieceptr += nBands;
            }

            weightptr++;
            weightMaxptr++;
            theta += dthetady;

        }// end for y
        
        weightMaxcolptr += nColsPano;
        phi             += dphidx;
            
    }// end for x

}

void CRenderPanorama::RenderPieceSampleBilinear(CImage &weightMax, CByteImage &alpha, CImage &image, CImagePieceRenderInfo &piece, CMatrix P)
// Sample image piece using bilinear sampling
{
    // panorama dimensions
    assert(weightMax.nBands == 1);
    assert(alpha.nBands == 1);
    int nColsPano     = weightMax.nCols;
    int nRowsPano     = weightMax.nRows;
    int nBands        = image.nBands;

    int nRowsImage  = image.nRows;
    int nColsImage  = image.nCols;
    const int rowStrideImage = image.rowStride();
    const int colStrideImage = image.colStride();
    float nRows2    = (nRowsImage - 1)/2.0f;
    float nCols2    = (nColsImage - 1)/2.0f;
    float nRowsCols = (nRowsImage - 1) * (nColsImage - 1) / 4.0f;

    float P00 = P(0, 0); float P01 = P(0, 1); float P02 = P(0, 2);
    float P10 = P(1, 0); float P11 = P(1, 1); float P12 = P(1, 2);
    float P20 = P(2, 0); float P21 = P(2, 1); float P22 = P(2, 2);
    
    float *weightMaxptr = weightMax.data + piece.xmin * nColsPano + piece.ymin;
    float *weightMaxcolptr = weightMaxptr;
    unsigned char* alphaptr = alpha.data + piece.xmin * nColsPano + piece.ymin;
    unsigned char* alphacolptr = alphaptr;

    int pieceRows = piece.xmax - piece.xmin + 1;
    int pieceCols = piece.ymax - piece.ymin + 1;

    piece.image.Create(pieceRows, pieceCols, nBands);
    piece.weightLinear.Create(pieceRows, pieceCols, 1);

    float *impieceptr = piece.image.data;
    float *weightptr  = piece.weightLinear.data;

    float theta = piece.thetaMin;
    float phi   = piece.phiMax;

    for (int x = piece.xmin, xrel = 0; x <= piece.xmax; x++, xrel++)
    {      
        weightMaxptr = weightMaxcolptr;
        alphaptr     = alphacolptr;
        theta        = piece.thetaMin;

        for (int y = piece.ymin, yrel = 0; y <= piece.ymax; y++, yrel++)
        {
            float x0 = -(float)sin(phi);
            float x1 =  (float)cos(phi) * (float)sin(theta);
            float x2 =  (float)cos(phi) * (float)cos(theta); 

            float u0 = P00 * x0 + P01 * x1 + P02 * x2;
            float u1 = P10 * x0 + P11 * x1 + P12 * x2;
            float u2 = P20 * x0 + P21 * x1 + P22 * x2;
  
            float u = u0/u2;
            float v = u1/u2;
            int ui = (int)floor(u);
            int vi = (int)floor(v);

            if ( (ui >= 0) && (ui + 1 < nRowsImage) && (vi >= 0) && (vi + 1 < nColsImage) )
            {	        	
                float uf = u - ui;
                float vf = v - vi;
                float oneMinusuf = 1 - uf;
                float oneMinusvf = 1 - vf;

                float eps = (float)1E-6;
                float weight = (nRows2 - (float)fabs(ui - nRows2)) * (nCols2 - (float)fabs(vi - nCols2)) / nRowsCols; 
                weight += eps;

                float* imptr = &image(ui, vi, 0);
			
                for (int b = 0; b < nBands; b++)
                {
                    float I00  = *imptr;
                    imptr += colStrideImage;
                    float I01  = *imptr;
                    imptr += rowStrideImage;
                    float I11  = *imptr;
                    imptr -= colStrideImage;
                    float I10 = *imptr;               
                    imptr -= rowStrideImage; 

                    *impieceptr = I00 * oneMinusuf * oneMinusvf + 
                        I01 * oneMinusuf *         vf + 
                        I10 *         uf * oneMinusvf +
                        I11 *         uf *         vf;

                    impieceptr++;
                    imptr++;
                }
                *weightptr    = weight;
                *weightMaxptr = max(*weightMaxptr, weight);
                *alphaptr = 1;	      
            }
            else 
            {
                impieceptr += nBands;
            }

            weightptr++;
            weightMaxptr++;
            alphaptr++;
            theta += dthetady;
        }// end for y
        
        weightMaxcolptr += nColsPano;
        alphacolptr     += nColsPano;
        phi             += dphidx;

    }// end for x
}

void CRenderPanorama::RenderPieceSampleCubicBSpline(CImage &weightMax, CImage &image, CImagePieceRenderInfo &piece, CMatrix P)
// Sample image piece using cubic B-spline interpolation
{
    // panorama dimensions
    assert(weightMax.nBands == 1);
    int nColsPano     = weightMax.nCols;
    int nRowsPano     = weightMax.nRows;
    int nBands        = image.nBands;

    int nRowsImage  = image.nRows;
    int nColsImage  = image.nCols;
    const int rowStrideImage = image.rowStride();
    const int colStrideImage = image.colStride();
    float nRows2    = (nRowsImage - 1)/2.0f;
    float nCols2    = (nColsImage - 1)/2.0f;
    float nRowsCols = (nRowsImage - 1) * (nColsImage - 1) / 4.0f;

    float P00 = P(0, 0); float P01 = P(0, 1); float P02 = P(0, 2);
    float P10 = P(1, 0); float P11 = P(1, 1); float P12 = P(1, 2);
    float P20 = P(2, 0); float P21 = P(2, 1); float P22 = P(2, 2);
    
    float *weightMaxptr = weightMax.data + piece.xmin * nColsPano + piece.ymin;
    float *weightMaxcolptr = weightMaxptr;

    int pieceRows = piece.xmax - piece.xmin + 1;
    int pieceCols = piece.ymax - piece.ymin + 1;

    piece.image.Create(pieceRows, pieceCols, nBands);
    piece.weightLinear.Create(pieceRows, pieceCols, 1);

    float *impieceptr = piece.image.data;
    float *weightptr  = piece.weightLinear.data;

    float theta = piece.thetaMin;
    float phi   = piece.phiMax;

    for (int x = piece.xmin; x <= piece.xmax; x++)
    {
        weightMaxptr = weightMaxcolptr;
        theta        = piece.thetaMin;

        for (int y = piece.ymin; y <= piece.ymax; y++)
        {
            float x0 = -(float)sin(phi);
            float x1 =  (float)cos(phi) * (float)sin(theta);
            float x2 =  (float)cos(phi) * (float)cos(theta); 

            float u0 = P00 * x0 + P01 * x1 + P02 * x2;
            float u1 = P10 * x0 + P11 * x1 + P12 * x2;
            float u2 = P20 * x0 + P21 * x1 + P22 * x2;
  
            float u = u0/u2;
            float v = u1/u2;
            int ui = (int)floor(u);
            int vi = (int)floor(v);

            if ( (ui > 0) && (ui < nRowsImage - 2) && (vi > 0) && (vi < nColsImage - 2) )
            {	        	
                float uf = u - ui;
                float vf = v - vi;
				
                float eps = (float)1E-6;
                float weight = (nRows2 - (float)fabs(ui - nRows2)) * (nCols2 - (float)fabs(vi - nCols2)) / nRowsCols; 
                weight += eps;

                //float* imptr = &image(ui, vi, 0);
			
                for (int b = 0; b < nBands; b++)
                {
                    for (int vr = -1; vr <= 2; vr++)
		    {
                        for (int ur = -1; ur <= 2; ur++)
			{
                            *impieceptr += image(ui + ur, vi + vr, b) * R(vr,vf) * R(ur,uf);
			}
		    }
                    *impieceptr++;		
		}
					
                
                *weightptr    = weight;
                *weightMaxptr = max(*weightMaxptr, weight);
            }
            else 
            {
                impieceptr += nBands;
            }

            weightptr++;
            weightMaxptr++;
            theta += dthetady;

        }// end for y
        
        weightMaxcolptr += nColsPano;
        phi             += dphidx;

    }// end for x
}

void CRenderPanorama::AddBand(CImage &pano, CImage &num, CImage &den)
{       
    float *panoptr = pano.data;
    float *numptr  = num.data;
    float *denptr  = den.data;

    for (int i = 0; i < pano.nRows * pano.nCols; i++)
    {
        if (*denptr > 0)
        {
            for (int b = 0; b < pano.nBands; b++)
            {
                *panoptr += *numptr / *denptr;
                panoptr++;
                numptr++;                
            }// end for b
        }// end if den > 0
        else
        {
            panoptr += pano.nBands;
            numptr += pano.nBands;
        }
        denptr++;
    }// end for i    
}

void CRenderPanorama::BlendPiece(CImage &num, CImage &den, CImagePieceRenderInfo &piece, CImage &image, CImage &weight)
{
    // panorama dimensions
    assert(den.nCols == num.nCols);
    assert(den.nRows == num.nRows);
    int nColsPano     = num.nCols;
    int nRowsPano     = num.nRows;
    assert(den.nBands == 1);
    int nBands        = num.nBands;

    float *numptr = num.data + piece.xmin * nColsPano * nBands + piece.ymin * nBands;
    float *numcolptr = numptr;
    
    float *denptr = den.data + piece.xmin * nColsPano + piece.ymin;
    float *dencolptr = denptr;
    
    float *imptr = image.data;
    float *weightptr = weight.data;

    for (int x = piece.xmin; x <= piece.xmax; x++)
    {
        numptr = numcolptr;
        denptr = dencolptr;

        for (int y = piece.ymin; y <= piece.ymax; y++)
        {
            if (*weightptr > 0)
            {
                for (int b = 0; b < nBands; b++)
                {
                    *numptr += *weightptr * *imptr;
                    numptr++;
                    imptr++;
                }
                *denptr += *weightptr;
            }
            else
            {
                numptr += nBands;
                imptr += nBands;
            }
            
            weightptr++;
            denptr++;
        }// end for y

        numcolptr += nColsPano * nBands;
        dencolptr += nColsPano;

    }// end for x

}// end BlendPiece

void CRenderPanorama::RenderPieceLinearBlend(CImage &num, CImage &den, CByteImage &alpha, CImage &image, CImagePieceRenderInfo &piece, CMatrix P)
{
    // panorama dimensions
    assert(den.nCols == num.nCols);
    assert(den.nRows == num.nRows);
    int nColsPano     = num.nCols;
    int nRowsPano     = num.nRows;
    assert(image.nBands == num.nBands);
    assert(den.nBands   == 1);
    int nBands        = num.nBands;

    int nRowsImage  = image.nRows;
    int nColsImage  = image.nCols;
    float nRows2    = (nRowsImage - 1)/2.0f;
    float nCols2    = (nColsImage - 1)/2.0f;
    float nRowsCols = (nRowsImage - 1) * (nColsImage - 1) / 4.0f;

    float P00 = P(0, 0); float P01 = P(0, 1); float P02 = P(0, 2);
    float P10 = P(1, 0); float P11 = P(1, 1); float P12 = P(1, 2);
    float P20 = P(2, 0); float P21 = P(2, 1); float P22 = P(2, 2);

    int rowStrideNum   = nColsPano * nBands;
    int colStrideNum   = nBands;
    int rowStrideDen   = nColsPano;
    int colStrideDen   = 1;
    int rowStrideAlpha = nColsPano;
    int colStrideAlpha = 1;
  
    float *numptr    = num.data + piece.xmin * rowStrideNum + piece.ymin * colStrideNum;
    float *numcolptr = numptr;

    float *denptr    = den.data + piece.xmin * rowStrideDen + piece.ymin * colStrideDen;
    float *dencolptr = denptr;
  
    unsigned char* alphaptr = alpha.data + piece.xmin * rowStrideAlpha + piece.ymin * colStrideAlpha;
    unsigned char* alphacolptr = alphaptr;
  
    float theta = piece.thetaMin;
    float phi   = piece.phiMax;
            
    for (int x = piece.xmin; x <= piece.xmax; x++)
    {
        numptr = numcolptr;
        denptr = dencolptr;
        alphaptr = alphacolptr;
        theta  = piece.thetaMin;
   
        for (int y = piece.ymin; y <= piece.ymax; y++)
        {
            float x0 = -(float)sin(phi);
            float x1 =  (float)cos(phi) * (float)sin(theta);
            float x2 =  (float)cos(phi) * (float)cos(theta);

            float u0 = P00 * x0 + P01 * x1 + P02 * x2;
            float u1 = P10 * x0 + P11 * x1 + P12 * x2;
            float u2 = P20 * x0 + P21 * x1 + P22 * x2;
            
            int ui = (int)floor((u0/u2) + 0.5);
            int vi = (int)floor((u1/u2) + 0.5);
            
            if ( (ui >= 0) && (ui < nRowsImage) && (vi >= 0) && (vi < nColsImage) )
            {
                float weight = (nRows2 - (float)fabs(ui - nRows2)) * (nCols2 - (float)fabs(vi - nCols2)) / nRowsCols; 

                float* imptr = &image(ui, vi, 0);
                for (int b = 0; b < nBands; b++)
                {
                    *numptr += *imptr * weight;
                    numptr++;
                    imptr++;
                }
                *denptr += weight;
                *alphaptr = 1;
            }
            else 
            {
                numptr += colStrideNum;
            }

            denptr++;
            alphaptr++;
            theta += dthetady;

        }// end for y
        
        numcolptr += rowStrideNum;
        dencolptr += rowStrideDen;
        alphacolptr += rowStrideAlpha;
        phi       += dphidx;
            
    }// end for x

}

void CRenderPanorama::RenderPieceNoBlend(CImage &pano, CByteImage &alpha, CImage &image, CImagePieceRenderInfo &piece, CMatrix P)
{
    // panorama dimensions
    int nColsPano     = pano.nCols;
    int nRowsPano     = pano.nRows;
    assert(image.nBands == pano.nBands);
    int nBands        = pano.nBands;

    int nRowsImage  = image.nRows;
    int nColsImage  = image.nCols;
    float nRows2    = (nRowsImage - 1)/2.0f;
    float nCols2    = (nColsImage - 1)/2.0f;
    float nRowsCols = (nRowsImage - 1) * (nColsImage - 1) / 4.0f;

    float P00 = P(0, 0); float P01 = P(0, 1); float P02 = P(0, 2);
    float P10 = P(1, 0); float P11 = P(1, 1); float P12 = P(1, 2);
    float P20 = P(2, 0); float P21 = P(2, 1); float P22 = P(2, 2);

    int rowStride      = nColsPano * nBands;
    int colStride      = nBands;
    int alphaRowStride = nColsPano;
    int alphaColStride = 1;

    float *panoptr    = pano.data + piece.xmin * rowStride + piece.ymin * colStride;
    float *panocolptr = panoptr;
    unsigned char *alphaptr   = alpha.data + piece.xmin * alphaRowStride + piece.ymin * alphaColStride;
    unsigned char *alphacolptr = alphaptr;

    float theta = piece.thetaMin;
    float phi   = piece.phiMax;
            
    for (int x = piece.xmin; x <= piece.xmax; x++)
    {
        panoptr  = panocolptr;
        alphaptr = alphacolptr;
        theta    = piece.thetaMin;
   
        for (int y = piece.ymin; y <= piece.ymax; y++)
        {
            float x0 = -(float)sin(phi);
            float x1 =  (float)cos(phi) * (float)sin(theta);
            float x2 =  (float)cos(phi) * (float)cos(theta);

            float u0 = P00 * x0 + P01 * x1 + P02 * x2;
            float u1 = P10 * x0 + P11 * x1 + P12 * x2;
            float u2 = P20 * x0 + P21 * x1 + P22 * x2;
            
            int ui = (int)floor((u0/u2) + 0.5);
            int vi = (int)floor((u1/u2) + 0.5);
            
            if ( (ui >= 0) && (ui < nRowsImage) && (vi >= 0) && (vi < nColsImage) )
            {
                float* imptr = &image(ui, vi, 0);
                memcpy(panoptr, imptr, nBands * sizeof(float));
                *alphaptr = 1;
            }
	  
            panoptr += colStride;
            alphaptr += alphaColStride;
            theta += dthetady;

        }// end for y
        
        panocolptr  += rowStride;
        alphacolptr += alphaRowStride;
        phi         += dphidx;
            
    }// end for x

}
