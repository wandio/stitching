//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// ImageHelpers.h

#pragma once

#include "Image.h"

vector<float> SumRectangle(CImage &im, int rowMin, int rowMax, int colMin, int colMax); // compute sum of pixel values in specified rectangle
