//
// Copyright (c) 2011. Matthew Brown, University of British Columbia.
// This code may be used, distributed, or modified only under license
// from the University of British Columbia.  This notice must be retained
// in all copies.
//

// ByteImage.cpp

#include "ByteImage.h"
#include "jpegio.h"
#include "math.h"
#include <iostream>
#include "assert.h"
#include <stdio.h>
#include <string.h>

using namespace std;

CByteImage::CByteImage()
{
    nRows = 0;
    nCols = 0;
    nBands = 0;
    data = NULL;
}

CByteImage::CByteImage(int rows, int cols, int bands)
{
    data = NULL;
    Create(rows, cols, bands);
}

CByteImage::~CByteImage()
{
    delete[] data;
}

CByteImage::CByteImage(const CByteImage &im)
{
    data = NULL;
    this->operator=(im);     
}

void CByteImage::operator=(const CByteImage &im)
{
    Create(im.nRows, im.nCols, im.nBands);
    memcpy(data, im.data, nRows * nCols * nBands * sizeof(unsigned char));       
}

unsigned char &CByteImage::operator()(int row, int col, int band)
{
    return data[row * nCols * nBands + col * nBands + band];
}

const unsigned char CByteImage::operator()(int row, int col, int band) const
{
    return data[row * nCols * nBands + col * nBands + band];
}

int CByteImage::rowStride(void) const
{
    return(nCols * nBands);
}

int CByteImage::colStride(void) const
{
    return(nBands);
}

void CByteImage::Create(char* filename)
{
    delete[] data;
    ReadJPEG0(data, nCols, nRows, nBands, filename);
  
    if (nBands == 1)
        *this = ConvertToColour(*this);
}

void CByteImage::Create(int rows, int cols, int bands)
{
    delete[] data;
  
    nRows = rows; 
    nCols = cols;
    nBands = bands;
  
    int nData = nRows * nCols * nBands;
  
    data = new unsigned char[nData];
    memset(data, 0, nData * sizeof(unsigned char));
}

void CByteImage::Create(const CImage &im)
{
    Create(im.nRows, im.nCols, im.nBands);

    int nData = nRows * nCols * nBands;
    unsigned char* bptr = data;
    float*         fptr = im.data;

    for (int i = 0; i < nData; i++)
    {
        *bptr = (unsigned char)(*fptr);
        bptr++;
        fptr++;
    }
}

void CByteImage::Clear(void)
{
    delete[] data;
    nRows = 0; 
    nCols = 0;
    nBands = 0;
    data = NULL;
}

void CByteImage::WriteJPEG(const char* filename, int quality)
{
    WriteJPEG0(data, nRows, nCols, nBands, quality, filename);
}
  
void CByteImage::assign(const CByteImage &im1, int row, int col)
// copy the image im1 to row, col in this im
{
    assert(im1.nBands == nBands); 
    assert(row + im1.nRows <= nRows); 
    assert(col + im1.nCols <= nCols);

    CByteImage &im = *this;

    for (int i = 0; i < im1.nRows; i++)
    {
        for (int j = 0; j < im1.nCols; j++)
        {
            for (int b = 0; b < im1.nBands; b++) 
	    {
                im(row + i, col + j, b) = im1(i, j, b);
	    }
        }
    }
}

void CByteImage::assign(const CImage &im1, int row, int col)
// copy the (float) image im1 to row, col in this (byte) im
{
    assert(im1.nBands == nBands); 
    assert(row + im1.nRows <= nRows); 
    assert(col + im1.nCols <= nCols);

    CByteImage &im = *this;

    for (int i = 0; i < im1.nRows; i++)
    {
        for (int j = 0; j < im1.nCols; j++)
        {
            for (int b = 0; b < im1.nBands; b++) 
	    {
                im(row + i, col + j, b) = (unsigned char)im1(i, j, b);
	    }
        }
    }
}

void CByteImage::Copy(int rowDst, int colDst, const CImage &imSrc, int rowSrcMin, int colSrcMin, int rowSrcMax, int colSrcMax)
// copy rowSrcMin, colSrcMin to rowSrcMax, colSrcMax of (float) image imSrc 
// to rowDst, colDst in this (byte) image
// (no clipping)
{
    assert(imSrc.nBands == nBands);
    int nRowsCopy = rowSrcMax - rowSrcMin + 1;
    int nColsCopy = colSrcMax - colSrcMin + 1;
    assert(nRowsCopy > 0);
    assert(nColsCopy > 0);
    assert(rowDst >= 0);
    assert(colDst >= 0);
    assert(rowDst + nRowsCopy <= nRows);
    assert(colDst + nColsCopy <= nCols);

    int dstRowStride = rowStride();
    int dstColStride = colStride();
    unsigned char* dstRowPtr = data + rowDst * dstRowStride + colDst * dstColStride;
    unsigned char* dstPtr    = dstRowPtr;

    int srcRowStride = imSrc.rowStride();
    int srcColStride = imSrc.colStride();
    float *srcRowPtr = imSrc.data + rowSrcMin * srcRowStride + colSrcMin * srcColStride;
    float *srcPtr    = srcRowPtr;

    for (int i = 0; i < nRowsCopy; i++)
    {		
        for (int j = 0; j < nColsCopy; j++)
	{
            for (int b = 0; b < nBands; b++)
	    {
                *dstPtr = (unsigned char) (*srcPtr);
                dstPtr++;
                srcPtr++;
	    }// end for b
	}// end for j
        srcRowPtr += srcRowStride;
        dstRowPtr += dstRowStride;
        srcPtr = srcRowPtr;
        dstPtr = dstRowPtr;
    }// end for i

}// end Copy

void CByteImage::Copy(int rowDst, int colDst, const CByteImage &imSrc, int rowSrcMin, int colSrcMin, int rowSrcMax, int colSrcMax)
// copy rowSrcMin, colSrcMin to rowSrcMax, colSrcMax of image imSrc 
// to rowDst, colDst in this (byte) image
// (no clipping)
{
    assert(imSrc.nBands == nBands);
    int nRowsCopy = rowSrcMax - rowSrcMin + 1;
    int nColsCopy = colSrcMax - colSrcMin + 1;
    assert(nRowsCopy > 0);
    assert(nColsCopy > 0);
    assert(rowDst >= 0);
    assert(colDst >= 0);
    assert(rowDst + nRowsCopy <= nRows);
    assert(colDst + nColsCopy <= nCols);

    int dstRowStride = rowStride();
    int dstColStride = colStride();
    unsigned char* dstRowPtr = data + rowDst * dstRowStride + colDst * dstColStride;
    unsigned char* dstPtr    = dstRowPtr;

    int srcRowStride = imSrc.rowStride();
    int srcColStride = imSrc.colStride();

    unsigned char *srcRowPtr = imSrc.data + rowSrcMin * srcRowStride + colSrcMin * srcColStride;
    unsigned char *srcPtr    = srcRowPtr;

    for (int i = 0; i < nRowsCopy; i++)
    {		
        for (int j = 0; j < nColsCopy; j++)
	{
            memcpy(dstPtr, srcPtr, nBands);
            dstPtr += nBands;
            srcPtr += nBands;
	}// end for j
        srcRowPtr += srcRowStride;
        dstRowPtr += dstRowStride;
        srcPtr = srcRowPtr;
        dstPtr = dstRowPtr;
    }// end for i

}// end Copy


CByteImage ConvertToColour(const CByteImage &gim)
{
    CByteImage im(gim.nRows, gim.nCols, 3);
    int nPixels = gim.nRows * gim.nCols;

    unsigned char *gimptr = gim.data;
    unsigned char *imptr  =  im.data;
    
    for (int i = 0; i < nPixels; i++)
    {
        for (int b = 0; b < 3; b++)
        {
            *imptr = *gimptr;
            imptr++;
        }
        gimptr++;
    }

    return(im);
}

CByteImage operator*(const CByteImage &im1, unsigned char f)
{
    CByteImage im(im1.nRows, im1.nCols, im1.nBands);
    
    unsigned char *im1ptr = im1.data;
    unsigned char *imptr = im.data;
    
    for (int i = 0; i < im.nRows * im.nCols * im.nBands; i++)
    {
        *imptr = *im1ptr * f;
        im1ptr++;
        imptr++;
    }
    
    return(im);
}

void ByteImageToImage(CImage &im, const CByteImage &bim)
{
    im.Create(bim.nRows, bim.nCols, bim.nBands);
    int nData = bim.nRows * bim.nCols * bim.nBands;
    float *fptr = im.data;
    unsigned char *bptr = bim.data;
  
    for (int i = 0; i < nData; i++)
    {
        *fptr = (float)*bptr;
        fptr++;
        bptr++;
    } 
}

void DrawRect(CByteImage &im, int row0, int col0, int row1, int col1, vector<float> colour)
{
    row0 = min(max(0, row0), im.nRows - 1);
    col0 = min(max(0, col0), im.nCols - 1);
    row1 = min(max(0, row1), im.nRows - 1);
    col1 = min(max(0, col1), im.nCols - 1);
  
    for (int r = row0; r < row1; r++)
        for (int b = 0; b < im.nBands; b++)
            im(r, col0, b) = colour[b];

    for (int r = row0; r < row1; r++)
        for (int b = 0; b < im.nBands; b++)
            im(r, col1, b) = colour[b];

    for (int c = col0; c < col1; c++)
        for (int b = 0; b < im.nBands; b++)
            im(row0, c, b) = colour[b];

    for (int c = col0; c < col1; c++)
        for (int b = 0; b < im.nBands; b++)
            im(row1, c, b) = colour[b];
}

void DrawRect3(CByteImage &im, int row0, int col0, int row1, int col1, vector<float> colour)
{
    DrawRect(im, row0, col0, row1, col1, colour);
    DrawRect(im, row0 - 1, col0 - 1, row1 + 1, col1 + 1, colour);
    DrawRect(im, row0 + 1, col0 + 1, row1 - 1, col1 - 1, colour);
}


void SubSample2Block(CByteImage &sim, const CByteImage &im)
// Subsample image using 2x2 block average
{
    int nRows  = im.nRows;
    int nCols  = im.nCols;
    int nBands = im.nBands;
    int nRows2 = (int)floor(nRows / 2.0f);
    int nCols2 = (int)floor(nCols / 2.0f);

    sim.Create(nRows2, nCols2, nBands);
    
    unsigned char *simptr   = sim.data;
    unsigned char *imptr    = im.data;
    unsigned char *imrowptr = im.data;
    int imRowStride = im.rowStride();
    int imRowStride2 = imRowStride * 2;

    for (int i = 0; i < nRows2; i++)
    {
        imptr = imrowptr;

        for (int j = 0; j < nCols2; j++)
        {
            for (int b = 0; b < nBands; b++)
            {
                unsigned char *imptrtmp;
                imptrtmp = imptr;
                int avg = 0;
                
                avg += *imptrtmp;
                imptrtmp += nBands;
                avg += *imptrtmp;
                imptrtmp += imRowStride;
                avg += *imptrtmp;
                imptrtmp -= nBands;
                avg += *imptrtmp;
                
                *simptr = (unsigned char)(avg / 4);
              
                simptr++;
                imptr++;
            }
            
            imptr += nBands;
	    
        }// end for j

        imrowptr += imRowStride2;
    }// end for i
    
}
