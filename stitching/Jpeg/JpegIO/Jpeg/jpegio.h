// jpegio.h

#pragma once

void ReadJPEG0(unsigned char* &data, int &nRows, int &nCols, int &nBands, const char* filename);
void WriteJPEG0(unsigned char* data, int nRows, int nCols, int nBands, int quality, const char* filename);
