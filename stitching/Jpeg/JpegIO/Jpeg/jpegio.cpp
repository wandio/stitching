// jpegio.cpp

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#import "jpeglib.h"

using namespace std;

void ReadJPEG0(unsigned char* &data, int &nRows, int &nCols, int &nBands, const char* filename)
{
    FILE* fileptr = fopen(filename, "rb");
  
    if (fileptr == NULL)
    {
        cerr << "ReadJPEG -- failed to open input file" << endl;
        return;
    }

    struct jpeg_decompress_struct decompInfo;

    struct jpeg_error_mgr err;
    decompInfo.err = jpeg_std_error(&err);

    jpeg_create_decompress(&decompInfo);
    jpeg_stdio_src(&decompInfo, fileptr);
  
    jpeg_read_header(&decompInfo, true);
    jpeg_start_decompress(&decompInfo);

    nCols = decompInfo.output_width;
    nRows = decompInfo.output_height;
    nBands = decompInfo.output_components;
  
    data = new unsigned char[nRows * nCols * nBands];

    unsigned char *rowPtr = data;
    int rowStride = nCols * nBands;

    for (int i = 0; i < nRows; i++)
    {
        jpeg_read_scanlines(&decompInfo, &rowPtr, 1);
        rowPtr += rowStride;
    }
  
    jpeg_finish_decompress(&decompInfo);
    jpeg_destroy_decompress(&decompInfo);
  
    fclose(fileptr);
}

void WriteJPEG0(unsigned char* data, int nRows, int nCols, int nBands, int quality, const char* filename)
{
        
    FILE* fileptr = fopen(filename, "wb");
    if (fileptr == NULL)
    {
        cerr << "WriteJPEG -- failed to open output file\n";
        return;
    }
  
    struct jpeg_compress_struct compInfo;
    
    struct jpeg_error_mgr err;
    compInfo.err = jpeg_std_error(&err);
    
    jpeg_create_compress(&compInfo);    
    jpeg_stdio_dest(&compInfo, fileptr);

    compInfo.image_width = nCols;
    compInfo.image_height = nRows;
    compInfo.input_components = nBands;

    if (nBands == 1)
        compInfo.in_color_space = JCS_GRAYSCALE;
    else if (nBands == 3)
        compInfo.in_color_space = JCS_RGB;
    else
    {
        cerr << "WriteJPEG -- image must have 1 or 3 bands (nBands = " << nBands << ")\n";
        return;
    }
    
    jpeg_set_defaults(&compInfo);
    jpeg_set_quality(&compInfo, quality, true);
    jpeg_start_compress(&compInfo, true);

    unsigned char* rowPtr = data;
    int rowStride = nCols * nBands;
    for (int i = 0; i < nRows; i++)
    {
        jpeg_write_scanlines(&compInfo, &rowPtr, 1);
        rowPtr += rowStride;
    }

    jpeg_finish_compress(&compInfo);
    fclose(fileptr);

    jpeg_destroy_compress(&compInfo);        
}
