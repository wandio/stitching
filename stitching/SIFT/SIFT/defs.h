// defs.h

// COMMERCIAL version

/* Copyright (c) 2003. David G. Lowe, University of British Columbia.
   This code may be used, distributed, or modified only under license
   from the University of British Columbia.  This notice must be retained
   in all copies.
*/

#ifndef _DEFS_H
#define _DEFS_H

#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <stdio.h>

/*--------------------------- Constants and macros -------------------------*/

/* These constants specify the size of the index vectors that provide
   a descriptor for each keypoint.  The region around the keypoint is
   sampled at OriSize orientations with IndexSize by IndexSize bins.
   VecLength is the length of the resulting decriptor vector.
*/
//#define OriSize 4
//#define IndexSize 3
#define OriSize 8
#define IndexSize 4
#define VecLength (IndexSize * IndexSize * OriSize)

/* Number of neighbors found for each key during indexing and
   verification.  These are used to compare distance of closest
   neighbors to neighbor NNUM-1.  Must be 2 or greater.  It is best
   to use 8 so that up to 7 images of the same object from similar
   viewpoints can be taken without interference.
 */
#define NNUM 8

/* Slight overestimate of PI, so orientations will be in range [0,PI]. */
#define PI 3.1415927

#define MAX(x,y)  (((x) > (y)) ? (x) : (y))
#define MIN(x,y)  (((x) < (y)) ? (x) : (y))
#define ABS(x)    (((x) < 0) ? (-(x)) : (x))

/* Following defines TRUE and FALSE if not previously defined. */
#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

/* Given the name of a structure, NEW allocates space for it and returns
     a pointer to the structure.
*/
#define NEW(s,pool) ((struct s *) MallocPool(sizeof(struct s),pool))


/* Following defines the maximum number of different storage pools. */
#define POOLNUM 10

/* Each of the following assigns a unique number less than POOLNUM to
   a particular pool of storage needed for this application. 
*/
#define PERM_POOL  0   /* Permanent storage that is never released. */
#define IMAGE_POOL 1   /* Data used only for the current image. */
#define MODEL_POOL 2   /* Data for current set of object models. */
#define TREE_POOL  3   /* Nodes for current k-d tree.. */
#define KEY_POOL   4   /* Keypoints */

/*------------------------------- Externals -----------------------------*/

extern const int FirstOnly, SampleSize, IntVec, LongDebugLog;
extern int OutputAllKeys, SkipLevels, TotalMatches, TotalKeys;
extern float Bright, Contrast, Rotation, Scale, Stretch, Noise;
extern const double IntVecFactor, MagFactor;

/*------------------------------- Structures ----------------------------*/

typedef struct ImageSt {
    int rows, cols;          /* Dimensions of image. */
    float **pixels;          /* 2D array of image pixels. */
} *Image;

#endif 
