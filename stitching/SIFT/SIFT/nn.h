// nn.h

// COMMERCIAL version

/* Copyright (c) 2003. David G. Lowe, University of British Columbia.
   This code may be used, distributed, or modified only under license
   from the University of British Columbia.  This notice must be retained
   in all copies.
*/

#ifndef _NN_H
#define _NN_H

#include "key.h"

void BuildAccessTree(Keypoint keys);
void FindNeighbours(Keypoint *result, int numNN, Keypoint keypoint);

#endif
