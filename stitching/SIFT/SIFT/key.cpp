// key.cpp

// COMMERCIAL version

/* Copyright (c) 2003. David G. Lowe, University of British Columbia.
   This code may be used, distributed, or modified only under license
   from the University of British Columbia.  This notice must be retained
   in all copies.
*/

/* This file contains a fast, commercial quality, version of the code
   to extract invariant keypoints from an image.  The main routine is
   GetKeypoints(image) which returns a list of all invariant features
   from an image.
*/

#include "key.h"
#include <memory.h>
#include <iostream>
#include <vector>
#include <functional>
#include <algorithm>
#include <float.h>
using namespace std;


/* --------------------------- Constants ------------------------------- */


/* Use this variable to select the number of keypoints that are found.
   If SkipLevels is set to 0, then all possible keypoints are
   found.  For values higher than 0, the given number of lowest levels
   of the pyramid are skipped.  Each increase by 1 reduces the number
   of keypoints and cost of computation by about a factor of 2.
*/
int SkipLevels = 2;
//int SkipLevels = 0;

/* Set this to TRUE to find about 75% more keypoints be searching at a
   finer granularity of scale.  This will give more matches, but the
   increase in cost will likely be even higher than the number of
   matches.  It is useful when computation cost is not a dominant
   concern.
*/
int ExtraKeypoints = FALSE;
//int ExtraKeypoints = TRUE;

/* Magnitude of difference-of-Gaussian value at a keypoint must be
   above this threshold.  This avoids considering points with very low
   contrast that are dominated by noise.  A value of 0.02 is useful
   for best efficiency, as it eliminates many unstable keypoints, but
   0.01 finds more matches for low-contrast images.
*/
double PeakThresh = 0.02;   /* Range of 0.005 to 0.02 is good. */

/* This threshold eliminates responses at edges.  The minimum Eigenvalue
   of the Hessian of the DOG function (i.e., minimum principle curvature)
   must be below this threshold.  A value of 0.002 eliminates about 15%
   of the least stable points.  Experiments show that the eliminated
   points have less than half the matching success rate of other points.
*/
double EdgeThresh = 0.0005;  /* Use 0.0005 */

/* Peaks in the DOG function must be at least BorderDist samples away
   from the image border, at whatever sampling is used for that scale.
   Keypoints close to the border (BorderDist < about 12) will have part
   of the descriptor landing outside the image.  The best value is about
   10-12, as any lower value gives extra keypoints that are very unlikely
   to be correctly matched.
*/
const int BorderDist = 12;

/* OriBins gives the number of bins in the orientation histogram (36
   gives 10 degree spacing of bins).
*/
#define OriBins 36

/* Size of Gaussian used to select orientations as multiple of scale
     of smaller Gaussian in DOG function used to find keypoint.
*/
const double OriSigma = 2.0;  /* Use 2.0 */

/* All local peaks in the orientation histogram are used to generate
   keypoints, as long as the local peak is within OriHistThresh of the
   maximum peak.  A value of 1.0 only selects a single orientation at
   each location.  A good value is 0.8. 
*/
double OriHistThresh = 0.8;

/* This constant specifies how large a region is covered by each index
   vector bin.  It gives the spacing of index samples in terms of
   pixels at this scale (which is then multiplied by the scale of a
   keypoint).  It should be set experimentally to as small a value as
   possible to keep features local (good values are 3 when IndexSize is
   4, and 4.0 when IndexSize is 3).
*/
const double MagFactor = 4.0;

/* Width of Gaussian weighting window for index vector values.  It is
   given relative to half-width of index, so value of 1.0 means that
   weight has fallen to about half near corners of index patch.  A
   value of 0.8 works somewhat better than large values (which are
   equivalent to not using weighting).
*/
const double IndexSigma = 0.8;

/* Multiplicative factor for converting descriptor values from float
   to integer byte representation.  Value of 512 means that almost all
   float values are expected to be less than 0.5 (higher values are
   thresholded).
*/
const double IntVecFactor = 512.0;  

/* Index values are thresholded at this value so that regions with
   high gradients do not need to match precisely in magnitude and do
   not dominate information from other locations.  Best value should
   be determined experimentally.  Value of 1.0 has no effect.  Value
   of 0.2 is considerably better.
*/
const double MaxIndexVal = 0.2;  /* Use 0.2 */

/* This flag allows quadratic fit to be skipped, so that value of this step
   can be determined.  This stage was missing from original code, but it
   does improve matching correctness by at least 10% as well as accuracy.
*/
const int SkipQuadraticFit = FALSE;

/* It is inefficient to pass an array such as this as a parameter,
   so we use a global declaration instead.
*/
float DescripArray[IndexSize][IndexSize][OriSize];


/* -------------------- Macros for table lookup ------------------------ */
/* For efficiency, we use table lookup to evaluate a number of
   functions.  This is particularly important for atan2, which is very
   expensive in at least some libraries.  The tables are initialized
   by the function InitLookupTables().
*/

/* This is a lookup table for the function exp(x), for x in the range
   [0,-10], in increments of 0.01.  The macro EXP(x) returns the
   closest value in the table.  
*/
float ExpLookupTable[1000] = {0.0};
#define EXP(x)  (ExpLookupTable[(int)(-100.0 * x)])

/* This is a lookup table for the function atan(y/x), for y/x in the range
   [0,1], in increments of 0.01.  The macro ATAN(x) returns the
   closest value in the table.  
*/
float AtanLookupTable[101];
#define ATAN(x)  (AtanLookupTable[(int)(100.0 * x)])
//#define ATAN(x) atan(x)


/* -------------------- Local function prototypes ------------------------ */

void InitLookupTables();
Keypoint FindMaxMin(Image *dog, Image *mag, Image *ori, int levels, int skip);
int LocalMaxMin(float val, Image dog, int row, int col, int level);
int NotOnEdge(Image dog, int r, int c);
Keypoint InterpKeyPoint(Image *dog, int s, int r, int c, Image *mag,
			Image *ori, Keypoint keys);
float FitQuadratic(float offset[3], Image *dogs, int s, int r, int c);
float SampleBilinear(Image im, float r, float c);
Keypoint AssignOriHist(Image mag, Image ori, float levelScale,
       float levelSize, float levelRow, float levelCol, float peakval, Keypoint keys);
void InsertIntoHist(float hist[OriBins], Image mag, Image ori,
		    int r, int c, float distsq, float sigma);
void SmoothHistogram(float *hist, int bins);
float InterpPeak(float a, float b, float c);
Keypoint MakeKeypoint(Image mag, Image ori, float levelScale, float levelSize,
		 float levelRow, float levelCol, float angle, float peakval, Keypoint keys);
Image DoubleSize(Image image);
Image HalfImageSize(Image image);
void SubtractImage(Image im1, Image im2);
Image ReduceSize(Image image);
void GradOriImages(Image im, Image grad, Image ori);
void GaussianBlur(Image image, int sig);
void ConvHorizontal(Image image, int sig, int ksize);
void ConvVertical(Image image, int sig, int ksize);
void ConvBuffer10(float *bp, int rsize);
void ConvBuffer14(float *bp, int rsize);
void ConvBuffer17(float *bp, int rsize);
void MakeKeypointSample(Keypoint key, Image grad, Image ori, float scale,
			float row, float col);
void NormalizeVec(float *vec, int len);
void KeySampleVec(float *vec, Keypoint key, Image grad, Image ori,
		  float scale, float row, float col);
void KeySample(Keypoint key, Image mag, Image ori, float scale,
	       float row, float col);
void AddSample(Keypoint k, Image mag, Image orim, int r, int c,
	       float rpos, float cpos, float rx, float cx, float invsig2);
void PlaceInIndex(float mag, float ori, float rx, float cx);


/* ----------------------------- Routines --------------------------------- */

/* Given an image, find the keypoints and return a pointer to a list
   of keypoint records.  This routine builds image pyramids of
   difference-of-Gaussian values in dog, and magnitude and orientation
   values in mag and ori.
*/

Keypoint GetKeypoints(Image image, int SkipLevels0)
{
	SkipLevels = SkipLevels0;

	int level = 0, skip;
    Image dog[20], mag[20], ori[20], blur;

    if (ExpLookupTable[0] == 0.0)
      InitLookupTables();

    /* To find the maximum number of keypoints, the image size must
       be double that of the input to sample function more frequently.
       However, if SkipLevels >= 2, then we don't need to double the
       image as lower levels will not be processed.
    */
    if (SkipLevels < 2)
      image = DoubleSize(image);
    else
      /* Use copy of image so it can still be displayed unchanged. */
      image = CopyImage(image, IMAGE_POOL);
    skip = (SkipLevels < 2 ? SkipLevels : SkipLevels - 2);
   
    /* Apply initial smoothing to input image.  Sigma is 1.414 if
       ExtraKeypoints flag is set, otherwise 1.732.
    */
    GaussianBlur(image, ExtraKeypoints ? 1 : 2);

    /* Create each level of pyramids.  Keep reducing image by factors of 1.5
       until one dimension is smaller than minimum size.
    */
    while (image->rows > 2 * BorderDist + 2 &&
	   image->cols > 2 * BorderDist + 2 && level < 20) {

      /* Create smoothed version of image.  We use a sigma of 1.4
         unless ExtraKeypoints is TRUE, in which case sigma is 1.0.
         ExtraKeypoints finds more keypoints due to less smoothing
         between levels.
      */
      blur = CopyImage(image, IMAGE_POOL);
      GaussianBlur(blur, ExtraKeypoints ? 0 : 1);

      if (level >= skip) 
	  {
		/* Subtract blurred image for DOG function. */
		SubtractImage(image, blur);
		dog[level] = image;

		/* Find image gradients and orientations.  These are not needed
	    for lowest level of pyramid. */
	    mag[level] = CreateImage(image->rows, image->cols, IMAGE_POOL);
		ori[level] = CreateImage(image->rows, image->cols, IMAGE_POOL);
		GradOriImages(blur, mag[level], ori[level]);
	  }
      
	  /* Generate next level of pyramid by reducing by factor of 1.5. */
      image = ReduceSize(blur); 
      level++;
    }
    
    /* Now search for peaks in the pyramid structures. */
    return FindMaxMin(dog, mag, ori, level, skip);

}


/* Initialize the lookup tables for the EXP(x) and ATAN(x) functions.
*/
void InitLookupTables()
{
    int i;

    for (i = 0; i < 1000; i++)
      ExpLookupTable[i] = (float)(exp((i + 0.5f) / -100.0f));
    for (i = 0; i < 101; i++)
      AtanLookupTable[i] = (float)(atan2((i + 0.5f) / 100.0f, 1.0f));
}


/* Find the local maxima and minima of the DOG images.  This requires
     checking each pixel in the pyramid against its neighbors.
*/
Keypoint FindMaxMin(Image *dog, Image *mag, Image *ori, int levels, int skip)
{
   int i, r, c, rows, cols;
   float val, **pix;
   Keypoint keys = NULL;
   
   /* Search through each scale, leaving 1 scale below and 1 above. */
   for (i = skip + 1; i < levels - 1; i++) 
   {
	  rows = dog[i]->rows;
      cols = dog[i]->cols;
      pix = dog[i]->pixels;   /* Pointer to pixels for this scale. */
      
	  /* Only find peaks at least BorderDist pixels from borders. */
      for (r = BorderDist; r < rows - BorderDist; r++)
	  {
         for (c = BorderDist; c < cols - BorderDist; c++) 
		 {
            val = pix[r][c];

	       /* DOG magnitude must be above 0.8 * PeakThresh threshold
	       (final precise threshold check will be done once peak
	       interpolation is performed).  Then check whether this
	       point is a peak in 3x3 region at each level, and is not
	       on an elongated edge.
	       */
            if (fabs(val) > 0.8 * PeakThresh  &&
				LocalMaxMin(val, dog[i], r, c, 0) &&
				LocalMaxMin(val, dog[i-1], r, c, -1) &&
				LocalMaxMin(val, dog[i+1], r, c, 1) && 
				NotOnEdge(dog[i], r, c))
			{
				keys = InterpKeyPoint(dog, i, r, c, mag, ori, keys);
			}
				
	     }// end for c
	  }// end for r
   }// end for i

   return keys;
}

/* Check whether this pixel is a local maximum (positive value) or
   minimum (negative value) compared to the 3x3 neighbourhood that
   is centered at (row,col).  If level is -1 (or 1), then (row,col) must
   be scaled down (or up) a level by factor 1.5.
*/
int LocalMaxMin(float val, Image dog, int row, int col, int level)
{
    int r, c;
    float **pix;

    pix = dog->pixels;
    
    /* Calculate coordinates for image one level down (or up) in pyramid. */
    if (level < 0) {
       row = (3 * row + 1) / 2;
       col = (3 * col + 1) / 2;
    } else if (level > 0) {
       row = (2 * row) / 3;
       col = (2 * col) / 3;
    }
    
    if (val > 0.0) {
       for (r = row - 1; r <= row + 1; r++)
          for (c = col - 1; c <= col + 1; c++)
             if (pix[r][c] > val)
                return FALSE;
    } else {
       for (r = row - 1; r <= row + 1; r++)
          for (c = col - 1; c <= col + 1; c++)
             if (pix[r][c] < val)
                return FALSE;
    }
    return TRUE;
}


/* Returns FALSE if this point on the DOG function lies on an edge, which
   means it has a minimum eigenvalue (principle curvature) near 0.
   This test is done early because it is very efficient and eliminates
   many points. 
     Matrix = |A B|
              |C D|
     t = trace/2 = (A + D) / 2
     s = sqrt(t^2 - det) = sqrt(t^2 - (AD - BC)) = sqrt(((A - D)/2)^2 + BC)
         [shows that s is positive for symmetric matrix, where B = C]
     One eigenvalue = t + s
     Other eigenvalue = t - s
*/
int NotOnEdge(Image dog, int r, int c)
{
    float H00, H11, H01, t, s, eigen1, eigen2, minEigen;
    float **d = dog->pixels;

    /* Compute 2x2 Hessian values from pixel differences. */
    H00 = d[r-1][c] - 2.0f * d[r][c] + d[r+1][c];
    H11 = d[r][c-1] - 2.0f * d[r][c] + d[r][c+1];
    H01 = ((d[r+1][c+1] - d[r+1][c-1]) - (d[r-1][c+1] - d[r-1][c-1])) / 4.0f;

    t = 0.5f * (H00 + H11);
    s = (float)(sqrt(t*t - (H00 * H11 - H01 * H01)));

    eigen1 = t + s;
    eigen2 = t - s;
    eigen1 = ABS(eigen1);
    eigen2 = ABS(eigen2);
    minEigen = MIN(eigen1, eigen2);

    return (minEigen > EdgeThresh);
}


/* Create a keypoint at a peak near scale space location (s,r,c), where
   s is scale (index of DOG image), and (r,c) is (row, col) location at
   this level.   Return the list of keys with any new keys added.
*/
Keypoint InterpKeyPoint(Image *dog, int s, int r, int c, Image *mag,
   Image *ori, Keypoint keys)
{
    float offset[3], peakval, levelRow, levelCol, levelScale, levelSize;

    if (SkipQuadraticFit) {
      if (fabs(dog[s]->pixels[r][c]) < PeakThresh)
	return keys;
      levelRow = (float)r;
      levelCol = (float)c;
      levelScale = (float) 1.7;

    } else {
      /* Fit quadratic to determine offset and peak value. */
      peakval = FitQuadratic(offset, dog, s, r, c);

      /* Do not create a keypoint if interpolation is far outside
	 expected limits, or if magnitude of peak value is below
	 threshold (i.e., contrast is too low).  
      */
      if (fabs(offset[0]) > 1.5  ||  fabs(offset[1]) > 1.5  ||
	  fabs(offset[2]) > 1.5  ||  fabs(peakval) < PeakThresh)
	return keys;

      /* Experiments show that less than 1% of points have offset[0]
	 greater than 0.5, so there is no need to worry about shifting
	 levels in the pyramid.
      */
      levelRow = r + offset[1];
      levelCol = c + offset[2];

      /* Compute interpolated scale relative to this pyramid level.  The
	 scale units are in terms of sigma for the smallest of the
	 Gaussians in the DOG used for this level.  The initial factor
	 1.7 was very important for performance compatibility with
	 octave version parameters.
      */
      levelScale = 1.7f * (float)pow(1.5f, offset[0]);
    }

    /* Compute size of pixels at this level relative to input image. */
    levelSize = (float)pow(1.5f, s) * ((SkipLevels < 2) ? 0.5f : 1.0f);

    return AssignOriHist(mag[s], ori[s], levelScale, levelSize,
			 levelRow, levelCol, peakval, keys);
}


/* Macro that computes a floating-point coordinate one level up or
   down in the pyramid. 
*/
#define UP(c)    ((4.0f * (c) - 1.0f) / 6.0f)
#define DOWN(c)  (1.5f * (c) + 0.25f)


/* Apply the method developed by Matthew Brown (see BMCV 02 paper) to
   fit a 3D quadratic function through the DOG function values around
   the location (s,r,c), i.e., (scale,row,col), at which a peak has
   been detected.  Return the interpolated peak position as a vector
   in "offset", which gives offset from position (s,r,c).  The
   returned value is the interpolated DOG magnitude at this peak.
*/
float FitQuadratic(float offset[3], Image *dog, int s, int r, int c)
{
    float g[3], **mid, up00, up10, up_10, up01, up0_1,
      down00, down10, down_10, down01, down0_1;
    static float **H = NULL;

    /* First time through, allocate space for Hessian matrix, H. */
    if (H == NULL)
      H = AllocMatrix(3, 3, PERM_POOL);

    mid = dog[s]->pixels;

    /* Sample the dog function at one scale up and down at offsets needed
       for the Hessian.  We need to use bilinear interpolation as pixels
       are not aligned at the same positions.
    */
    up00 = SampleBilinear(dog[s+1], UP(r), UP(c));
    up10 = SampleBilinear(dog[s+1], UP(r+1), UP(c));
    up_10 = SampleBilinear(dog[s+1], UP(r-1), UP(c));
    up01 = SampleBilinear(dog[s+1], UP(r), UP(c+1));
    up0_1 = SampleBilinear(dog[s+1], UP(r), UP(c-1));
    down00 = SampleBilinear(dog[s-1], DOWN(r), DOWN(c));
    down10 = SampleBilinear(dog[s-1], DOWN(r+1), DOWN(c));
    down_10 = SampleBilinear(dog[s-1], DOWN(r-1), DOWN(c));
    down01 = SampleBilinear(dog[s-1], DOWN(r), DOWN(c+1));
    down0_1 = SampleBilinear(dog[s-1], DOWN(r), DOWN(c-1));

    /* Fill in the values of the gradient from pixel differences. */
    g[0] = (up00 - down00) / 2.0f;
    g[1] = (mid[r+1][c] - mid[r-1][c]) / 2.0f;
    g[2] = (mid[r][c+1] - mid[r][c-1]) / 2.0f;

    /* Fill in the values of the Hessian from pixel differences. */
    H[0][0] = down00 - 2.0f * mid[r][c] + up00;
    H[1][1] = mid[r-1][c] - 2.0f * mid[r][c] + mid[r+1][c];
    H[2][2] = mid[r][c-1] - 2.0f * mid[r][c] + mid[r][c+1];
    H[0][1] = H[1][0] = ((up10 - up_10) - (down10 - down_10)) / 4.0f;
    H[0][2] = H[2][0] = ((up01 - up0_1) - (down01 - down0_1)) / 4.0f;
    H[1][2] = H[2][1] = ((mid[r+1][c+1] - mid[r+1][c-1]) -
			 (mid[r-1][c+1] - mid[r-1][c-1])) / 4.0f;

    /* Solve the 3x3 linear sytem, Hx = -g. Result, x, gives peak offset.
       Note that SolveLinearSystem destroys contents of H. */
    offset[0] = - g[0];
    offset[1] = - g[1];
    offset[2] = - g[2];
    SolveLinearSystem(offset, H, 3); 

    /* Also return value of DOG at peak location using initial value plus
       0.5 times linear interpolation with gradient to peak position
       (this is correct for a quadratic approximation).  
    */
    return (mid[r][c] + 0.5f * DotProd(offset, g, 3));
}


/* Sample the image at the given non-integer (r,c) position using
   bilinear resampling.
*/
float SampleBilinear(Image im, float r, float c)
{
    int ir, ic;
    float wr, wc, **pix;

    ir = (int) r;                 /* Round down to integer pixel. */
    wr = 1.0f - (r - (float) ir);  /* Weight for this pixel. */
    ic = (int) c;
    wc = 1.0f - (c - (float) ic);
    pix = im->pixels;

    /* We should not be finding keypoints right at image border, or
       else BorderDist should be increased. */
    assert(ir >= 0 && ic >= 0 && ir + 1 < im->rows && ic + 1 < im->cols);

    /* Blinear interpolation between 4 closest pixels. */
    return (float)(wr * wc * pix[ir][ic] + (1.0 - wr) * wc * pix[ir+1][ic] +
	    wr * (1.0f - wc) * pix[ir][ic+1] + 
	    (1.0f - wr) * (1.0f - wc) * pix[ir+1][ic+1]);
}
    

/* Assign one or more orientations to this keypoint location.
     Orientation assignment is done by creating a Gaussian weighted
     histogram of the gradient directions in the region.  The
     histogram is smoothed and the largest peak selected.  The results
     are in the range of -PI to PI.  Return keys with any new
     keypoints added.
*/
Keypoint AssignOriHist(Image mag, Image ori, float levelScale,
       float levelSize, float levelRow, float levelCol, float peakval, Keypoint keys)
{
   int i, r, c, irow, icol, initr, initc, rows, cols, radius, prev, next;
   float hist[OriBins], distsq, angle, sigma, interp, drow, dcol, maxval = 0.0;
   
   for (i = 0; i < OriBins; i++)
      hist[i] = 0.0;
   irow = (int) (levelRow + 0.5);
   icol = (int) (levelCol + 0.5);
   rows = mag->rows;
   cols = mag->cols;
   
   /* Look at pixels within 2.5 sigma around the point and sum their
      Gaussian weighted gradient magnitudes into the histogram.
      Only odd pixels are sampled for efficiency (gives only slight
      loss in matching performance).
   */
   sigma = (float)(OriSigma * levelScale);
   radius = (int) (sigma * 2.5f);
   initr = irow - radius;
   if (initr % 2 == 0)
     initr++;
   initc = icol - radius;
   if (initc % 2 == 0)
     initc++;
   for (r = initr; r <= irow + radius; r += 2)
     for (c = initc; c <= icol + radius; c += 2) {
       drow = r - levelRow;
       dcol = c - levelCol;
       distsq = drow * drow + dcol * dcol;

       /* Only use pixels within circle of given radius and that
	  are within image boundary. */
       if (distsq < radius * radius  && 
	   r > 0 && c > 0 && r < rows - 2 && c < cols - 2)
	 InsertIntoHist(hist, mag, ori, r, c, distsq, sigma);
     }
   SmoothHistogram(hist, OriBins);

   /* Find maximum value in histogram. */
   for (i = 0; i < OriBins; i++)
      if (hist[i] > maxval)
         maxval = hist[i];

   /* Look for each local peak in histogram.  If value is within
      OriHistThresh of maximum value, then generate a keypoint.
   */
   for (i = 0; i < OriBins; i++) {
     prev = (i == 0 ? OriBins - 1 : i - 1);
     next = (i == OriBins - 1 ? 0 : i + 1);
     if (hist[i] > hist[prev]  &&  hist[i] >= hist[next]  &&
	 hist[i] >= OriHistThresh * maxval) {
       
       /* Use parabolic fit to interpolate peak location from 3 samples.
	  Set angle in range -PI to PI. */
       interp = InterpPeak(hist[prev], hist[i], hist[next]);
       angle = (float)(2.0f * PI * (i + interp) / OriBins - PI);
       if (angle < -PI)
	 angle += (float) (2.0 * PI - 0.0001);
       assert(angle >= -PI  &&  angle < PI);
       
       /* Create a keypoint with this orientation. */
       keys = MakeKeypoint(mag, ori, levelScale, levelSize, levelRow, levelCol,
			   angle, peakval, keys);
     }
   }
   return keys;
}


/* Given a pyramid sample at location (r,c), insert its gradient
    magnitude into the orientation histogram using linear
    interpolation between the two closest bins.
*/
void InsertIntoHist(float hist[OriBins], Image mag, Image ori,
		    int r, int c, float distsq, float sigma)
{
   int bin, nextbin;
   float weight, binloc, frac, gval, expval;

   gval = mag->pixels[r][c];
   expval = - distsq / (2.0f * sigma * sigma);
   assert(expval > -10.0f && expval <= 0.0f);
   weight = EXP(expval);

   /* Ori is in range of -PI to PI.  Map to bin location. */
   binloc = (float)(OriBins * (ori->pixels[r][c] + PI + 0.0001f) / (2.0f * PI));
   bin = (int) binloc;
   frac = binloc - bin;  /* Fractional distance to next bin. */
   assert(bin >= 0 && bin <= OriBins);
   if (bin == OriBins)
     bin = 0;
   nextbin = (bin == OriBins - 1) ? 0 : bin + 1;
   hist[bin] += (1.0f - frac) * weight * gval;
   hist[nextbin] += frac * weight * gval;
}


/* Smooth a histogram by using repeated application of a [1/4 1/2 1/4]
   kernel.  Assume the histogram is connected in a circular buffer.
*/
void SmoothHistogram(float *hist, int bins)
{
   int i, c;
   float prev, first, temp;
   
   /* Smoothing 6 times was found to give most reliable peak. */
   for (c = 0; c < 6; c++) {
     prev = hist[bins - 1];
     first = hist[0];   /* Remember original first value. */
     for (i = 0; i < bins; i++) {
       temp = hist[i];
       hist[i] = (prev + 2.0f * hist[i] + ((i + 1 == bins) ? first : hist[i+1]))
	         / 4.0f;
       prev = temp;
     }
   }
}


/* Return a number in the range [-0.5, 0.5] that represents the
   location of the peak of a parabola passing through the 3 evenly
   spaced samples.  The center value is assumed to be greater than or
   equal to the other values if positive, or less than if negative.
*/
float InterpPeak(float a, float b, float c)
{
    if (b < 0.0) {
	a = -a; b = -b; c = -c;
    }
    assert(b >= a  &&  b >= c);
    return 0.5f * (a - c) / (a - 2.0f * b + c);
}


/* Create a new keypoint and return list of keypoints with new one added.
*/
Keypoint MakeKeypoint(Image mag, Image ori, float levelScale, float levelSize,
	      float levelRow, float levelCol, float angle, float peakval, Keypoint keys)
{
    Keypoint k;

    k = NEW(KeypointSt, KEY_POOL);
    k->next = keys;
    keys = k;
    k->ori = angle;
    k->row = levelSize * (levelRow + 0.5) - 0.5;
    k->col = levelSize * (levelCol + 0.5) - 0.5;
    k->scale = levelSize * levelScale;
	k->strength = peakval;
    MakeKeypointSample(k, mag, ori, levelScale, levelRow, levelCol);
    return keys;
}


/*----------------------- Image utility routines ----------------------*/

/* Double image size. Use linear interpolation between closest pixels.
   Size is two rows and columns short of double to simplify interpolation.
*/
Image DoubleSize(Image image)
{
   int rows, cols, nrows, ncols, r, c, r2, c2;
   float **im, **newim;
   Image newimage;
   
   rows = image->rows;
   cols = image->cols;
   nrows = 2 * rows - 2;
   ncols = 2 * cols - 2;
   newimage = CreateImage(nrows, ncols, IMAGE_POOL);
   im = image->pixels;
   newim = newimage->pixels;
   
   for (r = 0; r < rows - 1; r++)
      for (c = 0; c < cols - 1; c++) {
         r2 = 2 * r;
         c2 = 2 * c;
         newim[r2][c2] = im[r][c];
         newim[r2+1][c2] = 0.5f * (im[r][c] + im[r+1][c]);
         newim[r2][c2+1] = 0.5f * (im[r][c] + im[r][c+1]);
         newim[r2+1][c2+1] = 0.25f * (im[r][c] + im[r+1][c] + im[r][c+1] +
            im[r+1][c+1]);
      }
   return newimage;
}


/* Reduce the size of the image by half by selecting alternate pixels on
   every row and column.  We assume image has already been blurred
   enough to avoid aliasing.
*/
Image HalfImageSize(Image image)
{
   int rows, cols, nrows, ncols, r, c, ri, ci;
   float **im, **newim;
   Image newimage;
   
   rows = image->rows;
   cols = image->cols;
   nrows = rows / 2;
   ncols = cols / 2;
   newimage = CreateImage(nrows, ncols, IMAGE_POOL);
   im = image->pixels;
   newim = newimage->pixels;
   
   for (r = 0, ri = 0; r < nrows; r++, ri += 2)
      for (c = 0, ci = 0; c < ncols; c++, ci += 2)
	newim[r][c] = im[ri][ci];
   return newimage;
}


/* Subtract image im2 from im1 and leave result in im1.
*/
void SubtractImage(Image im1, Image im2)
{
   float **pix1, **pix2;
   int r, c;
   
   pix1 = im1->pixels;
   pix2 = im2->pixels;
   
   for (r = 0; r < im1->rows; r++)
      for (c = 0; c < im1->cols; c++)
	pix1[r][c] -= pix2[r][c];
}


/* Reduce the size of the image by resampling with a pixel spacing of
   1.5 times original spacing.  Each block of 9 original pixels is
   replaced by 4 new pixels resampled with bilinear interpolation.
*/
Image ReduceSize(Image image)
{
   int nrows, ncols, r, c, r1, r2, c1, c2;
   float **im, **newPixels;
   Image newimage;
   
   nrows = (2 * image->rows) / 3;
   ncols = (2 * image->cols) / 3;
   newimage = CreateImage(nrows, ncols, IMAGE_POOL);
   im = image->pixels;
   newPixels = newimage->pixels;
   
   for (r = 0; r < nrows; r++)
      for (c = 0; c < ncols; c++) {
         if (r % 2 == 0) {
            r1 = (r >> 1) * 3;
            r2 = r1 + 1;
         } else {
            r1 = (r >> 1) * 3 + 2;
            r2 = r1 - 1;
         }      
         if (c % 2 == 0) {
            c1 = (c >> 1) * 3;
            c2 = c1 + 1;
         } else {
            c1 = (c >> 1) * 3 + 2;
            c2 = c1 - 1;
         }      
	 newPixels[r][c] = 0.5625f * im[r1][c1] +
                    0.1875f * (im[r2][c1] + im[r1][c2]) + 0.0625f * im[r2][c2];
      }
      
   return newimage;
}


/* Given a smoothed image, im, return image gradient and orientation
     at each odd pixel in grad and ori.  For efficiency, we fill in
     only the odd pixel locations.
*/
void GradOriImages(Image im, Image grad, Image ori)
{
    float xgrad, ygrad, **pix, **gradpix, **oripix, ang;
    int rows, cols, r, c;
   
    rows = im->rows;
    cols = im->cols;
    pix = im->pixels;
    gradpix = grad->pixels;
    oripix = ori->pixels;

    /* For efficiency, we fill in only the odd pixel locations.
    */
    for (r = 1; r < rows-1; r += 2)
      for (c = 1; c < cols-1; c += 2) {
	xgrad = pix[r][c+1] - pix[r][c-1];
	ygrad = pix[r-1][c] - pix[r+1][c];
	gradpix[r][c] = (float)sqrt(xgrad * xgrad + ygrad * ygrad);

	/* Compute atan efficiently, as this is very expensive using
	   the built in atan2 function.  First determine quadrant, and
	   then call lookup table with ATAN macro.  Compute angle in
	   the range -PI to PI.
	*/
	if (ygrad > 0.0) {
	  if (xgrad > 0.0) {
	    if (xgrad > ygrad)
	      ang = ATAN(ygrad / xgrad);
	    else
			ang = (float)(0.5f * PI - ATAN(xgrad / ygrad));
	  } else {
	    if (- xgrad > ygrad)
	      ang = (float)(PI - ATAN(- ygrad / xgrad));
	    else
			ang = (float)(0.5 * PI + ATAN(- xgrad / ygrad));
		}
	} else {
	  if (xgrad > 0.0) {
	    if (xgrad > - ygrad)
	      ang = - ATAN(- ygrad / xgrad);
	    else
	      ang = (float)(- 0.5f * PI + ATAN(- xgrad / ygrad));
		} else {
	    if (xgrad == 0.0  &&  ygrad == 0.0)
	      ang = 0.0;
	    else if (- xgrad > - ygrad)
	      ang = (float)(- PI + ATAN(ygrad / xgrad));
	    else	     
			ang = (float)(- 0.5 * PI - ATAN(xgrad / ygrad));
	  }
	}	    
	
	oripix[r][c] = ang;
      }
}



/* --------------------------- Blur image --------------------------- */

/* Convolve image with a Gaussian.  For efficiency, this handles only
   three values of sigma, depending on the parameter sig:
     if sig = 0, then sigma = 1
     if sig = 1, then sigma = 1.414 = sqrt(2)
     if sig = 2, then sigma = 1.732 = sqrt(3)
*/
void GaussianBlur(Image image, int sig)
{
    int ksize;

    /* Size of kernel depends on sigma chosen. */
    ksize = (sig == 0 ? 5 : (sig == 1 ? 7 : 9));

    ConvHorizontal(image, sig, ksize);
    ConvVertical(image, sig, ksize);
}


/* Convolve image with Gaussian along image rows.  Pixels outside the
   image are set to the value of the closest image pixel.
*/
void ConvHorizontal(Image image, int sig, int ksize)
{
    int rows, cols, r, i, halfsize;
    float **pixels, val;
    
    //float buffer[4000];
    float buffer[10000];
    
    halfsize = ksize / 2;
    rows = image->rows;
    cols = image->cols;
    pixels = image->pixels;
    
    //assert(cols + ksize < 4000);
    assert(cols + ksize < 10000);

    for (r = 0; r < rows; r++) {
	/* Copy the row into buffer with pixels at ends replicated for
	   half the mask size.  This avoids need to check for ends
	   within inner loop. */
        val = pixels[r][0];
	for (i = 0; i < halfsize; i++)
	    buffer[i] = val;
	memcpy(& buffer[halfsize], pixels[r], cols * sizeof(float));
	val = pixels[r][cols - 1];
	for (i = 0; i < halfsize; i++)
	    buffer[halfsize + cols + i] = val;

	if (sig == 0)
	  ConvBuffer10(buffer, cols);
	else if (sig == 1)
	  ConvBuffer14(buffer, cols);
	else
	  ConvBuffer17(buffer, cols);

	/* Copy buffer result back to image. */
	memcpy(pixels[r], buffer, cols * sizeof(float));
    }
}


/* Number of buffers used for ConvVertical.  Good value is 8, with
   no clear savings for higher values.
*/
#define VBUFSIZE 8

/* Convolve image with Gaussian along image columns.  It is very
   expensive to traverse down image columns due to cache behavior, and
   this was a signficant part of overall cost.  Therefore, we will
   maintain a set of VBUFSIZE column buffers so that each traverse
   down a column can fill all of them.  Tried loop unrolling, but it
   gave no improvement (so cache is the issue, rather than pointer
   computations).
*/
void ConvVertical(Image image, int sig, int ksize)
{
    int rows, cols, r, c, i, b, bufs, boff, halfsize;
    float **pixels, *buf, *pp, val, val2;
    static float **buffers = NULL;
    static int  maxcols = 0;

    halfsize = ksize / 2;
    rows = image->rows;
    cols = image->cols;
    pixels = image->pixels;

    if (maxcols < rows + ksize) {
      maxcols = MAX(rows + ksize, 2 * maxcols);
      buffers = AllocMatrix(VBUFSIZE, maxcols, PERM_POOL);
    }

    /* Process VBUFSIZE columns at a time for efficient cache behavior. */
    for (c = 0; c < cols; c += VBUFSIZE) {
      bufs = MIN(VBUFSIZE, cols - c);   /* Number of buffers to fill. */

      /* Fill ends of each buffer with closest image pixel value. */
      for (b = 0; b < bufs; b++) {
	val = pixels[0][c + b];
	val2 = pixels[rows - 1][c + b];
	buf = buffers[b];
	for (i = 0; i < halfsize; i++) {
	  buf[i] = val;
	  buf[halfsize + rows + i] = val2;
	}
      }
      
      /* Traverse down an image column filling all buffers. */
      for (r = 0; r < rows; r++) {
	pp = & pixels[r][c];
	boff = r + halfsize;
	for (b = 0; b < bufs; b++)
	  buffers[b][boff] = *pp++;
      }

      /* Convolve each buffer with desired kernel. */
      for (b = 0; b < bufs; b++) {
	if (sig == 0)
	  ConvBuffer10(buffers[b], rows);
	else if (sig == 1)
	  ConvBuffer14(buffers[b], rows);
	else
	  ConvBuffer17(buffers[b], rows);
      }

      /* Copy buffer results back to image. */
      for (r = 0; r < rows; r++) {
	pp = & pixels[r][c];
	for (b = 0; b < bufs; b++)
	  *pp++ = buffers[b][r];
      }
    }
}


/* Perform convolution of the kernel with the buffer, returning the
   result at the beginning of the buffer.  rsize is the size
   of the result, while buffer has size: rsize + ksize - 1.
   This is for sigma = 1.0, with a kernel size of 5.
*/
void ConvBuffer10(float *bp, int rsize)
{
    int i;

    /* Make this inner loop super-efficient. */
    for (i = 0; i < rsize; i++) {
      *bp = 0.06f * bp[0] + 0.24f * bp[1] + 0.4f * bp[2] +
	    0.24f * bp[3] + 0.06f * bp[4];
      bp++;
    }
}


/* Same as ConvBuffer10, but for sigma = 1.4 and kernel size of 7.
*/
void ConvBuffer14(float *bp, int rsize)
{
    int i;

    for (i = 0; i < rsize; i++) {
      *bp = 0.04f * bp[0] + 0.1f * bp[1] + 0.22f * bp[2] + 0.28f * bp[3] +
	      0.22f * bp[4] + 0.1f * bp[5] + 0.04f * bp[6];
      bp++;
    }
}


/* Same as ConvBuffer10, but for sigma = 1.732 and kernel size 9.
*/
void ConvBuffer17(float *bp, int rsize)
{
  int i;

  for (i = 0; i < rsize; i++) {
    *bp = 0.02f * bp[0] + 0.0514f * bp[1] + 0.1183f * bp[2] + 0.1951f * bp[3] +
          0.2305f * bp[4] + 0.1951f * bp[5] + 0.1183f * bp[6] + 0.0514f * bp[7] +
          0.02f * bp[8];
    bp++;
  }
}


/*--------------------- Making image descriptor -------------------------*/

/* Use the parameters of this keypoint to sample the gradient images
     at a set of locations within a circular region around the keypoint.
     The (scale,row,col) values are relative to current octave sampling.
     The resulting vector is stored in the key.
*/
void MakeKeypointSample(Keypoint key, Image mag, Image ori, float scale,
			float row, float col)
{
   int i, intval;
   float vec[VecLength];
   
   /* Produce sample vector. */
   KeySampleVec(vec, key, mag, ori, scale, row, col);
   
   /* Normalize vector.  This should provide illumination invariance
      for planar lambertian surfaces (except for saturation effects).
      Normalization also improves nearest-neighbor metric by
      increasing relative distance for vectors with few features.
   */
   NormalizeVec(vec, VecLength);

   /* Now that normalization has been done, threshold elements of
      index vector to decrease emphasis on large gradient magnitudes.
   */
   for (i = 0; i < VecLength; i++)
     if (vec[i] > MaxIndexVal)
       vec[i] = (float)MaxIndexVal;

   /* This final normalization only makes a slight improvement, and it
      could be left out to allow detection of low-complexity vectors.
   */
   NormalizeVec(vec, VecLength);

   /* Convert float vector to integer. */
   key->ivec = (unsigned char*) 
     MallocPool(VecLength * sizeof(unsigned char), KEY_POOL);
   for (i = 0; i < VecLength; i++) {
     intval = (int) (IntVecFactor * vec[i]);
     assert(intval >= 0);
     key->ivec[i] = (unsigned char) MIN(255, intval);
   }
}


/* Normalize length of vec to 1.0.  Return normalization factor.
*/
void NormalizeVec(float *vec, int len)
{
   int i;
   float val, fac, sqlen = 0.0;

   for (i = 0; i < len; i++) {
     val = vec[i];
     sqlen += val * val;
   }
   fac = 1.0f / (float)sqrt(sqlen);
   for (i = 0; i < len; i++)
     vec[i] *= fac;
}


/* Create a 3D index array into which gradient values are accumulated.
   After filling array, copy values back into vec.
*/
void KeySampleVec(float *vec, Keypoint key, Image mag, Image ori,
		  float scale, float row, float col)
{
   int i, j, k, v;

   /* Initialize descriptor array. */
   for (i = 0; i < IndexSize; i++)
      for (j = 0; j < IndexSize; j++)
         for (k = 0; k < OriSize; k++)
            DescripArray[i][j][k] = 0.0;
    
   KeySample(key, mag, ori, scale, row, col);
      
   /* Unwrap the 3D index values into 1D vec. */
   v = 0;
   for (i = 0; i < IndexSize; i++)
     for (j = 0; j < IndexSize; j++)
       for (k = 0; k < OriSize; k++)
	 vec[v++] = DescripArray[i][j][k];
}



/* Add features to vec obtained from sampling the mag and ori images
   for a particular scale.  Location of key is (scale,row,col) with respect
   to images at this scale.  We examine each pixel within a circular
   region containing the keypoint, and distribute the gradient for that
   pixel into the appropriate bins of the index array.
*/
void KeySample(Keypoint key, Image mag, Image ori, float scale,
	       float row, float col)
{
   int r, c, iradius, irow, icol, initr, initc, rows, cols;
   float spacing, radius, sqradius, ssine, scosine, roff, roffsq, coff,
     rpos, cpos, rx, cx, sigma, invsig2, magval, o, expval, scosr, ssinr,
     *orirow, *magrow;

   irow = (int) (row + 0.5);
   icol = (int) (col + 0.5);
   rows = mag->rows;
   cols = mag->cols;
          
   /* The spacing of index samples in terms of pixels at this scale. */
   spacing = (float)(scale * MagFactor);
   ssine = (float)(sin(key->ori) / spacing);
   scosine = (float)(cos(key->ori) / spacing);

   /* Precompute 1/(2*sigma**2), for sigma of the weighting function
      applied to descriptor window. */
   sigma = (float)(IndexSigma * 0.5f * IndexSize);
   invsig2 = 1.0f / (2.0f * sigma * sigma);

   /* Radius of index sample region must extend close to diagonal corner of
      index patch plus half sample for interpolation.  */
   radius = 1.2f * spacing * (IndexSize + 1) / 2.0f + 0.5f; 
   sqradius = radius * radius;
   iradius = (int) radius;
   initr = irow - iradius;
   if (initr % 2 == 0)
     initr++;
   initc = icol - iradius;
   if (initc % 2 == 0)
     initc++;
          
   /* Examine all points from the gradient image that could lie within
      the index square.  Experiments show that only every 2nd pixel
      needs to be indexed, with less than 1% loss in correct matches.
   */
   for (r = initr; r <= irow + iradius; r += 2) {
     if (r > 0  &&  r < rows - 2) {
       /* Precompute as many values as possible before going to inner
	  loop which is a dominant cost of the full system. */
       roff = r - row;
       roffsq = roff * roff;
       scosr = scosine * roff;
       ssinr = ssine * roff;
       magrow = mag->pixels[r];
       orirow = ori->pixels[r];

       for (c = initc; c <= icol + iradius; c += 2) {
	 coff = c - col;

	 if (c > 0  &&  c < cols - 2  &&  roffsq + coff * coff < sqradius) {
    
	   /* Rotate sample location to make it relative to index
	      orientation.  Divide by spacing to put in index units.
	   */
	   rpos = scosr + ssine * coff;
	   cpos = - ssinr + scosine * coff;
         
	   /* Compute location of sample in terms of real-valued index array
	      coordinates.  Subtract 0.5 so that rx of 1.0 means to put full
	      weight on index[1] (e.g., when rpos is 0 and IndexSize is 3).
	   */
	   rx = rpos + IndexSize / 2.0f - 0.5f;
	   cx = cpos + IndexSize / 2.0f - 0.5f;

	   /* Test whether this sample falls within boundary of index patch. */
	   if (rx > -1.0 && rx < (float) IndexSize  &&
	       cx > -1.0 && cx < (float) IndexSize) {

	     /* Compute Gaussian weight for sample, as function of
		radial distance from center.  */
	     expval = - (rpos * rpos + cpos * cpos) * invsig2;
	     assert(expval > -10.0 && expval <= 0.0);
	     magval = EXP(expval) * magrow[c];

	     /* Subtract keypoint orientation to give relative orientation,
		and put in  range [0, 2*PI]. */
	     o = (float)(orirow[c] - key->ori);
	     if (o > 2*PI)
	       o -= (float) (2*PI);
	     if (o < 0.0)
	       o += (float) (2*PI);
		 assert(o >= 0.0  &&  o <= 2.0001*PI);
	     PlaceInIndex(magval, o, rx, cx);
	   }
	 }
       }
     }
   }
}


/* Increment the appropriate locations in the index to incorporate
   this image sample.  The location of the sample in the index is (rx,cx).
   This routine is a dominant cost for the system, so all loops have been
   unrolled and made as efficient as possible.
*/
void PlaceInIndex(float mag, float ori, float rx, float cx)
{
   int ri, ci, oi, rindex, cindex, oindex1, oindex2;
   float oval, rfrac, cfrac, cfrac1, ofrac, ofrac1, rweight, cweight, *ivec;
   
   oval = (float)(OriSize * ori / (2*PI));
   ri = (int) ((rx >= 0.0) ? rx : rx - 1.0);  /* Round down to next integer. */
   ci = (int) ((cx >= 0.0) ? cx : cx - 1.0);
   oi = (int) oval;
   rfrac = rx - ri;         /* Fractional part of location. */
   cfrac = cx - ci;
   cfrac1 = 1.0f - cfrac;
   ofrac = oval - oi;
   ofrac1 = 1.0f - ofrac;

   /* Precompute orientation indices, with wraparound at 2*PI. */
   oindex1 = (oi >= OriSize ? 0 : oi);
   oindex2 = oindex1 + 1;
   if (oindex2 >= OriSize)
     oindex2 = 0;

   assert(ri >= -1  &&  ri < IndexSize  &&  ci >= -1  &&  ci < IndexSize  &&
	  oval >= 0  &&  oi <= OriSize  &&  rfrac >= 0.0  &&  rfrac <= 1.0);
   
   /* Put appropriate fraction in each of 8 buckets around this point
      in the (row,col,ori) dimensions.
   */
   if (ri >= 0) {
     rweight = mag * (1.0f - rfrac);
     if (ci >= 0) {
       ivec = DescripArray[ri][ci];
       cweight = rweight * cfrac1;
       ivec[oindex1] += cweight * ofrac1;
       ivec[oindex2] += cweight * ofrac;
     }
     cindex = ci + 1;
     if (cindex < IndexSize) {
       ivec = DescripArray[ri][cindex];
       cweight = rweight * cfrac;
       ivec[oindex1] += cweight * ofrac1;
       ivec[oindex2] += cweight * ofrac;
     }
   }
   rindex = ri + 1;
   if (rindex < IndexSize) {
     rweight = mag * rfrac;
     if (ci >= 0) {
       ivec = DescripArray[rindex][ci];
       cweight = rweight * cfrac1;
       ivec[oindex1] += cweight * ofrac1;
       ivec[oindex2] += cweight * ofrac;
     }
     cindex = ci + 1;
     if (cindex < IndexSize) {
       ivec = DescripArray[rindex][cindex];
       cweight = rweight * cfrac;
       ivec[oindex1] += cweight * ofrac1;
       ivec[oindex2] += cweight * ofrac;
     }
   }
}

