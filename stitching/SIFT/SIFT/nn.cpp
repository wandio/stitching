// nn.cpp

// COMMERCIAL version

/* Copyright (c) 2003. David G. Lowe, University of British Columbia.
   This code may be used, distributed, or modified only under license
   from the University of British Columbia.  This notice must be retained
   in all copies.
*/

/*
    This module finds the nearest-neighbours of an exemplar using the
      best-bin-first search of a k-d tree.

    The following routines are the interface to this module:

    BuildAccessTree(Keypoint keys)
       Initialize the k-d tree from the set of keypoints.

    FindNeighbours(Keypoint *result, int numNN, Keypoint key)
       Find numNN nearest neighbours and store in result vector (each
          neighbour has "distsq" field set).
*/

#include "util.h"
#include "key.h"

/*--------------------- Internal Data Structures --------------------------*/

/* This tree is used to efficiently access the set of closest examplars to
   an input.  Each node selects an input feature dimension to subdivide
   into child nodes.  The leaves of the tree have either a single
   examplar or an array of ones with identical inputs. */
typedef struct TreeSt {
    int leaf;                  /* TRUE if this is a leaf node. */
    Keypoint exemplar;         /* Exemplar (if this is a leaf node). */
    struct TreeSt *parent;     /* Parent node in tree (NULL for root). */
    int divfeat;               /* Input used for subdivision. */
    int divval;                /* The value used for subdivision. */
    struct TreeSt *child1, *child2;  /* Child nodes. */
} *Tree;


/* This record represents a branch point when finding neighbours in
   the tree.  It contains a record of the minimum distance to the query
   point, as well as the node and child at which the search resumes.
*/
typedef struct BranchSt {
    Tree node;           /* Tree node at which search resumes */
    int mindistsq;     /* Minimum distance to query for all nodes below. */
} *Branch;


/*--------------------- Internal Global variables -------------------------*/

/* It is too expensive to guarantee finding the closest neighbor, so cut
   off search bins that are more distant than the square root of this
   number times the distance of closest neighbor found so far.  Good
   values are about 0.1 to 0.2.  No effect is given by value of 1.0.
*/
const double SearchDistSq = 0.1;  /* Was 0.1 */

/* Also cut off search after this number of bins have been examined.
   This is set to keep computation limited.  This can achieve good
   results with about 100, although best matching results are much higher.
 */
const int MaxCheck = 100;  /* Was 100 */


/* For best-bin-first search, we must retain a list of all branch points
   waiting to be searched.  To avoid having to do memory allocation during
   each search, we use a preallocated list of these structures.  The
   number is kept below a maximum by deleting the farthest branches, which
   are unlikely to be searched in any case.
*/
#define MaxHeap 512                 /* Should be a power of 2. */
struct BranchSt Heap[MaxHeap + 1];  /* Heap of branch records. */
int HeapLen;                       /* Number of elements in the heap. */

Tree AccessTree = NULL;    /* The K-D tree used to find neighbours. */
int CheckCount;            /* Number of exemplars searched so far. */
int NCount;                /* Number of neighbours stored in result. */
float IntConvFactor;       /* Factor converting int distsq to float. */
int TreeSize;
int MaxDepth;

/* -------------------- Local function prototypes ------------------------ */

void DivideTree(Tree *plinkloc, Tree parent, Keypoint exlist, int depth);
void ChooseDivision(Tree node, Keypoint keylist, int depth);
void Subdivide(Tree node, Keypoint keylist, int divfeat, int divval,
	       int depth);
void SearchLevel(Keypoint *result, int numNN, Keypoint key,
		 unsigned char *ivec, Tree node, int mindistsq);
void HeapInsert(Tree node, int mindistsq);
Branch HeapMin();
void Heapify(int parent);
float DistSquared(Keypoint k1, Keypoint k2);
float IntDistSquared(Keypoint k1, Keypoint k2);
void CheckNeighbour(Keypoint *result, int numNN, Keypoint key, Keypoint nkey);


/*------------------------ Build Access Tree -------------------------------*/

/* Build the k-d tree used to find the N closest neighbours to each input.
*/
void BuildAccessTree(Keypoint keys)
{
    Keypoint ex, exlist;

    /* If previous tree was built, then free its storage. */
    FreeStoragePool(TREE_POOL);
    AccessTree = NULL;
    TreeSize = 0;
    MaxDepth = 0;

    /* Precompute conversion factor for efficiency. */
    IntConvFactor = (float)(1.0f / (IntVecFactor * IntVecFactor));

    /* Link all of the exemplars into a list using the tnext field. */
    exlist = NULL;
    for (ex = keys; ex != NULL; ex = ex->next) {
	ex->tnext = exlist;
	exlist = ex;
    }
    DivideTree(& AccessTree, NULL, exlist, 0);

/*    if (DebugLog)
      fprintf(logFile, "Built search tree with %d nodes. Max depth: %d\n",
      TreeSize, MaxDepth);*/

}


/* Create a tree node that subdivides the given list of exemplars, and
   then call this routine recursively.  Place a pointer to this new node
   in the location plinkloc. 
*/
void DivideTree(Tree *plinkloc, Tree parent, Keypoint exlist, int depth)
{
    Tree node;

    node = NEW(TreeSt, TREE_POOL);
    node->leaf = FALSE;
    node->parent = parent;
    *plinkloc = node;

    /* If only one exemplar remains, then make this a leaf node. */
    if (exlist->tnext == NULL) {
	node->leaf = TRUE;
	node->exemplar = exlist;
	TreeSize++;
	MaxDepth = MAX(MaxDepth, depth);
    } else
	ChooseDivision(node, exlist, depth);
}


/* Choose which feature to use in order to subdivide the examples given
   in keylist.  Choose feature with the highest variance.  
     We require that the feature be different from any parent feature.
   This is quite appropriate for this application as there are many
   features of roughly equal weight, so there is no reason to subdivide
   more than once on any one.  This greatly simplifies the calculation of
   minimum distance to a bin boundary when searching, as there is no need
   to keep track of and undo the influence of a higher-level boundary.
*/
void ChooseDivision(Tree node, Keypoint keylist, int depth)
{
    int i, count = 0, maxfeat = 0;
    float dist, val, maxvar = 0.0;
    float mean[VecLength], var[VecLength];
    Keypoint key;
    Tree par;

    for (i = 0; i < VecLength; i++) {
      mean[i] = 0.0;
      var[i] = 0.0;
    }
    /* Compute mean values.  Tried sampling keys, but results were better
       using full set.  Also tried using median instead of mean, but
       this gives worse approximate search results for these descriptors.
    */
    for (key = keylist; key != NULL; key = key->tnext) {
      count++;
      for (i = 0; i < VecLength; i++)
	mean[i] += (float) key->ivec[i];
    }
    for (i = 0; i < VecLength; i++)
      mean[i] /= count;

    /* Compute variances (no need to divide by count). */
    for (key = keylist; key != NULL; key = key->tnext)
      for (i = 0; i < VecLength; i++) {
	val = (float) key->ivec[i];
	dist = val - mean[i];
	var[i] += dist * dist;
      }
    /* Go back through parents and eliminate features that have been used. */
    for (par = node->parent; par != NULL; par = par->parent)
      var[par->divfeat] = 0.0;

    /* Select feature with maximum variance. */
    for (i = 0; i < VecLength; i++)
      if (var[i] > maxvar) {
	maxvar = var[i];
	maxfeat = i;
      }
    /* If maxvar = 0, then all remaining features have the same values
       and maxfeat will be set arbitrarily to 0. */

    Subdivide(node, keylist, maxfeat, (int) mean[maxfeat], depth);
}
      

/* Subdivide the list of exemplars using the float feature divfeat and
   the division point divval.  Call DivideTree recursively on each list. 
*/
void Subdivide(Tree node, Keypoint keylist, int divfeat, int divval, int depth)
{
    Keypoint key, tempnext, list1 = NULL, list2 = NULL;

    node->divfeat = divfeat;
    node->divval = divval;
    
    /* Split exemplars into two lists. */
    for (key = keylist; key != NULL; key = tempnext) {
	tempnext = key->tnext;
	if (key->ivec[divfeat] <= divval) {
	    key->tnext = list1;
	    list1 = key;
	} else {
	    key->tnext = list2;
	    list2 = key;
	}
    }
    /* If either list is NULL, it means we have hit the unlikely case
       in which all remaining features are identical so we split off
       one vector to the empty list.  Should almost never happen.
    */
    if (list1 == NULL) {
      list1 = list2;
      list2 = list2->tnext;
      list1->tnext = NULL;
    }
    if (list2 == NULL) {
      list2 = list1;
      list1 = list1->tnext;
      list2->tnext = NULL;
    }

    DivideTree(& node->child1, node, list1, depth + 1);
    DivideTree(& node->child2, node, list2, depth + 1);
}


/*----------------------- Nearest Neighbour Lookup ------------------------*/

/* Find set of numNN nearest neighbours to the keypoint, and place in
   result vector.
*/
void FindNeighbours(Keypoint *result, int numNN, Keypoint keypoint)
{
    Branch branch;

    HeapLen = 0;
    NCount = 0;
    CheckCount = 0;

    /* Follow first search path down to root. */
    SearchLevel(result, numNN, keypoint, keypoint->ivec, AccessTree, 0);

    /* Keep searching other branches until finished. */
    while ((branch = HeapMin()) != NULL  &&  CheckCount < MaxCheck)
// MB test -- force to find numNN
/*		&& (CheckCount * 4 < MaxCheck ||
	    IntConvFactor * branch->mindistsq <
	       SearchDistSq * result[0]->distsq)) */
	   SearchLevel(result, numNN, keypoint, keypoint->ivec, branch->node,
		  branch->mindistsq);
}


/* Move down one level of the tree.  Based on any mismatches at
   higher levels, all exemplars below this level must have a distance of
   at least "mindistsq". 
*/
void SearchLevel(Keypoint *result, int numNN, Keypoint key,
		 unsigned char *ivec, Tree node, int mindistsq)
{
    int feat;
    int diff;

    /* If this is a leaf node, then do check and return. */
    if (node->leaf) {
        
        // make sure keys are not from same image
        if (key->imageID != node->exemplar->imageID)
        {  CheckNeighbour(result, numNN, key, node->exemplar); }
        return;
    }

    /* Which child branch should be taken? */
    feat = node->divfeat;
    diff = ivec[feat] - node->divval;

    if (diff < 0) {

      /* Create a branch record for the branch not taken.  Add influence
	 of this feature boundary (simple in this case only because we
	 know that this feature was not used in a parent node).  Don't
	 bother adding more branches to heap after halfway point, as
	 cost of adding exceeds their value.
      */
      if (2 * CheckCount < MaxCheck)
	HeapInsert(node->child2, mindistsq + diff * diff);

      /* Call recursively to search next level down. */
      SearchLevel(result, numNN, key, ivec, node->child1, mindistsq);

    } else {

      /* Same as above, but switches child1 <=> child2 */
      if (2 * CheckCount < MaxCheck)
	HeapInsert(node->child1, mindistsq + diff * diff);
      SearchLevel(result, numNN, key, ivec, node->child2, mindistsq);
    }
}


/*------------------------- Priority Queue -----------------------------*/

/* The priority queue is implemented with a heap.  A heap is a complete
   (full) binary tree in which each parent is less than both of its
   children, but the order of the children is unspecified.
     Note that a heap uses 1-based indexing to allow for power-of-2
   location of parents and children.  We ignore element 0 of Heap array.
*/

/* Insert a new element in the heap, with values "node" and "mindistsq".
   We select the next empty leaf node, and then keep moving any larger
   parents down until the right location is found to store this element.
      If the heap is full, then just replace one of the leaf nodes, choosing
   a different one each time (does not remove the smallest, so keep max
   heap size large to prevent overflow).
*/
void HeapInsert(Tree node, int mindistsq)
{
    int loc, par;
    static int replaceloc = 0;

    /* Choose the location at which node will be inserted.  If the
       heap is full, then cycle through selection of all leaf nodes.
    */
    if (HeapLen == MaxHeap)
      loc = HeapLen - (replaceloc++ % (HeapLen / 2));
    else
      loc = ++HeapLen;   /* Remember 1-based indexing. */

    /* Keep moving parents down until a place is found for this node. */
    par = loc / 2;                 /* Location of parent. */
    while (par > 0  &&  Heap[par].mindistsq > mindistsq) {
      Heap[loc] = Heap[par];       /* Move parent down to loc. */
      loc = par;
      par = loc / 2;
    }
    /* Insert the element at the determined location. */
    Heap[loc].node = node;
    Heap[loc].mindistsq = mindistsq;
}


/* Return the node from the heap with minimum value.  Reorganize
   to maintain the heap.
*/
Branch HeapMin()
{
    struct BranchSt temp;

    if (HeapLen == 0)
      return NULL;

    /* Switch first node with last. */
    temp = Heap[1];
    Heap[1] = Heap[HeapLen];
    Heap[HeapLen] = temp;

    HeapLen--;
    Heapify(1);      /* Move new node 1 to right position. */

    return & Heap[HeapLen + 1];  /* Return old last node. */
}


/* Take a heap rooted at position "parent" and enforce the heap critereon
   that a parent must be smaller than its children.
*/
void Heapify(int parent) 
{
    int left, right, minloc = parent;
    struct BranchSt temp;

    /* Check the left child */
    left = 2 * parent;
    if (left <= HeapLen  &&
	Heap[left].mindistsq < Heap[parent].mindistsq)
      minloc = left;

    /* Check the right child */
    right = left + 1;
    if (right <= HeapLen  && 
	Heap[right].mindistsq < Heap[minloc].mindistsq)
      minloc = right;

    /* If a child was smaller, than swap parent with it and Heapify. */
    if (minloc != parent) {
      temp = Heap[parent];
      Heap[parent] = Heap[minloc];
      Heap[minloc] = temp;
      Heapify(minloc);
    }
}


/*----------------------- Distance measure ----------------------------*/

/* Return the squared distance between two keypoint vectors. This is
   highly optimized, with loop unrolling, as it is one of the most
   expensive inner loops of recognition.
*/
float IntDistSquared(Keypoint k1, Keypoint k2)
{
    int diff, distsq = 0;
    int diff0, diff1, diff2, diff3;
    unsigned char *v1, *v2, *final, *finalgroup;

    v1 = k1->ivec;
    v2 = k2->ivec;
    final = v1 + VecLength;
    finalgroup = final - 3;

    /* Process 4 pixels with each loop for efficiency. */
    while (v1 < finalgroup) {
      diff0 = v1[0] - v2[0];
      diff1 = v1[1] - v2[1];
      diff2 = v1[2] - v2[2];
      diff3 = v1[3] - v2[3];
      distsq += diff0 * diff0 + diff1 * diff1 + diff2 * diff2 + diff3 * diff3;
      v1 += 4;
      v2 += 4;
    }
    /* Process last 0-3 pixels.  Not needed for standard vector lengths. */
    while (v1 < final) {
      diff = *v1++ - *v2++;
      distsq += diff * diff;
    }
    return IntConvFactor * (float) distsq;
}


/* Measure distance to this exemplar and add it to the result list
   if appropriate. 
*/
void CheckNeighbour(Keypoint *result, int numNN, Keypoint key, Keypoint nkey)
{
    int i, inserted;
    float distsq;
    Keypoint temp;

    CheckCount++;
    distsq = IntDistSquared(key, nkey);

    /* If this belongs to result list, then insert the exemplar into
       appropriate location of result, and shift all subsequent
       neighbours up by 1.
    */
    if (NCount < numNN  ||  distsq < result[NCount-1]->distsq) {
      nkey->distsq = distsq;
      inserted = FALSE;
      for (i = 0; i < NCount; i++) {
	if (!inserted && distsq < result[i]->distsq)
	  inserted = TRUE;
	if (inserted) {
	  temp = result[i];
	  result[i] = nkey;
	  nkey = temp;
	}
      }
      if (NCount < numNN) {
	result[NCount] = nkey;
	NCount++;
      }
    }
}

