// util.h

// COMMERCIAL version

/* Copyright (c) 2003. David G. Lowe, University of British Columbia.
   This code may be used, distributed, or modified only under license
   from the University of British Columbia.  This notice must be retained
   in all copies.
*/

#ifndef _UTIL_H
#define _UTIL_H

#include "defs.h"
#include "key.h"

void *MallocPool(int size, int pool);
void FreeStoragePool(int pool);
float **AllocMatrix(int rows, int cols, int pool);
Image CreateImage(int rows, int cols, int pool);
Image CopyImage(Image image, int pool);
Image DoubleSize(Image image);
Image HalfImageSize(Image image);
void SubtractImage(Image im1, Image im2);
void GradOriImages(Image im, Image grad, Image ori);
void GaussianBlur(Image image, float sigma);
void SolveLeastSquares(float *solution, int rows, int cols, float **jacobian,
		       float *errvec, float **sqarray);
void SolveLinearSystem(float *solution, float **sq, int size);
float DotProd(float *v1, float *v2, int len);

#endif
